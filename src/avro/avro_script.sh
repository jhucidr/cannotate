java -jar ../../lib/avro-tools-1.7.7.jar compile schema GenomeSequence.json ../../src
java -jar ../../lib/avro-tools-1.7.7.jar compile schema FileMetadata.json ../../src
java -jar ../../lib/avro-tools-1.7.7.jar compile schema GeneMetadata.json ../../src
java -jar ../../lib/avro-tools-1.7.7.jar compile schema TranscriptionUnit.json ../../src
java -jar ../../lib/avro-tools-1.7.7.jar compile schema VcfMetadata.json ../../src
java -jar ../../lib/avro-tools-1.7.7.jar compile schema AnnotationMetadata.json ../../src