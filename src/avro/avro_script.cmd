java -jar ..\..\lib\avro-tools-1.7.7.jar compile schema FileMetadata.json ..\..\src
java -jar ..\..\lib\avro-tools-1.7.7.jar compile schema GeneMetadata.json ..\..\src
java -jar ..\..\lib\avro-tools-1.7.7.jar compile schema transcription_unit.json ..\..\src
java -jar ..\..\lib\avro-tools-1.7.7.jar compile schema VcfMetadata.json ..\..\src
java -jar ..\..\lib\avro-tools-1.7.7.jar compile schema AnnotationMetadata.json ..\..\src
java -jar ..\..\lib\avro-tools-1.7.7.jar compile schema GenomeSequence.json ..\..\src