#!/bin/bash

######################
# Track and record Processing Time
######################
START_TIME=$SECONDS
LAP=${START_TIME}
TIMING_OUTPUT=time.txt

echo "" >> ${TIMING_OUTPUT}

Timer() {
	ELAPSED_TIME=$(($SECONDS - $START_TIME))
	LAP_TIME=$(($SECONDS - $LAP))
	echo -e "$1\tDuration=${LAP_TIME}\tTotal=${ELAPSED_TIME}" >> ${TIMING_OUTPUT}
	LAP=$SECONDS
}
Timer "Starting new Test Cannotate Run..." >> ${TIMING_OUTPUT}

RUN() {
	echo "Running $@"
	$@
}

######################
# Set up Required input Files
######################

read -n1 -p "Download Input Files? [y,n]" input 
if [[ $input == "Y" || $input == "y" ]]; then
	#pulls down vcf files
        RUN "./download_vcf.sh"
	Timer "Download all chromosome VCFs Complete"

        # pulls down the annotation 'databases'
        RUN "./down_db_script.sh"

	Timer "Download Input Annotation Files Complete"
fi


######################
# CANNOTATE Environment:
######################
CANNOTATE_JAR=HadoopProject.jar
CONFIG_FILE=AccumuloConfig.txt
RUN_CANNOTATE="/isilon/sequencing/CIDRSeqSuiteSoftware/java/jdk1.8.0_45/bin/java -jar ${CANNOTATE_JAR} "
OUTPUT_REPORT_FILE="CANNOTATE_OUTPUT.txt"
INPUT_VCF_FILE="./vcf_list.txt"
ANNOTATIONS_FILE="multi_db_file.txt"

######################
# Run CANNOTATE
######################
RUN ${RUN_CANNOTATE} -create ${CONFIG_FILE}
Timer "CANNOTATE Database Creation Complete"

RUN "${RUN_CANNOTATE} -ingest_multiple_annotations ${CONFIG_FILE} ${ANNOTATIONS_FILE}" 
Timer "Ingest Annotation Complete"

while read vcf; do
  RUN "${RUN_CANNOTATE} -ingest_vcf ${CONFIG_FILE} $vcf"
  Timer "Ingest VCF Complete for $vcf"
done <$INPUT_VCF_FILE


Timer "Ingest VCF Complete"

RUN "${RUN_CANNOTATE} -generate_report ${CONFIG_FILE} ${OUTPUT_REPORT_FILE}"
Timer "Report Generation Complete"
