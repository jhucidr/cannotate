#!/bin/bash

mkdir ./down_db/
./annotate_variation.pl -downdb -webfrom ucsc mirna ./down_db
./annotate_variation.pl -downdb -webfrom annovar  1000g ./down_db
./annotate_variation.pl -downdb -webfrom annovar  1000g2010 ./down_db
./annotate_variation.pl -downdb -webfrom annovar  segdup ./down_db
./annotate_variation.pl -downdb -webfrom ucsc gwasCatalog ./down_db
./annotate_variation.pl -downdb -webfrom annovar  refGene ./down_db
./annotate_variation.pl -downdb -webfrom annovar  knownGene ./down_db
./annotate_variation.pl -downdb -webfrom ucsc band ./down_db
./annotate_variation.pl -downdb -webfrom annovar  snp138 ./down_db
./annotate_variation.pl -downdb -webfrom annovar  ensGene ./down_db
./annotate_variation.pl -downdb -webfrom ucsc  segdup ./down_db./
./annotate_variation.pl -downdb -webfrom ucsc mirnatarget ./down_db
./annotate_variation.pl -downdb -webfrom annovar  refGene ./down_db
./annotate_variation.pl -downdb -webfrom ucsc tfbs ./down_db

./annotate_variation.pl -downdb -webfrom annovar snp135 ./down_db
./annotate_variation.pl -downdb -webfrom annovar snp132 ./down_db
./annotate_variation.pl -downdb -webfrom annovar snp131 ./down_db
./annotate_variation.pl -downdb -webfrom annovar snp130 ./down_db
./annotate_variation.pl -downdb -webfrom annovar snp129 ./down_db
./annotate_variation.pl -downdb -webfrom annovar snp128 ./down_db

./annotate_variation.pl -downdb -webfrom annovar nci60 ./down_db
./annotate_variation.pl -downdb -webfrom annovar ljb26_all ./down_db
./annotate_variation.pl -downdb -webfrom annovar exac03 ./down_db
./annotate_variation.pl -downdb -webfrom annovar esp6500siv2_all ./down_db
./annotate_variation.pl -downdb -webfrom annovar dbnsfp30a ./down_db
./annotate_variation.pl -downdb -webfrom annovar cosmic70 ./down_db
./annotate_variation.pl -downdb -webfrom annovar cg69 ./down_db
./annotate_variation.pl -downdb -webfrom annovar cg46 ./down_db
./annotate_variation.pl -downdb -webfrom annovar 1000g2012apr ./down_db
./annotate_variation.pl -downdb -webfrom annovar 1000g2010jul ./down_db




