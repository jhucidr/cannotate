
import edu.jhu.cannotate.record.GenomicPosition;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class RefGeneUnderstander {

    public static void main(String[] args) throws Exception {
        try (Stream<String> lines = Files.lines(Paths.get("/Users/sean-la/Documents/sequencing/annovar/down_db/hg18_refGene.txt"))) {

            List<GenomicPosition> units = lines.map(line -> line.split("\t"))
                    .map(parts -> new GenomicPosition(parts[4], parts[5], parts[2]))
                    .sorted()
                    .collect(Collectors.toList());

            GenomicPosition previous = null;
            for (GenomicPosition unit : units) {
                if (previous == null) {
                    previous = unit;
                    continue;
                } else {
                    if (unit.overlaps(previous)) {
                        System.out.println("Overlap");
                        System.out.println(unit);
                        System.out.println(previous);
                    }
                    previous = unit;
                }

            }
        }

    }

}
