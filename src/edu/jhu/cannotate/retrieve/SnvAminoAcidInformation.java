/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.TranscriptonUnitInformation;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class SnvAminoAcidInformation {

    private final GenomicPosition singleNucleotideVariant;
    private final String referenceAminoAcid;
    private final String variantAminoAcid;
    private final long positionOne;
    private final long positionTwo;
    private final long positionThree;

    public SnvAminoAcidInformation(GenomicPosition singleNucleotideVariant, String trio, long positionOne, long positionTwo, long positionThree) {
        if (singleNucleotideVariant.isASpan()) {
            throw new IllegalStateException();
        }
        this.singleNucleotideVariant = singleNucleotideVariant;
        this.referenceAminoAcid = trio;
        this.positionOne = positionOne;
        this.positionTwo = positionTwo;
        this.positionThree = positionThree;
        this.variantAminoAcid = calculateVariantAminoAcid();
    }

    public String getReferenceAminoAcid() {
        return referenceAminoAcid;
    }

    public String getVariantAminoAcid() {
        return variantAminoAcid;
    }

    public long getPositionOne() {
        return positionOne;
    }

    public long getPositionTwo() {
        return positionTwo;
    }

    public long getPositionThree() {
        return positionThree;
    }

    public GenomicPosition getSingleNucleotideVariant() {
        return singleNucleotideVariant;
    }

    public int calculateRelativeVariantPosition() {
        if (singleNucleotideVariant.getStartPosition() == positionOne) {
            return 0;
        } else if (singleNucleotideVariant.getStartPosition() == positionTwo) {
            return 1;
        } else if (singleNucleotideVariant.getStartPosition() == positionThree) {
            return 2;
        } else {
            try {
                Thread.sleep(1000);
                System.out.println(this);
                Thread.sleep(1000);
            } catch (Exception ex) {
            }
            throw new IllegalStateException("The SNV does not match any of the positions in this amino acid.");
        }
    }

    private String calculateVariantAminoAcid() {
        return replace(referenceAminoAcid, calculateRelativeVariantPosition(), singleNucleotideVariant.getAlt());
    }

    private String replace(String referenceString, int snvLocation, String alt) {
        StringBuilder stringBuilder = new StringBuilder(referenceString);
        return stringBuilder.replace(snvLocation, snvLocation + 1, alt).toString();
    }

    @Override
    public String toString() {
        return "SnvAminoAcidInformation{" + "singleNucleotideVariant=" + singleNucleotideVariant + ", referenceAminoAcid=" + referenceAminoAcid + ", variantAminoAcid=" + variantAminoAcid + ", positionOne=" + positionOne + ", positionTwo=" + positionTwo + ", positionThree=" + positionThree + '}';
    }

}
