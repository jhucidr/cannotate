/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.TranscriptionUnitConfiguration;
import edu.jhu.cannotate.table.AccumuloTable;
import java.io.File;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

public class Annotator {

    private final File outputFile;
    private final TranscriptionUnitConfiguration conf;

    public Annotator(File outputFile) {
        this.outputFile = outputFile;
        this.conf = TranscriptionUnitConfiguration.DEFAULT;
    }

    public Annotator(File outputFile, TranscriptionUnitConfiguration conf) {
        this.outputFile = outputFile;
        this.conf = conf;
    }

   
    
    //RowId, Family, Qualifier
    //RowId: Start_Stop_Chr
    //Family: Record-Type_SourceFileName
    //Qualifier: Ref_Alt_AnnoInfo
    public void cannotate() throws Exception {
        try (AnnotationWriter.BufferedRecordHandler handler = new AnnotationWriter.BufferedRecordHandler(outputFile)) {
            AnnotationWriter writer = new AnnotationWriter(handler);
            AnnotationEngine cannodata = new AnnotationEngine(writer, conf);
            try (Stream<Map.Entry<Key, Value>> annotationStream = AccumuloTable.ANNOTATION_DATA.streamTable()) {
                annotationStream
                        .map(AccumuloRecordType::buildAccumuloRecord)
                        .forEach(cannodata::add);
            }
        }

    }

    
}
