/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.record.AccumuloRecord;
import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.AccumuloReferenceGenomeRecord;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.table.AccumuloTable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class ReferenceRetriever {

    /**
     * This method returns a List of Reference records associated with a
     * particular Genomic Position. If you are looking to retrieve the DNA
     * object, or DNA String, associated with a specific GenomicPosition, you
     * should use ReferenceDictionary.fromDictionary() instead. 
     *
     * @param position
     * @return
     * @throws Exception
     */
    public static List<AccumuloReferenceGenomeRecord> getReference(GenomicPosition position) throws Exception {
        Scanner scanner = AccumuloTable.TESTY_MCTEST_FACE.buildScanner();
        Range referenceRange = position.getReferenceRange();
        System.out.println("referenceRange = " + referenceRange);
        scanner.setRange(referenceRange);

        List<AccumuloReferenceGenomeRecord> result = new ArrayList<>();
        for (Entry<Key, Value> entry : scanner) {
            AccumuloReferenceGenomeRecord record = AccumuloRecordType.REFERENCE.buildRecord(entry);
            if (record.getChromosome().equalsIgnoreCase(position.getChromosome())) {
                result.add(record);
            }
        }
        return result;
    }

    //Key: chr, start (and maybe stop?) 
    //Value: actual DNA object, or possibly the AccumuloRecord object
    public static void main(String[] args) throws Exception {
        GenomicPosition pos = new GenomicPosition("1", 123000, 129000);
        List<AccumuloReferenceGenomeRecord> dna = getReference(pos);
        System.out.println("dna = " + dna);

    }

}
