/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import edu.jhu.cannotate.record.AccumuloGeneAnnotationRecord;
import edu.jhu.cannotate.record.AccumuloRecord;
import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.TranscriptionUnitConfiguration;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AnnotationEngine {

    //RowId -> Set<Record>
    //needs to be a map because of the possibility of INDELS
    //TODO: consider separating out the gene annotation records?
    private final SetMultimap<GenomicPosition, AccumuloRecord> positionToAnnotationMap;
    private final SetMultimap<GenomicPosition, AccumuloRecord> regionToAnnotationMap;
    private final AnnotationWriter writer;
    private final TranscriptionUnitConfiguration conf;
    private GenomicPosition previous = null;
    private long pos = 0;

    public AnnotationEngine(AnnotationWriter writer, TranscriptionUnitConfiguration conf) {
        this.positionToAnnotationMap = HashMultimap.create();
        this.regionToAnnotationMap = HashMultimap.create();
        this.conf = conf;
        this.writer = writer;
    }

    public void add(final AccumuloRecord record) {
        if (record.getStartAsLong() > pos) {
            //magic number - 
            pos += 10_000_000;
            System.out.println("pos = " + pos);
        }
        AccumuloRecordType recordType = record.getAnnotationType();
        AccumuloRecord _record = recordType.toSubtype(record);
        if (recordType.isMulti()) {
            if (recordType == AccumuloRecordType.GENE) {
                AccumuloGeneAnnotationRecord geneRecord = recordType.toSubtype(_record);
                geneRecord.configureTranscriptionUnitInformation(conf);
                regionToAnnotationMap.put(geneRecord.getWidenedGenomicPosition(), geneRecord);
            } else {
                regionToAnnotationMap.put(_record.getGenomicPosition(), _record);
            }

        } else {
            flushMap(_record);
            positionToAnnotationMap.put(_record.getGenomicPosition(), _record);
        }
    }

    public void writeData(Tuple2<GenomicPosition, Set<AccumuloRecord>> toWrite) {
        AnnotationOrganizer organizer = new AnnotationOrganizer(conf, toWrite);
        if (organizer.hasTheDatas()) {
            try {
                writer.write(organizer);
            } catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
    }

    private void flushMap(AccumuloRecord record) {
        GenomicPosition current = record.getGenomicPosition();
        if (false == current.equals(previous)) {
            //Clear out any out-of-range annotations.
            List<Tuple2<GenomicPosition, Set<AccumuloRecord>>> toFlush
                    = positionToAnnotationMap.keySet()
                    .stream()
                    .filter((k) -> k.isLessThan(current))
                    .map((edu.jhu.cannotate.record.GenomicPosition k) -> new Tuple2<>(k, positionToAnnotationMap.get(k)))
                    .collect(Collectors.toList());
            if (toFlush.size() > 0) {
                toFlush.stream().map(this::addRegions).peek(this::writeData).map((t) -> t.get1()).forEach(positionToAnnotationMap::removeAll);
                //this is MAYBE the wrong place to do this, but if it is I'm not sure what the right place is. - SMLG
                removeRegions(current);
            }
            previous = current;
        }
        //flush if the record is not being added to ANY of the existing keys,
        // either as a filter-bud or a as a multilocation (filter or gene annotation).
    }

    private void removeRegions(GenomicPosition current) {
        List<GenomicPosition> regionsToRemove = regionToAnnotationMap.keySet()
                .stream()
                .filter((region) -> region.isLessThan(current))
                .collect(Collectors.toList());
        regionsToRemove.stream().forEach(regionToAnnotationMap::removeAll);
    }

    //TODO: pass through collection modification is gross.
    private Tuple2<GenomicPosition, Set<AccumuloRecord>> addRegions(Tuple2<GenomicPosition, Set<AccumuloRecord>> toMakePosition) {
        regionToAnnotationMap.keySet()
                .stream()
                .filter((region) -> region.overlaps(toMakePosition.get1()))
                .map(regionToAnnotationMap::get)
                .forEach(toMakePosition.get2()::addAll);
        return toMakePosition;
    }

}
