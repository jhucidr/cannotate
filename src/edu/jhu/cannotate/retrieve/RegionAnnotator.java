/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.TranscriptionUnitConfiguration;
import edu.jhu.cannotate.retrieve.AnnotationWriter.BufferedRecordHandler;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class RegionAnnotator {

    private final File outputDirectory;
    private final TranscriptionUnitConfiguration conf;

    public RegionAnnotator(File outputDirectory) {
        this.outputDirectory = outputDirectory;
        this.conf = TranscriptionUnitConfiguration.DEFAULT;
    }

    public RegionAnnotator(File outputDirectory, TranscriptionUnitConfiguration conf) {
        this.outputDirectory = outputDirectory;
        this.conf = conf;
    }

    public void cannotate() throws Exception {
        if(false == outputDirectory.isDirectory()) {
            outputDirectory.mkdirs();
        }
        List<AnnotationRegion> regions = new AnnotationRegionBuilder(10, 250_000_000L).buildRegions();
        regions.stream().parallel().forEach(this::annotateRegion);
    }

    //TODO: there is overlap between the output files, which is a clumsy way to handle edge cases.
    //Filter out everything from the overlap except for region based annotations.
    public void annotateRegion(AnnotationRegion region) {
        try (BufferedRecordHandler handler = new BufferedRecordHandler(buildOutputFile(region))) {
            AnnotationWriter writer = new AnnotationWriter(handler);
            AnnotationEngine cannodata = new AnnotationEngine(writer, conf);
            try (Stream<Map.Entry<Key, Value>> annotationStream = region.buildAnnotationRegionStream()) {
                annotationStream
                        .map(AccumuloRecordType::buildAccumuloRecord)
                        .forEach(cannodata::add);
            }
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    private File buildOutputFile(AnnotationRegion region) {
        return new File(outputDirectory, region.getStartPosition() + "_" + region.getStopPosition() + ".txt"); 
    }

}
