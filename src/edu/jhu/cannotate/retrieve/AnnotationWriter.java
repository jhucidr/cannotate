/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.ingest.AnnotationMetadataRecord;
import edu.jhu.cannotate.ingest.VcfMetadataRecord;
import edu.jhu.cannotate.record.DataType;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.metadata.MetadataUtils;
import edu.jhu.cannotate.retrieve.AnnotationOrganizer.MetaDataColumn;
import edu.jhu.cannotate.util.Utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnnotationWriter {

    //GenomicPosition_Annotations_Samples
    //We need to know up front:
    //All the different annotation file sources - Eacho A-file = 1-2 columns. Right now, only 1.
    //All the different VCF file sources - Eacho VCF 1-VERY MANY columns
    //For now, we're just going to retrieve all Samples and Annotations.
    private final BufferedRecordHandler recordHandler;
    private final List<DataType> annotationSource;
    private final List<String> samples = VcfMetadataRecord.retrieveAllSamples(); //TODO: Pass this in to Cannotate2.
    public static final String DELIMITER = "\t";

    public static void main(String... args) throws Exception {
        System.out.println(VcfMetadataRecord.retrieveAllSamples());
//        MetadataUtils.getMetadataByType(AvroRecordType.VCF_METADATA)
//                .forEach(System.out::println);
    }

    public AnnotationWriter(BufferedRecordHandler recordHandler) throws Exception {
        this.annotationSource = AnnotationMetadataRecord.getCurrentAnnotationFields();
        this.recordHandler = recordHandler;
        //TODO: move?
        recordHandler.writeLine(generateHeader());
        recordHandler.writeSummaryLine(generateSummaryHeader());
    }

    public void write(AnnotationOrganizer organizer) throws IOException {
        String summaryRecord = organizer.createSummaryRecord(samples, annotationSource);
        recordHandler.writeSummaryLine(summaryRecord);
//        System.out.println("summaryRecord = " + summaryRecord);
        String fullRecord = summaryRecord + organizer.getSampleInfo(samples);
        System.out.println("fullRecord    = " + fullRecord);
        recordHandler.writeLine(fullRecord);
    }

    public final String generateSummaryHeader() {
        String requiredColumns = Stream.of(Column.values())
                .map(Column::name)
                .map(Utils::translateEnumNameToCamelCase)
                .collect(Collectors.joining(DELIMITER));
        StringBuilder sb = new StringBuilder(requiredColumns).append(DELIMITER);
        sb.append(Stream.of(MetaDataColumn.values())
                .map(MetaDataColumn::toString)
                .collect(Collectors.joining(DELIMITER)))
                .append(DELIMITER);
        System.out.println("annotationSource = " + annotationSource);
        sb.append(annotationSource.stream()
                .peek(System.out::println)
                .map(DataType::name)
                .collect(Collectors.joining(DELIMITER)))
                .append(DELIMITER);
        return sb.toString();
    }

    public final String generateHeader() {
        StringBuilder sb = new StringBuilder(generateSummaryHeader());
        sb.append(samples.stream().collect(Collectors.joining(DELIMITER))).append(DELIMITER);
        return sb.toString();
    }

    static enum Column {

        CHR {
                    @Override
                    public String extractValue(GenomicPosition pos) {
                        return pos.getChromosome();
                    }
                }, START {
                    @Override
                    public String extractValue(GenomicPosition pos) {
                        return "" + pos.getStartPosition();
                    }
                }, STOP {
                    @Override
                    public String extractValue(GenomicPosition pos) {
                        return "" + pos.getStopPosition();
                    }
                }, REF {
                    @Override
                    public String extractValue(GenomicPosition pos) {
                        return pos.getRef();
                    }
                }, ALT {
                    @Override
                    public String extractValue(GenomicPosition pos) {
                        return pos.getAlt();
                    }
                };

        public abstract String extractValue(GenomicPosition pos);

        @Override
        public String toString() {
            return Utils.translateEnumNameToCamelCase(name());
        }

    }

    public static class BufferedRecordHandler implements AutoCloseable {

        private final BufferedWriter writer;
        private final File outFile;
        private final BufferedWriter summaryWriter;

        public BufferedRecordHandler(File outFile) throws IOException {
            this.outFile = outFile;
            this.writer = new BufferedWriter(new FileWriter(outFile));
            File summaryFile = new File(outFile.getParentFile(), "Summary_" + outFile.getName());
            this.summaryWriter = new BufferedWriter(new FileWriter(summaryFile));
        }

        public void writeLine(String s) throws IOException {
            writer.write(s);
            writer.newLine();
        }

        public void writeSummaryLine(String s) throws IOException {
            summaryWriter.write(s);
            summaryWriter.newLine();
        }

        public void close() throws IOException {
            try {
                this.writer.close();
            } catch (Exception omnomnom) {
            }
            try {
                this.summaryWriter.close();
            } catch (Exception OMNOMNOM) {
            }
        }

        public File getOutputFile() {
            return outFile;
        }

    }

}
