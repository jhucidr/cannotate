/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AnnotationRegionBuilder {

    private final long numRegions;
    private final long maxPosition;
    private final long regionSize;
    private static final long PADDINGTON = 10_000;

    public AnnotationRegionBuilder(long numRegions, long maxPosition) {
        this.numRegions = numRegions;
        this.maxPosition = maxPosition;
        this.regionSize = (long) Math.ceil((double) maxPosition / numRegions);
    }

    public List<AnnotationRegion> buildRegions() {
        return LongStream.range(0, numRegions).mapToObj(this::buildRegion).collect(Collectors.toList());
    }

    //ASSUME THAT PADDINGTON IS SEVERAL ORDERS OF MAGNITUDE SMALLER THAN MAX POSITION.
    //TODO: Build weighted regions based on our table splits, instead of evenly-spaced regions.
    //This will allow us to properly balance our load accross the compute nodes.      
    private AnnotationRegion buildRegion(long regionNumber) {
        long start = regionNumber == 0 ? 0 : regionNumber * regionSize - PADDINGTON;
        long stop = (regionNumber + 1) * regionSize + PADDINGTON;
        return new AnnotationRegion(start, stop);
    }

}
