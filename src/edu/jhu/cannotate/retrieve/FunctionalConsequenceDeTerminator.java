/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.avro.RegionType;
import edu.jhu.cannotate.record.CodonChangeInformation;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.TranscriptionRegionsBuilder;
import edu.jhu.cannotate.record.TranscriptonUnitInformation;
import edu.jhu.cannotate.record.TranscriptonUnitInformation.TranscriptionRegion;
import edu.jhu.cannotate.reference.ReferenceDictionary;
import edu.jhu.cannotate.util.dna.Codon;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.dna.FunctionalConsequence;
import edu.jhu.cannotate.util.dna.FunctionalConsequenceInformation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class FunctionalConsequenceDeTerminator {

    private final GenomicPosition variantPosition;
    private final TranscriptonUnitInformation transcriptionUnitInformation;
    // private final GenomicPosition openReadingFrame;

    public FunctionalConsequenceDeTerminator(GenomicPosition variantPosition, TranscriptonUnitInformation transcriptionUnitInformation) {
        this.variantPosition = variantPosition;
        this.transcriptionUnitInformation = transcriptionUnitInformation;
        //     this.openReadingFrame = transcriptionUnitInformation.getOpenReadingFrame();
    }

    public List<FunctionalConsequenceInformation> determineFunctionalAnnotations() {
        Set<TranscriptonUnitInformation.TranscriptionRegion> geneAnnotations = transcriptionUnitInformation.determineGeneAnnotations(variantPosition);
        CodonChangeInformation info = getFunctionalChange(geneAnnotations);
        return geneAnnotations.stream()
                .map(anno -> determineFunctionalConsequences(anno, info))
                .collect(Collectors.toList());
    }

    private CodonChangeInformation getFunctionalChange(Set<TranscriptonUnitInformation.TranscriptionRegion> geneAnnotations) {
        if (variantPosition.isASpan() == false && geneAnnotations.stream().anyMatch(anno -> anno.getRegionType() == RegionType.EXON)) {
            //SNV
            SnvAminoAcidInformation aminoAcidInformation = determineTrio();

            Codon refCodon = Codon.translateToCodon(aminoAcidInformation.getReferenceAminoAcid());
            Codon variantCodon = Codon.translateToCodon(aminoAcidInformation.getVariantAminoAcid());
            return new CodonChangeInformation(refCodon, variantCodon);
        }
//            else {
        //this is InDels, which we need to address sometime...
        return CodonChangeInformation.NONE;
//        }
    }

    private FunctionalConsequenceInformation determineFunctionalConsequences(TranscriptonUnitInformation.TranscriptionRegion region, CodonChangeInformation possibleCodonChangeInformation) {
        Set<FunctionalConsequence> consequences = Arrays.stream(FunctionalConsequence.values())
                .filter(cons -> cons.doesTheGloveFit(possibleCodonChangeInformation, region))
                .collect(Collectors.toSet());
        return new FunctionalConsequenceInformation(consequences, possibleCodonChangeInformation, region);
    }

    public static class SlidingExonWindow {

        private final GenomicPosition variantPosition;
        private final TranscriptonUnitInformation transcriptionUnitInformation;
        //horrible mudpant mutable variables
        private long offset = 0;
        private boolean isDone = false;
        private TranscriptionRegion previousExon = null;
        private String result = null;
        private final List<Long> positions = new ArrayList<>();

        public SlidingExonWindow(GenomicPosition variantPosition, TranscriptonUnitInformation transcriptionUnitInformation) {
            this.variantPosition = variantPosition;
            this.transcriptionUnitInformation = transcriptionUnitInformation;
        }

        public SnvAminoAcidInformation doTheThing() {
            transcriptionUnitInformation.getRegionByType(RegionType.EXON)
                    .stream()
                    .sorted()
                    .map(exon -> exon.addReferenceData(ReferenceDictionary.fromDictionary(exon)))
                    .forEachOrdered(this::handleExon);
            return buildInformation();//calculate the amino acid information.
        }

        private void setPositionValuesForResult(TranscriptionRegion exon) {
            int numBasesFromPreviousExon = result.length();
            int numBasesFromCurrentExon = 3 - numBasesFromPreviousExon;
            setBasesFromPreviousExon(numBasesFromPreviousExon);
            setBasesFromCurrentExon(exon, numBasesFromCurrentExon);

        }

        //TODO: include an error when the sum of our exons is not divisible by 3.
        public void handleExon(TranscriptionRegion exon) {
            if (isDone) {
                return;
            }
            if (result != null && result.length() < 3) {
                int numBasesFromPreviousExon = result.length();
                int numBasesFromCurrentExon = 3 - numBasesFromPreviousExon;
                String suffix = exon.getRef().substring(0, numBasesFromCurrentExon);
                setPositionValuesForResult(exon);
                result = result + suffix;
                isDone = true;
            }
            if (variantPosition.overlaps(exon)) {
                //Determine the window

                //offset is in the range [0,2]
                //offset tracks exons where size is not divisible by three, and accumulates those values.
                //offset measures how far off the variant is from the start of the trio (within an exon).
                offset = (offset + variantPosition.getStartPosition() - exon.getStartPosition()) % 3;
                long actualStart = variantPosition.getStartPosition() - offset;
                result = exon.getOverlappingRange(actualStart, actualStart + 2);
                if (result.length() == 3) {
                    positions.add(actualStart);
                    positions.add(actualStart + 1);
                    positions.add(actualStart + 2);
                    isDone = true;
                } else {
                    //We know we're at a leading or trailing edge, because otherwise, we'd have a size of '3'.
                    if (Utils.withinRange(exon.getStartPosition(), variantPosition.getStartPosition(), 3)) { //Leading Edge
                        //pull it out of the previous exon
                        if (previousExon == null) {
                            throw new NullPointerException("No previous exon found.");
                        }
                        String fromPrevious = previousExon.getStringRange(previousExon.getStopPosition() - (2 - result.length()), previousExon.getStopPosition());
                        setPositionValuesForResult(exon);
                        result = fromPrevious + result;
                        isDone = true;
                    } else if (Utils.withinRange(exon.getStopPosition(), variantPosition.getStopPosition(), 3)) {//Trailing Edge
                        //Pull it out of the next exon, which is done up above when result != null.
                        finishedWithAnExonCleanup(exon);
                        return;
                    } else {
                        throw new IllegalStateException("Unable to locate variant within either of two adjacent exons:" + previousExon.toString() + exon.toString());
                    }
                }
            } else {
                finishedWithAnExonCleanup(exon);
            }
        }

        private void finishedWithAnExonCleanup(TranscriptionRegion exon) {
            long width = exon.calculateWidth();
            offset = (offset + width) % 3;
            previousExon = exon;
        }

        private SnvAminoAcidInformation buildInformation() {
            Collections.sort(positions);
            return new SnvAminoAcidInformation(variantPosition, result, positions.get(0), positions.get(1), positions.get(2));
        }

        private void setBasesFromCurrentExon(TranscriptionRegion exon, int numBasesFromCurrentExon) {
            LongStream.range(0, numBasesFromCurrentExon).map(i -> exon.getStartPosition() + i).forEachOrdered(positions::add);
        }

        private void setBasesFromPreviousExon(int numBasesFromPreviousExon) {
            LongStream.range(-1 * numBasesFromPreviousExon, 0).map(i -> previousExon.getStopPosition() + i).forEachOrdered(positions::add);
        }

    }

    // determines which start, stop the variant is in
    private SnvAminoAcidInformation determineTrio() {
        SlidingExonWindow slidingWindow = new SlidingExonWindow(variantPosition, transcriptionUnitInformation);
        return slidingWindow.doTheThing();
    }

    public static void main(String[] args) {
//        String refGeneLine = "590	NR_103536	chr1	+	10	80	80	80	2	10,40,69,	30,60,80,	0	FAM87B	unk	unk	-1,-1,";
        String refGeneLine = "595	NM_001039211	chr1	+	1385068	1405538	1386063	1403910	12	1385068,1387425,1387744,1389724,1390839,1391170,1391604,1392508,1394540,1396129,1397979,1403763,	1386138,1387502,1387814,1389880,1390899,1391296,1391729,1392560,1394611,1396297,1398088,1405538,	0	ATAD3C	cmpl	cmpl	0,0,2,0,0,0,0,2,0,2,2,0,";

        Set<TranscriptionRegion> regions = new TranscriptionRegionsBuilder(refGeneLine).build();

//        Set<TranscriptionRegion> regions = addRandomRefsToRegions(new TranscriptionRegionsBuilder(refGeneLine).build());
        TranscriptonUnitInformation transcriptonUnitInformation
                = new TranscriptonUnitInformation("GenusDavidus", "DavidsBestLilTranscript", regions);

        GenomicPosition variantPosition = new GenomicPosition("1", "T", "G", 1385080);
        FunctionalConsequenceDeTerminator terminator = new FunctionalConsequenceDeTerminator(variantPosition, transcriptonUnitInformation);
        //turn these two lines back on to run.
//        Map<TranscriptionRegion, Optional<CodonChangeInformation>> functionlAnnotations = terminator.determineFunctionalAnnotations();
//        System.out.println("determineFunctionalAnnotations = " + functionlAnnotations);

//        SlidingWindowBuilder builder = new SlidingWindowBuilder(transcriptonUnitInformation);
//        
//        List<GenomicPosition> testData = buildTestPositions(182567757, 182569648);
//        testData.addAll(buildTestPositions(182567757, 29));
//        testData.addAll(buildTestPositions(40, 43));
//        testData.addAll(buildTestPositions(57, 59));
//        testData.addAll(buildTestPositions(69, 72));
//        testData.addAll(buildTestPositions(77, 79));
//        testData
//                .stream()
//                .map(builder::buildSlidingWindow)
//                .map(SlidingWindow::doTheThing)
//                .forEach(System.out::println);
    }

//    private static class SlidingWindowBuilder {
//
//        private final TranscriptonUnitInformation info;
//
//        public SlidingWindowBuilder(TranscriptonUnitInformation info) {
//            this.info = info;
//        }
//
//        private SlidingWindow buildSlidingWindow(GenomicPosition position) {
//            System.out.println(position.getStartPosition());
//            return new SlidingWindow(position, info);
//        }
//
//    }
//
//    private static List<GenomicPosition> buildTestPositions(long start, long stop) {
//        List<GenomicPosition> goodBoys = new ArrayList<>();
//        String chermaserm = "1";
//        LongStream.rangeClosed(start, stop)
//                .forEach(l -> goodBoys.add(new GenomicPosition(chermaserm, l, l)));
////        LongStream.range(754102, 754112)
////                .forEach(l -> goodBoys.add(new GenomicPosition(chermaserm, l, l)));
//        return goodBoys;
//    }
//
//    private static Set<TranscriptionRegion> addRandomRefsToRegions(Set<TranscriptionRegion> regions) {
//        return regions.stream()
//                .map(r -> {
//                    if (r.getRegionType() == RegionType.EXON) {
//                        TranscriptionRegion randomRefReg = createRandomRefRegion(r);
//                        System.out.println("randomRefReg = " + randomRefReg);
//                        return randomRefReg;
//                    } else {
//                        return r;
//                    }
//                }
//                )
//                .collect(Collectors.toSet());
//    }
//
//    private static TranscriptionRegion createRandomRefRegion(TranscriptionRegion region) {
//        String ref = DnaUtils.generateRandomDNA(region.calculateWidth());
//        System.out.println(ref);
//        return new TranscriptionRegion(
//                region.getChromosome(),
//                ref,
//                region.getStartPosition(),
//                region.getStopPosition(),
//                region.getRegionType());
//    }
}
