/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.record.AccumuloGeneAnnotationRecord;
import edu.jhu.cannotate.record.AccumuloRecord;
import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.DataType;
import edu.jhu.cannotate.record.CodonChangeInformation;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.TranscriptionUnitConfiguration;
import edu.jhu.cannotate.record.TranscriptonUnitInformation;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.dna.FunctionalConsequenceInformation;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class AnnotationOrganizer {

    private final GenomicPosition genomicPosition;
    private final Set<String> annotatedSamples;
    //ID -> annotation data.
    private final Map<DataType, String> annotations;
    private final TranscriptionUnitConfiguration conf;

    public AnnotationOrganizer(TranscriptionUnitConfiguration conf, Tuple2<GenomicPosition, Set<AccumuloRecord>> toWrite) {
        this.conf = conf;
        this.genomicPosition = toWrite.get1();
        this.annotatedSamples = getSamples(toWrite.get2());
        this.annotations = getAnnotations(toWrite.get2());
    }

    private String accumuloValueToString(AccumuloRecord record) {
        if (record.getAnnotationType() == AccumuloRecordType.GENE) {
            AccumuloGeneAnnotationRecord geneRecord = (AccumuloGeneAnnotationRecord) record;
            geneRecord.configureTranscriptionUnitInformation(conf);
            TranscriptonUnitInformation info = geneRecord.getTranscriptonUnitInformation();
//            Set<TranscriptonUnitInformation.TranscriptionRegion> geneAnnotations = info.determineGeneAnnotations(genomicPosition);
            List<FunctionalConsequenceInformation> geneAnnotations 
                    = new FunctionalConsequenceDeTerminator(genomicPosition, info).determineFunctionalAnnotations();
            
            return mergeGeneAnnotations(geneRecord, geneAnnotations);
        } else {
            return record.getValue().toString();
        }
    }

    private String mergeGeneAnnotations(AccumuloGeneAnnotationRecord geneRecord, List<FunctionalConsequenceInformation> geneAnnotations) {
        return geneRecord.getTranscriptionRecord().getGeneName() + "(" + geneRecord.getStartAsLong() + "-" + geneRecord.getStopAsLong() + "){"
                + geneAnnotations.stream()
                .map(FunctionalConsequenceInformation::toString)
                .collect(Collectors.joining("; "))
                + "}";
    }
    
    private Map<DataType, String> getAnnotations(Set<AccumuloRecord> recs) {
        return recs.stream()
                .filter((edu.jhu.cannotate.record.AccumuloRecord rec) -> AccumuloRecordType.VCF != rec.getDatabaseType().getRecordType())
                .collect(Collectors.toMap(
                                (rec) -> rec.getDatabaseType(), //KEY
                                (rec) -> accumuloValueToString(rec), //VALUE
                                (s1, s2) -> s1 + "," + s2 //COLLISION HANDLING
                        ));
    }

    private Set<String> getSamples(Set<AccumuloRecord> recs) {
        return recs
                .stream()
                .filter((AccumuloRecord rec) -> AccumuloRecordType.VCF == rec.getDatabaseType().getRecordType())
                .map((sampleRec) -> sampleRec.getDataId())
                .collect(Collectors.toSet());
    }

    //GIVE ME THE DATAS
    public boolean hasTheDatas() {
        return (false == annotatedSamples.isEmpty()) && (false == annotations.isEmpty());
    }
    private static final DecimalFormat df = new DecimalFormat("0.000");

    static enum MetaDataColumn {

        NUM_SAMPLES {

                    @Override
                    public String getValue(AnnotationOrganizer writer, int totalNumSamples) {
                        return "" + writer.annotatedSamples.size();
                    }
                },
        PERCENT_SAMPLES {

                    @Override
                    public String getValue(AnnotationOrganizer writer, int totalNumSamples) {
                        return df.format((double) writer.annotatedSamples.size() / (double) totalNumSamples);
                    }
                };

        public abstract String getValue(AnnotationOrganizer writer, int totalNumSamples);

        @Override
        public String toString() {
            return Utils.translateEnumNameToCamelCase(name());
        }

    }

    public String createSummaryRecord(List<String> samples, List<DataType> annotationSource) {
        StringBuilder sb = new StringBuilder();
        sb.append(Arrays.stream(AnnotationWriter.Column.values())
                .map(col -> col.extractValue(genomicPosition))
                .collect(Collectors.joining(AnnotationWriter.DELIMITER)))
                .append(AnnotationWriter.DELIMITER);
        sb.append(Arrays.stream(AnnotationOrganizer.MetaDataColumn.values())
                .map(col -> col.getValue(this, samples.size()))
                .collect(Collectors.joining(AnnotationWriter.DELIMITER)))
                .append(AnnotationWriter.DELIMITER);
        sb.append(annotationSource.stream()
                .map(annotations::get)
                .map(value -> (value == null) ? ""/*OH_NOES_I_DONT_NO_WHERE_TEH_RECORD_IS!UHOH!!"*/ : value)
                .collect(Collectors.joining(AnnotationWriter.DELIMITER)))
                .append(AnnotationWriter.DELIMITER);
        return sb.toString();
    }

//    public String createRecord(List<String> samples, List<DataType> annotationSource) {
//        StringBuilder sb = new StringBuilder(createSummaryRecord(samples, annotationSource));
//        sb.append(samples.stream()
//                .map(sample -> annotatedSamples.contains(sample) ? "X" : "")
//                .collect(Collectors.joining(AnnotationWriter.DELIMITER)));
//        return sb.toString();
//    }
    public String getSampleInfo(List<String> samples) {
        return samples.stream()
                .map(sample -> annotatedSamples.contains(sample) ? "X" : "")
                .collect(Collectors.joining(AnnotationWriter.DELIMITER));
    }

}
