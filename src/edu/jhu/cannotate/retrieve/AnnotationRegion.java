/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.retrieve;

import edu.jhu.cannotate.table.AccumuloTable;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AnnotationRegion {
    private final String startPosition;
    private final String stopPosition;

    public AnnotationRegion(long startPosition, long stopPosition) {
        //            this.chromosomes = new HashSet<>();
        this.startPosition = AccumuloUtils.padInteger(startPosition);
        this.stopPosition = AccumuloUtils.padInteger(stopPosition + 1);
    } //            this.chromosomes = new HashSet<>();

    //TODO: if we're building multiple regions in a stream, we need to make an exception to throwing this.
    public Stream<Map.Entry<Key, Value>> buildAnnotationRegionStream() throws Exception {
        Scanner scanner = AccumuloTable.ANNOTATION_DATA.buildScanner();
        Range range = new Range(startPosition, stopPosition);
        scanner.setRange(range);
        Stream<Map.Entry<Key, Value>> stream = StreamSupport.stream(scanner.spliterator(), false);
        stream.onClose(scanner::close);
        return stream;
    }

    public String getStartPosition() {
        return startPosition;
    }

    public String getStopPosition() {
        return stopPosition;
    }
    
}
