/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.avro;

import edu.jhu.cannotate.record.CannotateRecord;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.apache.accumulo.core.data.Value;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AvroUtils {

    public static byte[] toByteRa(SpecificRecord instance) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
            DatumWriter<Object> writer = new SpecificDatumWriter<>(instance.getSchema());
            writer.write(instance, encoder);
            encoder.flush();
            return out.toByteArray();
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static Value toValue(SpecificRecord instance) {
        return new Value(AvroUtils.toByteRa(instance));
    }

    public static void toFile(SpecificRecord instance, File outputFile) throws Exception {
        DatumWriter<Object> writer = new SpecificDatumWriter<>(instance.getSchema());
        try (DataFileWriter<Object> dataFileWriter = new DataFileWriter<>(writer)) {
            dataFileWriter.create(instance.getSchema(), outputFile);
            dataFileWriter.append(instance);   
        }
    }
    
    public static void toFile(Stream<SpecificRecord> instances, File outputFile, Schema schema) throws Exception {
        DatumWriter<Object> writer = new SpecificDatumWriter<>(schema);
        try (DataFileWriter<Object> dataFileWriter = new DataFileWriter<>(writer)) {
            dataFileWriter.create(schema, outputFile);
            instances.forEach(instance -> append(dataFileWriter, instance));
        }
    }
    

    
    private static void append(DataFileWriter<Object> writer, SpecificRecord instance) {
        try {
            writer.append(instance);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static <T extends SpecificRecord> T fromByteRa(byte[] recordRa, Schema schema) {
        try {
            SpecificDatumReader<T> reader = new SpecificDatumReader<>(schema);
            Decoder decoder = DecoderFactory.get().binaryDecoder(recordRa, null);
            return (T) reader.read(null, decoder);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static void oldMain(String[] args) throws Exception {
        List<AvroRegion> regions = new ArrayList<>();
        regions.add(new AvroRegion("7", 12L, 15L, RegionType.EXON));
        regions.add(new AvroRegion("7", 35L, 72L, RegionType.SPLICING));
        AvroTranscriptionUnitInfo avroTranscriptionUnitInfo = new AvroTranscriptionUnitInfo("Region_Name", "Transcript_Name", regions);
        byte[] toByteRa = toByteRa(avroTranscriptionUnitInfo);
        SpecificRecord fromByteRa = fromByteRa(toByteRa, AvroTranscriptionUnitInfo.getClassSchema());
        System.out.println(fromByteRa);
    }

     public static List<CannotateRecord> readRecord(File file) throws Exception {
        // Deserialize Users from disk
        List<CannotateRecord> result = new ArrayList<>();
        DatumReader<CannotateRecord> userDatumReader = new SpecificDatumReader<>(CannotateRecord.class);
        try (DataFileReader<CannotateRecord> dataFileReader = new DataFileReader<>(file, userDatumReader)) {
            while (dataFileReader.hasNext()) {
// Reuse user object by passing it to next(). This saves us from
// allocating and garbage collecting many objects for files with
// many items.
                result.add(dataFileReader.next());
            }
        }
        return result;
    }
    
    public static void main(String... args) throws Exception {
        Stream<Integer> DAVID = Stream.of(1,2,3,4,5);
        
        DAVID.forEach(System.out::println);

    }
}
