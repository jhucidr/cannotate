/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.avro;

import edu.jhu.cannotate.record.TranscriptonUnitInformation;
import edu.jhu.cannotate.util.dna.Dna;
import edu.jhu.cannotate.reference.DnaWithMetaData;
import java.util.stream.Collectors;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AvroTranslator {

    public static AvroTranscriptionUnitInfo toAvro(TranscriptonUnitInformation info) {
        return new AvroTranscriptionUnitInfo(
                info.getGeneName(),
                info.getTranscriptName(),
                info.getRegions()
                .stream()
                .map(AvroTranslator::toAvroRegion)
                .collect(Collectors.toList()));
    }

    public static TranscriptonUnitInformation fromAvro(AvroTranscriptionUnitInfo avroInfo) {
        return new TranscriptonUnitInformation(
                avroInfo.getGeneName().toString(),
                avroInfo.getTranscriptName().toString(),
                avroInfo.getRegions()
                .stream()
                .map(AvroTranslator::fromAvroRegion)
                .collect(Collectors.toSet()));

    }

    public static AvroGenomeSequence toAvroDna(DnaWithMetaData dnaWithMetaData) {
        return new AvroGenomeSequence(
                dnaWithMetaData.getChromosome(), 
                dnaWithMetaData.getChromosome(), 
                dnaWithMetaData.getDna().getStartPosition(), 
                dnaWithMetaData.getDna().getStopPosition(), 
                dnaWithMetaData.getDna().getBytes());
    }
    
    public static DnaWithMetaData fromAvroDna(AvroGenomeSequence genomeSequence) {
        Dna dna = new Dna(
                (int) (genomeSequence.getStop() - genomeSequence.getStart()),//+ 1), 
                genomeSequence.getStart(), 
                genomeSequence.getSequence());
        return new DnaWithMetaData(dna, genomeSequence.getChr().toString());
    }
    
    private static AvroRegion toAvroRegion(TranscriptonUnitInformation.TranscriptionRegion region) {
        return new AvroRegion(
                region.getChromosome(),
                region.getStartPosition(),
                region.getStopPosition(),
                region.getRegionType());
    }

    private static TranscriptonUnitInformation.TranscriptionRegion fromAvroRegion(AvroRegion avroRegion) {
        return new TranscriptonUnitInformation.TranscriptionRegion(
                avroRegion.getChr().toString(),
                avroRegion.getStart(),
                avroRegion.getStop(),
                avroRegion.getRegionType());
    }
}
