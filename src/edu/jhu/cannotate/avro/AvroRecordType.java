/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.avro;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.accumulo.core.data.Value;
import org.apache.avro.Schema;
import org.apache.avro.specific.SpecificRecord;

/**
 *
 * @author sgriffit
 */
public enum AvroRecordType {

    //Not sure what other metadata we may want here.
    //TODO: ditch the class?
//Not sure what other metadata we may want here.
    //TODO: ditch the class?
    FILE_METADATA(AvroFileMetadata.class, AvroFileMetadata.getClassSchema()),
    GENE_METADATA(AvroGeneMetadata.class, AvroGeneMetadata.getClassSchema()),
    ANNOTATION_METADATA(AvroAnnotationMetadata.class, AvroAnnotationMetadata.getClassSchema()),
    VCF_METADATA(AvroVcfMetadata.class, AvroVcfMetadata.getClassSchema()),
    //non-metadata avro types
    TRANSCRIPTION_UNIT_INFO(AvroTranscriptionUnitInfo.class, AvroTranscriptionUnitInfo.getClassSchema()),
    REGION(AvroRegion.class, AvroRegion.getClassSchema()),
    REGION_TYPE(RegionType.class, RegionType.getClassSchema()),;

    private final Schema schema;

    private AvroRecordType(Class clazz, Schema schema) {
//        this.clazz = clazz;
        this.schema = schema;
    }

    public Schema getSchema() {
        return schema;
    }

    private static final Map<Schema, AvroRecordType> fromSchema;

    static {
        fromSchema = Arrays.stream(values())
                .collect(Collectors.toMap(
                                s -> s.getSchema(),
                                s -> s));
    }
    
    public static AvroRecordType getType(SpecificRecord instance){
        return fromSchema.get(instance.getSchema());
    }

    public Value toValue(SpecificRecord instance) {
        return new Value(AvroUtils.toByteRa(instance));
    }
    
    

}
