/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.avro;

import edu.jhu.cannotate.util.Utils;
import java.io.File;

/**
 *
 * @author sgriffit
 */
public class AvroFileMetadataUtils {

    public static String getCleanedFilename(AvroFileMetadata record) {
        return Utils.clean(record.getFilename().toString());
    }

    public String getFileName(AvroFileMetadata record) {
        return record.getFilename().toString();
    }

    public File getFile(AvroFileMetadata record) {
        return new File(record.getFullyQualifiedPathToFile().toString());
    }
}
