/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.avro;

import edu.jhu.cannotate.util.dna.Dna;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AvroBaseTest {

    public static Map<Character, Base> TEST_MAP = new HashMap<>();

    static {
        TEST_MAP.put('A', Base.A);
        TEST_MAP.put('T', Base.T);
        TEST_MAP.put('C', Base.C);
        TEST_MAP.put('G', Base.G);
        TEST_MAP.put('N', Base.N);
    }

    public static void main(String[] args) throws Exception {

        Path allThatDunnah = Paths.get("/Users/sean-la/Desktop/all_that_dna.txt");
        Stream<String> lines = Files.lines(allThatDunnah);
        String theOnlyOne = lines.findFirst().get();
        Dna dna = new Dna(0, theOnlyOne);
        byte[] allThatBytes = compress(theOnlyOne);
        
        AvroGenomeSequence avroGenomeSequenceBytes = new AvroGenomeSequence("DAVIDSFAVORITEREGION", "eleventy_one", 5L, 11L, dna.getBytes());
        AvroUtils.toFile(avroGenomeSequenceBytes, new File("/Users/sean-la/Desktop/all_that_dna_compressed_bytes_david.avro"));
        
    }

    public static byte[] compress(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new DeflaterOutputStream(baos);
            out.write(text.getBytes("UTF-8"));
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }

    
    
    public static String decompress(byte[] bytes) {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while ((len = in.read(buffer)) > 0) {
                baos.write(buffer, 0, len);
            }
            return new String(baos.toByteArray(), "UTF-8");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

}
