/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.util.dna.Codon;
import java.util.Objects;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class CodonChangeInformation {
    
    private final Codon referenceCodon;
    private final Codon variantCodon;
    public static final CodonChangeInformation NONE = new CodonChangeInformation() {
        @Override
        public Codon getReferenceCodon() {
            throw new UnsupportedOperationException("NONE has no codons.");
        }

        @Override
        public Codon getVariantCodon() {
            throw new UnsupportedOperationException("NONE has no codons.");
        }
        
    };

    public CodonChangeInformation(Codon referenceCodon, Codon variantCodon) {
        this.referenceCodon = referenceCodon;
        this.variantCodon = variantCodon;
    }

    private CodonChangeInformation() {
        this.referenceCodon = null;
        this.variantCodon = null;
    }
    
    @Override
    public String toString() {
        return "FunctionalChangeInformation{" + "referenceCodon=" + referenceCodon + ", variantCodon=" + variantCodon + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.referenceCodon);
        hash = 83 * hash + Objects.hashCode(this.variantCodon);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CodonChangeInformation other = (CodonChangeInformation) obj;
        if (this.referenceCodon != other.referenceCodon) {
            return false;
        }
        if (this.variantCodon != other.variantCodon) {
            return false;
        }
        return true;
    }

    public Codon getReferenceCodon() {
        return referenceCodon;
    }

    public Codon getVariantCodon() {
        return variantCodon;
    }
   
}
