/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.AccumuloRegionAnnotationRecord;
import edu.jhu.cannotate.avro.AvroGenomeSequence;
import edu.jhu.cannotate.avro.AvroTranscriptionUnitInfo;
import edu.jhu.cannotate.avro.AvroTranslator;
import edu.jhu.cannotate.avro.AvroUtils;
import edu.jhu.cannotate.ingest.CannotateLineData;
import edu.jhu.cannotate.reference.DnaWithMetaData;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.util.Map;
import java.util.function.Function;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;

/**
 * TODO: this is getting a little wonked out. Do some streamlining
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public enum AccumuloRecordType {

    FILTER(false, AccumuloFilterAnnotationRecord::new),
    GENE(true, AccumuloGeneAnnotationRecord::new),
    REGION(true, AccumuloRegionAnnotationRecord::new),
    VCF(false, AccumuloVcfRecord::new),
    VCF_INFO(false, AccumuloVcfInfoRecord::new),
    REFERENCE(true, AccumuloReferenceGenomeRecord::new);
    private final boolean multi;
    private final Function<AccumuloRecord, AccumuloRecord> constructor;

    private AccumuloRecordType(boolean multi, Function<AccumuloRecord, AccumuloRecord> constructor) {
        this.multi = multi;
        this.constructor = constructor;
    }

    public boolean isMulti() {
        return multi;
    }

    @SuppressWarnings(value = "unchecked")
    public <T extends AccumuloRecord> T buildRecord(Key key, Value value) {
        return (T) constructor.apply(new AccumuloRecord(key, value));
    }

    @SuppressWarnings("unchecked")
    public <T extends AccumuloRecord> T buildRecord(Map.Entry<Key, Value> entry) {
        return buildRecord(entry.getKey(), entry.getValue());
    }

    public static AccumuloRecord buildAccumuloRecord(Key key, Value val) {
        return new AccumuloRecord(key, val).getAnnotationType().buildRecord(key, val);
    }

    public static AccumuloRecord buildAccumuloRecord(Map.Entry<Key, Value> entry) {
        return buildAccumuloRecord(entry.getKey(), entry.getValue());
    }

    @SuppressWarnings(value = "unchecked")
    public <T extends AccumuloRecord> T buildRecord(GenomicPosition position, CannotateLineData metadata) {
        Key key = buildKey(position, metadata);
        Value val = buildValue(metadata);
        return (T) constructor.apply(new AccumuloRecord(key, val));
    }

    @SuppressWarnings(value = "unchecked")
    public <T extends AccumuloRecord> T toSubtype(AccumuloRecord parent) {
        return (T) constructor.apply(parent);
    }

    /**
     *
     * @param position - used to calculated start and stop.
     * @param ref
     * @param alt
     * @param dataId - This is identifying info about the record. Sample name,
     * or annotation name.
     * @param fileName - The source file where the data came from.
     * @return
     */
    private Key buildKey(GenomicPosition position, CannotateLineData metadata) {
        //RowId, Family, Qualifier
        //RowId: Start_Stop_Chr
        //Family: Record-Type_DataId_SourceFileName
        //Qualifier: Ref_Alt OR ExonStart_ExonStop OR ""
        //Value: AnnoInfo     (Maybe eventually?)(Original Record)
        // currently, the column family will vary from record to record.
        String rowId = position.buildRowId();
        String colFam = Utils.clean(metadata.getAnnotationType().name()) + "_" + Utils.clean(metadata.getDataId()) + "_" + Utils.clean(metadata.getFilename());
        return new Key(new Text(rowId), new Text(colFam), new Text(buildColumnQualifier(position, metadata)));
    }

    private String buildColumnQualifier(GenomicPosition position, CannotateLineData metadata) {
        if (metadata.getRecordType() == FILTER || metadata.getRecordType() == VCF || metadata.getRecordType() == VCF_INFO) {
            return position.getRef() + "_" + position.getAlt();
        } else {
            //TODO: reconsider what the qualifier should be for
            // gene and region annotations
            return "";
        }
    }

    private Value buildValue(CannotateLineData metadata) {
        if (metadata.getRecordType() == GENE) {
            try {
                TranscriptionRegionsBuilder regionsBuilder = new TranscriptionRegionsBuilder(metadata.getLine());
                @SuppressWarnings("unchecked")
                Tuple2<String, String> geneInfo = (Tuple2<String, String>) metadata.getExtraInfo().get();
                String geneName = geneInfo.get1();
                String transcriptName = geneInfo.get2();
                TranscriptonUnitInformation transcriptonUnitInformation
                        = new TranscriptonUnitInformation(geneName, transcriptName, regionsBuilder.build());
                AvroTranscriptionUnitInfo avroInfo = AvroTranslator.toAvro(transcriptonUnitInformation);
                byte[] avrobytes = AvroUtils.toByteRa(avroInfo);
                return new Value(avrobytes);
            } catch (Exception ex) {
                throw new IllegalStateException("Unable to create avro byte stream.", ex);
            }
        } else if (metadata.getRecordType() == REFERENCE) {
            @SuppressWarnings("unchecked")
            DnaWithMetaData dna = (DnaWithMetaData) metadata.getExtraInfo().get();
            AvroGenomeSequence avroRecord = AvroTranslator.toAvroDna(dna);
            return AvroUtils.toValue(avroRecord);
        } else {
            return new Value(metadata.getValue().getBytes());
        }
    }
}
