/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import java.io.Serializable;

/**
 * THIS IS ORIGINALLY FROM CIDRSEQSUITE
 *
 *
 */
public class Sample implements Serializable {

    private final String sample;
    private static final long serialVersionUID = 42;

    public Sample(String sample) {
        if (sample == null) {
            throw new NullPointerException("Sample must not be null");
        }
        this.sample = sample;
    }

    public String getName() {
        return sample;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sample) {
            Sample that = (Sample) obj;
            return this.sample.equalsIgnoreCase(that.sample);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.sample != null ? this.sample.toUpperCase().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return getName();
    }
}
