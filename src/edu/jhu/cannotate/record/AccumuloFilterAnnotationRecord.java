/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

/**
 *
 * We may wish to eventually have subtypes with more info for some database
 * entry types, which might reflect the way this data is used for real-real
 * annotations.
 *
 *
 * @created Jun 12, 2015
 */
public class AccumuloFilterAnnotationRecord extends AccumuloRecord {

    public static final AccumuloRecordType RECORD_TYPE = AccumuloRecordType.FILTER;

    public AccumuloFilterAnnotationRecord(AccumuloRecord source) {
        super(source);
    }

    //KEY:
    //START_STOP_CHR
    //REF_ALT
    //TYPE_ANNOTATION
    @Override
    public GenomicPosition getGenomicPosition() {
        return new GenomicPosition(getChromosome(), getReference(), getAlternative(), getStartPosition(), getStopPosition());
    }

}
