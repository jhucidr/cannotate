/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.avro.RegionType;
import edu.jhu.cannotate.ingest.Annovarser;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class TranscriptionRegionsBuilder {

    private final String geneDataBaseRecord;

    public TranscriptionRegionsBuilder(String geneDataBaseRecord) {
        this.geneDataBaseRecord = geneDataBaseRecord;
    }

    public Set<TranscriptonUnitInformation.TranscriptionRegion> build() {
        try {

            String[] parts = geneDataBaseRecord.split(Annovarser.DELIMITER);
            String chromosome = parts[2];

            long transcriptionStart = Long.valueOf(parts[4]);
            long transcriptionEnd = Long.valueOf(parts[5]);

            long codingStart = Long.valueOf(parts[6]);
            long codingEnd = Long.valueOf(parts[7]);

            String exonStarts = parts[9];
            String exonStops = parts[10];

            Set<TranscriptonUnitInformation.TranscriptionRegion> regions = new HashSet<>();
            //UTR_5_PRIME
            regions.add(new TranscriptonUnitInformation.TranscriptionRegion(
                    chromosome,
                    transcriptionStart,
                    codingStart,
                    RegionType.UTR_5_PRIME));
            //UTR_3_PRIME
            regions.add(new TranscriptonUnitInformation.TranscriptionRegion(
                    chromosome,
                    codingEnd,
                    transcriptionEnd,
                    RegionType.UTR_3_PRIME));

            regions.addAll(buildIntronsAndExons(chromosome, exonStarts, exonStops));

            return regions;
        } catch (Exception ex) {
            throw new IllegalStateException("There was a problem handling the following geneDataBaseRecord: <" + geneDataBaseRecord + ">", ex);
        }

    }

    private Set<TranscriptonUnitInformation.TranscriptionRegion> buildIntronsAndExons(String chromosome, String exonStarts, String exonStops) {
        String[] starts = exonStarts.split(",");
        String[] stops = exonStops.split(",");

        if (starts.length == stops.length) {
            Set<TranscriptonUnitInformation.TranscriptionRegion> regions = new HashSet<>();
            for (int i = 0; i < starts.length; i++) {
                long exonStart = Long.parseLong(starts[i]);
                long exonStop = Long.parseLong(stops[i]) - 1;
                TranscriptonUnitInformation.TranscriptionRegion exon
                        = new TranscriptonUnitInformation.TranscriptionRegion(
                                chromosome,
                                exonStart,
                                exonStop,
                                RegionType.EXON);
                regions.add(exon);
                if (i > 0) {
                    long intronStart = Long.parseLong(stops[i - 1]);
                    long intronStop = exonStart - 1;
                    TranscriptonUnitInformation.TranscriptionRegion intron
                            = new TranscriptonUnitInformation.TranscriptionRegion(
                                    chromosome,
                                    intronStart,
                                    intronStop,
                                    RegionType.INTRON);
                    regions.add(intron);
                }
            }
            return regions;
        } else {
            throw new IllegalStateException("The numbers of exon starts and stops do not match: " + exonStarts + " " + exonStops);
        }
    }

}
