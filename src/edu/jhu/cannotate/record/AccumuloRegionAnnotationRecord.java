/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate;

import edu.jhu.cannotate.record.AccumuloRecord;
import edu.jhu.cannotate.record.AccumuloRecordType;

public class AccumuloRegionAnnotationRecord extends AccumuloRecord {

    public static AccumuloRecordType RECORD_TYPE = AccumuloRecordType.REGION;

//    public AccumuloRegionAnnotationRecord(Map.Entry<Key, Value> entry) {
//        super(entry);
//    }
//    
//    public AccumuloRegionAnnotationRecord(AccumuloRecord record){
//        super(record.getEntry());
//    }
//
//    public AccumuloRegionAnnotationRecord(String rowId, String colFam, String colQual, String val) {
//        super(rowId, colFam, colQual, val);
//    }
//
//    // chr1	0 2300000	p36.33	gneg
//    public AccumuloRegionAnnotationRecord(String chr, String start, String stop, Annovarser.Type type, String annotationName, String line) {
//        super(AccumuloUtils.padInteger(start) + "_" + AccumuloUtils.padInteger(stop) + "_" + Utils.chromosomeConformist(chr), RegionType.REGION.name(), type.toString() + "_" + annotationName, line);
//    }
    public AccumuloRegionAnnotationRecord(AccumuloRecord base) {
        super(base);
    }

    @Override
    public String getAlternative() {
        return "";
    }

    @Override
    public String getReference() {
        return "";
    }

}
