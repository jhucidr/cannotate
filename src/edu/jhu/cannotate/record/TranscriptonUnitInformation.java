/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.avro.RegionType;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class TranscriptonUnitInformation {

    private final String geneName;
    private final String transcriptName;
    private final Set<TranscriptionRegion> regions;

    public TranscriptonUnitInformation(String geneName, String transcriptName, Set<TranscriptionRegion> regions) {
        this.geneName = geneName;
        this.transcriptName = transcriptName;
        this.regions = regions;
    }

    public String getGeneName() {
        return geneName;
    }

    public String getTranscriptName() {
        return transcriptName;
    }
    
    public GenomicPosition getRepresentativeGenomicPosition() {
        long start = regions.stream()
                .mapToLong(GenomicPosition::getStartPosition)
                .min()
                .getAsLong();
        long stop = regions.stream()
                .mapToLong(GenomicPosition::getStopPosition)
                .max()
                .getAsLong();
        String chr = regions.stream()
                .map(GenomicPosition::getChromosome)
                .findFirst()
                .get();
        return new GenomicPosition(chr, start, stop);
    }

    public GenomicPosition getOpenReadingFrame() {
        TranscriptionRegion utr5Region = regions.stream()
                .filter(reg -> reg.getRegionType() == RegionType.UTR_5_PRIME)
                .findFirst().get();
        TranscriptionRegion utr3Region = regions.stream()
                .filter(reg -> reg.getRegionType() == RegionType.UTR_3_PRIME)
                .findFirst().get();
        return new GenomicPosition(utr3Region.getChromosome(), utr5Region.getStartPosition(), utr3Region.getStopPosition());
    }
    
    public TranscriptonUnitInformation withUpstreamAndDownstream(long threshold) {
        Set<TranscriptionRegion> regionsWithUpstreamAndDownstream = buildUpstreamAndDownstreamRegions(threshold);
        regionsWithUpstreamAndDownstream.addAll(regions);
        return new TranscriptonUnitInformation(geneName, transcriptName, regionsWithUpstreamAndDownstream);
    }

    private Set<TranscriptionRegion> buildUpstreamAndDownstreamRegions(long threshold) {
        Set<TranscriptionRegion> upstreamAndDownstream = new HashSet<>();
        TranscriptionRegion utr5Region = regions.stream()
                .filter(reg -> reg.getRegionType() == RegionType.UTR_5_PRIME)
                .findFirst().get();
        TranscriptionRegion upstream
                = new TranscriptionRegion(
                        utr5Region.getChromosome(),
                        utr5Region.getStartPosition() - threshold,
                        utr5Region.getStartPosition(),
                        RegionType.UPSTREAM);

        TranscriptionRegion utr3Region = regions.stream()
                .filter(reg -> reg.getRegionType() == RegionType.UTR_3_PRIME)
                .findFirst().get();
        TranscriptionRegion downstream
                = new TranscriptionRegion(
                        utr3Region.getChromosome(),
                        utr3Region.getStopPosition(),
                        utr3Region.getStopPosition() + threshold,
                        RegionType.DOWNSTREAM);

        upstreamAndDownstream.add(upstream);
        upstreamAndDownstream.add(downstream);

        return upstreamAndDownstream;
    }

    public TranscriptonUnitInformation withSplicing(long splicingThreshold) {
        Set<TranscriptionRegion> regionsWithSplicing = buildSplicingRegions(splicingThreshold);
        regionsWithSplicing.addAll(regions);
        return new TranscriptonUnitInformation(geneName, transcriptName, regionsWithSplicing);
    }

    private Set<TranscriptionRegion> buildSplicingRegions(long splicingThreshold) {
        return regions.stream()
                .filter(reg -> reg.getRegionType() == RegionType.INTRON)
                .map(intron -> buildSplicingRegion(intron, splicingThreshold))
                .collect(Collectors.toSet());
    }

    private TranscriptionRegion buildSplicingRegion(TranscriptionRegion intron, long splicingThreshold) {
        return new TranscriptionRegion(
                intron.getChromosome(),
                intron.getStartPosition() - splicingThreshold,
                intron.getStartPosition() + splicingThreshold,
                RegionType.SPLICING);
    }

    public Set<TranscriptionRegion> getRegions() {
        return regions;
    }
    
    public List<TranscriptionRegion> getRegionByType(RegionType type){
        return regions.stream()
                .filter(r -> r.getRegionType().equals(type))
                .sorted()
                .collect(Collectors.toList());                
    }
    
    public String getExonicNucleotideRange(long start, long stop){
        return getRegionByType(RegionType.EXON)
                .stream()
                .sorted() //This is redundant
                .filter(r -> r.overlaps(start, stop))
                .map(r -> r.getOverlappingRange(start, stop))
                .collect(Collectors.joining());
    }

    public Set<TranscriptionRegion> determineGeneAnnotations(GenomicPosition variant) {
        return regions
                .stream()
                .filter(r -> r.overlaps(variant))
                .collect(Collectors.toSet());
    }

    public static class TranscriptionRegion extends GenomicPosition {

        private final RegionType regionType;

        public TranscriptionRegion(String chromosome, long startPosition, long stopPosition, RegionType regionType) {
            super(chromosome, startPosition, stopPosition);
            this.regionType = regionType;
        }

        public TranscriptionRegion(String chromosome, String reference, long startPosition, long stopPosition, RegionType regionType) {
            super(chromosome, reference, startPosition, stopPosition);
            this.regionType = regionType;
        }
        
        public RegionType getRegionType() {
            return regionType;
        }

        @Override
        public String toString() {
            return regionType.name() + "->" + super.toString();
        }

        public String prettyPrint() {
            return regionType + "(" + getStartPosition() + "-" + getStopPosition() + ")";
        }

        
        public TranscriptionRegion addReferenceData(String reference) {
            return new TranscriptionRegion(super.getChromosome(), reference, super.getStartPosition(), super.getStopPosition(), regionType);
        }
        
    }
}
