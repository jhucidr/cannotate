/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record.metadata;

import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.avro.AvroUtils;
import edu.jhu.cannotate.table.AccumuloTable;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.avro.specific.SpecificRecord;

/**
 *
 *
 */
//RowId, Family, Qualifier
//RowId: Filename
//Family: Fully Qaulified Path
//Qualifier: File's LastModifiedData
//Value: Set<String> sampleNames
public class MetadataRecord {

    protected final Key key;
    protected final Value value;
    protected final AvroRecordType type;

    public static final AccumuloTable TABLE = AccumuloTable.METADATA;
    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");

    //Key: 3 parts
    // Row Id - FileName?
    // Family - AvroRecordType
    // Qualifier -> upload_time
    static {
        //Create the table, if needed.
        try {
            if (false == TABLE.exists()) {
                TABLE.createTable();
            }
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static Key buildKey(File file, AvroRecordType type) {
        return new Key(file.getName(), type.toString(), "" + file.lastModified());
    }

    public MetadataRecord(Key key, Value value) {
        this.key = key;
        this.value = value;
        this.type = AvroRecordType.valueOf(key.getColumnFamily().toString());
    }

    public SpecificRecord getAvroRecord() {
        return AvroUtils.fromByteRa(getValue().get(), type.getSchema());
    }

    public MetadataRecord(File file, SpecificRecord value) throws Exception {
        this.type = AvroRecordType.getType(value);
        this.key = buildKey(file, type);
        this.value = type.toValue(value);
        //TODO: Make it easy to access the value's components?
        //TODO: Store these
    }

    public Mutation buildMutation() {
        Mutation result = AccumuloUtils.initializeMutation(key.getRow());
        result.put(key.getColumnFamily(), key.getColumnQualifier(), key.getColumnVisibilityParsed(), key.getTimestamp(), value);
        return result;
    }

    public AvroRecordType getType() {
        return AvroRecordType.valueOf(key.getColumnFamily().toString());
    }

    public Date getLastModified() {
        try {
            return SDF.parse(key.getColumnQualifier().toString());
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public final Key getKey() {
        return key;
    }

    public final Value getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "MetadataRecord{" + "key=" + key + ", value=" + value + ", type=" + type + '}';
    }

}
