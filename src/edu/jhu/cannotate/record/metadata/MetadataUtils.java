/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record.metadata;

import edu.jhu.cannotate.avro.AvroFileMetadata;
import edu.jhu.cannotate.avro.AvroGeneMetadata;
import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.avro.AvroVcfMetadata;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.table.AccumuloTable;
import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import static org.apache.avro.SchemaBuilder.record;
import org.apache.avro.specific.SpecificRecord;
import org.apache.hadoop.io.Text;

/**
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Jul 12, 2016
 */
public class MetadataUtils {

    public static void main(String... args) throws Exception {
        System.out.println(getMetadataForFile("lil_vcf.vcf"));
//        insertTestValues();

    }

    public static void insertTestValues() throws Exception {
        File inputVcf = new File("/Users/newcojd1/Documents/APL/Cannotate/Data/lil_vcf.vcf");
        List<SpecificRecord> metadata = new ArrayList<>();
        metadata.add(buildAvroFileMetadata(inputVcf));
        metadata.add(buildAvroGeneMetadata(inputVcf, new GenomicPosition("X", 125, 250)));
        metadata.add(buildAvroVcfMetadata(inputVcf, Arrays.asList("Hello", "darkness", "my", "old", "friend")));
        metadata.forEach(m -> insertMetadata(inputVcf, m));
    }

    public static void insertMetadata(File f, SpecificRecord metadata) {
        try {
            MetadataRecord metadataRecord = new MetadataRecord(f, metadata);
            AccumuloTable.METADATA.update(metadataRecord.buildMutation());
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }
    
    public static AvroVcfMetadata buildAvroVcfMetadata(File inputVcf, List<String> samples) throws Exception {
        return AvroVcfMetadata.newBuilder()
                .setFilename(inputVcf.getName())
                .setSamples(new ArrayList<>(samples))
                .build();
    }

    //This assumes that we've parsed the entire Gene Annotation File, and cached the record with the largest region.
    //We may end up doing this differently.
    public static AvroGeneMetadata buildAvroGeneMetadata(File f, GenomicPosition pos) throws Exception {
        return AvroGeneMetadata.newBuilder()
                .setFilename(f.getName())
                .setLargestRegion(pos.calculateWidth())
                .build();
    }

    public static AvroFileMetadata buildAvroFileMetadata(File f) throws Exception {
        return AvroFileMetadata.newBuilder()
                .setFileSizeInBytes(f.length())
                .setFilename(f.getName())
                .setFullyQualifiedPathToFile(f.getAbsolutePath())
                .setLastModifiedTimestamp(MetadataRecord.SDF.format(new Date(f.lastModified()))) //TODO: Date cleanup.
                .setUploadMachineName(InetAddress.getLocalHost().getHostName())
                .build();
    }

    public static Map<AvroRecordType, ? extends SpecificRecord> getMetadataForFile(String fileIdentifier) throws Exception {
        Scanner scanner = null;
        try {
            scanner = AccumuloTable.METADATA.buildScanner();
            scanner.setRange(Range.exact(fileIdentifier));
            Map<AvroRecordType, SpecificRecord> result = new HashMap<>();
            for (Entry<Key, Value> e : scanner) {
                MetadataRecord record = new MetadataRecord(e.getKey(), e.getValue());
                result.put(record.getType(), record.getAvroRecord());
            }
            return result;
        } finally {
            scanner.close();

        }

    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> getMetadataByType(AvroRecordType type) throws Exception {
        List<MetadataRecord> metadata = getAllThatMetadata(type);
        return metadata.stream()
                .map(MetadataRecord::getAvroRecord)
                .map(r -> (T) r) //Do that cast!
                .collect(Collectors.toList());
    }

    public static List<MetadataRecord> getAllThatMetadata(AvroRecordType... types) throws Exception {
        Scanner scanner = null;
        try {
            scanner = AccumuloTable.METADATA.buildScanner();
            for (AvroRecordType type : types) {
                scanner.fetchColumnFamily(new Text(type.toString()));
            }
            List<MetadataRecord> result = new ArrayList<>();
            scanner.forEach(e -> result.add(new MetadataRecord(e.getKey(), e.getValue())));
            return result;
        } finally {
            scanner.close();
        }
    }

}
