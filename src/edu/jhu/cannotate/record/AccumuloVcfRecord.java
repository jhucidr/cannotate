/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.ingest.CannotateLineData;
import edu.jhu.cannotate.ingest.ParsableMultiSampleVcfFile;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.hadoop.io.Text;

public class AccumuloVcfRecord extends AccumuloRecord {

    public static AccumuloRecordType RECORD_TYPE = AccumuloRecordType.VCF;

    public AccumuloVcfRecord(AccumuloRecord base) {
        super(base);
    }

//
//    private static Key buildKey(String chromosome, long position, String ref, String alt, String sampleName) {
//        String start = AccumuloUtils.padInteger(position);
//        String stop = AccumuloUtils.padInteger(calcStop(ref, alt, position));
//        String rowId = start + "_" + stop + "_" + Utils.chromosomeConformist(chromosome);
//        String colFam = ref + "_" + alt;
//        String colQual = RECORD_TYPE.toString() + "_" + sampleName;
//        return new Key(new Text(rowId), new Text(colFam), new Text(colQual));
//    }
//TODO: Filter/Handle/Ignore CNV lines (Copy Number Varients). These can be identified by a lack of an alt,
    //<CN0>,<CN2>,<CN3>,<CN4> will be in place of the alt.
    //You can find the file lines by searching for "SVTYPE=CNV" in the info string.

    public static String calcStop(String ref, String alt, long position) {
        if (alt.length() >= ref.length()) {
            return position + alt.length() - 1 + "";
        } else {
            return "" + position;
        }
    }

    @Override
    public GenomicPosition getGenomicPosition() {
        return new GenomicPosition(getChromosome(), getReference(), getAlternative(), getStartPosition(), getStopPosition());
    }

    public Text getRow() {
        return key.getRow();
    }

    public String getSampleName() {
        return Utils.after(key.getColumnQualifier().toString(), "_");
    }


    public static List<AccumuloVcfRecord> fromVcfRecord(String vcfFileName, ParsableMultiSampleVcfFile.Record record) {
        return record.getSampleMap()
                .entrySet()
                .stream()
                .map(e -> new VcfCannotateLineDataFactory(record, e.getKey(), vcfFileName))
                .flatMap(VcfCannotateLineDataFactory::stream)
                .map(tuple -> (AccumuloVcfRecord) AccumuloRecordType.VCF.buildRecord(tuple.get1(), tuple.get2()))
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "" + key + " = " + value;
    }

    public static class VcfCannotateLineDataFactory {

        private final ParsableMultiSampleVcfFile.Record record;
        private final Sample sample;
        private final String fileName;

        public VcfCannotateLineDataFactory(ParsableMultiSampleVcfFile.Record record, Sample sample, String fileName) {
            this.record = record;
            this.sample = sample;
            this.fileName = fileName;
        }

        private VcfDataType buildDataType(GenomicPosition pos) {
            return new VcfDataType(pos, record, sample);
        }

        public Stream<Tuple2<GenomicPosition, CannotateLineData>> stream() {
            return record.retrieveSampleLevelGenomicPositions(sample)
                    .stream()
                    .map(this::buildDataType)
                    .map(vdt -> vdt.retrieveGenomicPositionAndLineData(fileName));

        }

    }

    public static class VcfDataType implements DataType {

        private final GenomicPosition position;
        private final ParsableMultiSampleVcfFile.Record record;
        private final Sample sample;
        //static instance
        public static final VcfDataType INSTANCE = new VcfDataType() {

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                throw new IllegalStateException("No genomic position data available for static instance.");
            }

        };

        private VcfDataType() {
            this.position = null;
            this.record = null;
            this.sample = null;
        }

        public VcfDataType(GenomicPosition position, ParsableMultiSampleVcfFile.Record record, Sample sample) {
            this.position = position;
            this.record = record;
            this.sample = sample;
        }

        @Override
        public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
            info.setDataId(sample.getName());
            info.setValue(record.getSampleMap().get(sample).getGenotypeAndTheKitchenSink());
            return position;
        }

        @Override
        public AccumuloRecordType getRecordType() {
            return AccumuloRecordType.VCF;
        }

        @Override
        public String name() {
            return AccumuloRecordType.VCF.name();
        }

        public Tuple2<GenomicPosition, CannotateLineData> retrieveGenomicPositionAndLineData(String fileName) {
            CannotateLineData cannotateLineData = new CannotateLineData(fileName, this, record.getSampleMap().get(sample).getGenotypeAndTheKitchenSink());
            return new Tuple2<>(buildGenomicPositionAndPopulateLineData(cannotateLineData), cannotateLineData);
        }

    }

}
