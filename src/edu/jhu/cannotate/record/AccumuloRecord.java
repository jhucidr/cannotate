/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import com.google.common.collect.Maps;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;

//RowId, Family, Qualifier
//RowId: Start_Stop_Chr
//Family: Record-Type_DataId_SourceFileName
//Qualifier: Ref_Alt OR ExonStart-ExonStop OR ""
//Value: AnnoInfo     (Maybe eventually?)(Original Record)
// currently, the column family will vary from record to record.
public class AccumuloRecord {

    protected final Key key;
    protected final Value value;
    protected final DataType datatype;

    /**
     * This is NOT a copy constructor.
     *
     * @param base
     */
    public AccumuloRecord(AccumuloRecord base) {
        this.key = base.key;
        this.value = base.value;
        this.datatype = base.datatype;
    }

    public AccumuloRecord(Key key, Value value) {
        this.key = key;
        this.value = value;
        //This is somewhat expensive, mostly because of cleanUP().
        //TODO: Consider calculating this value as needed, or passing it into the constructor.
        this.datatype = DataType.fromString(Utils.before(key.getColumnFamily().toString(), "_"));
    }

    public String getReference() {
        return key.getColumnQualifier().toString().split("_")[0];
    }

    public String getAlternative() {
        return key.getColumnQualifier().toString().split("_")[1];
    }

    public GenomicPosition getGenomicPosition() {
        return new GenomicPosition(getChromosome(), getReference(), getAlternative(), getStartPosition(), getStopPosition());
    }
    
    public Text getRowId() {
        return key.getRow();
    }

    public Text getFamily() {
        return key.getColumnFamily();
    }

    public Text getQualifier() {
        return key.getColumnQualifier();
    }

    public Value getValue() {
        return value;
    }

    public Map.Entry<Key, Value> getEntry() {
        return Maps.immutableEntry(key, value);
    }

    public String getChromosome() {
        return key.getRow().toString().split("_")[2];
    }

    public String getStartPosition() {
        return Utils.before(key.getRow().toString(), "_");
    }

    public long getStartAsLong() {
        return Long.parseLong(getStartPosition());
    }

    public String getStopPosition() {
        return key.getRow().toString().split("_")[1];
    }

    public long getStopAsLong() {
        return Long.parseLong(getStopPosition());
    }

    public String getRecordType() {
        return Utils.before(key.getColumnFamily().toString(), "_");
    }

    //DATA ID is the annotation data OR the sample identifier
    public String getDataId() {
        return key.getColumnFamily().toString().split("_")[1];
    }

    public String getSourceFileName() {
        return key.getColumnFamily().toString().split("_")[2];
    }

    public Mutation buildMutation() {
        Mutation result = AccumuloUtils.initializeMutation(key.getRow());
        result.put(key.getColumnFamily(), key.getColumnQualifier(), key.getColumnVisibilityParsed(), key.getTimestamp(), value);
        return result;
    }

    public List<Mutation> buildChildishMutations() {
        return new ArrayList<>();
    }

    public DataType getDatabaseType() {
        return datatype;
    }

    public AccumuloRecordType getAnnotationType() {
        return getDatabaseType().getRecordType();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.key);
        hash = 17 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AccumuloRecord other = (AccumuloRecord) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + "key=" + key + ", value=" + value + '}';
    }



}
