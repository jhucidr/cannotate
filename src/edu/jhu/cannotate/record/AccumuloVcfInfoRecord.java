/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.ingest.Annovarser;
import edu.jhu.cannotate.ingest.CannotateLineData;
import edu.jhu.cannotate.ingest.ParsableMultiSampleVcfFile;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.accumulo.core.data.Mutation;
import org.apache.hadoop.io.Text;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AccumuloVcfInfoRecord extends AccumuloRecord {

    public AccumuloVcfInfoRecord(AccumuloRecord base) {
        super(base);
    }

    public Text getRow() {
        return key.getRow();
    }

    @Override
    public Mutation buildMutation() {
        Mutation result = AccumuloUtils.initializeMutation(key.getRow());
        result.put(key.getColumnFamily(), key.getColumnQualifier(), key.getColumnVisibilityParsed(), key.getTimestamp(), value);
        return result;
    }

    public static List<AccumuloVcfInfoRecord> fromVcfRecord(String vcfFileName, ParsableMultiSampleVcfFile.Record record) {
        return record.getAlts().stream().map(alt -> {
            String stop = AccumuloVcfRecord.calcStop(record.getRef(), alt, record.getPosition());
            String ref = record.getRef();
            GenomicPosition gp = new GenomicPosition(record.getChromosome(), ref, alt, record.getPosition(), Long.parseLong(stop));
            Tuple2<GenomicPosition, CannotateLineData> tupple = new VcfInfoDataType(gp, record).retrieveGenomicPositionAndLineData(vcfFileName);
            return (AccumuloVcfInfoRecord) AccumuloRecordType.VCF_INFO.buildRecord(tupple.get1(), tupple.get2());
        }).collect(Collectors.toList());
    }
    
    @Override
    public String toString() {
        return "" + key + " = " + value;
    }

    public static class VcfInfoDataType implements DataType {

        private final GenomicPosition position;
        private final ParsableMultiSampleVcfFile.Record record;
        //static instance
        public static final VcfInfoDataType INSTANCE = new VcfInfoDataType() {

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                throw new IllegalStateException("No genomic position data available for static instance.");
            }

        };

        private VcfInfoDataType() {
            this.position = null;
            this.record = null;
        }

        public VcfInfoDataType(GenomicPosition position, ParsableMultiSampleVcfFile.Record record) {
            this.position = position;
            this.record = record;
        }

        @Override
        public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
            info.setDataId(record.getId());
            info.setValue(record.getInfo());                        
            return position;
        }

        @Override
        public AccumuloRecordType getRecordType() {
            return AccumuloRecordType.VCF_INFO;
        }

        @Override
        public String name() {
            return "VCF_INFO";
        }

        public Tuple2<GenomicPosition, CannotateLineData> retrieveGenomicPositionAndLineData(String fileName) {
            CannotateLineData cannotateLineData = new CannotateLineData(fileName, this, record.getInfo());
            return new Tuple2<>(buildGenomicPositionAndPopulateLineData(cannotateLineData), cannotateLineData);
        }

    }

}
