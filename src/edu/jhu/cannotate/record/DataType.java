/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.ingest.Annovarser;
import edu.jhu.cannotate.ingest.CannotateLineData;
import edu.jhu.cannotate.util.Utils;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 */
public interface DataType {

    public AccumuloRecordType getRecordType();

    public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info);

    public default boolean isValidRecord(String line) {
        return true;
    }

    public static DataType fromString(String s) {
        return DataTypeUtils.fromString(s);
    }

    public String name();

    static class DataTypeUtils {

        private static final Map<String, DataType> FROM_STRING;

        private static void addToMap(DataType toAdd) {
            FROM_STRING.put(Utils.cleanUP(toAdd.name()), toAdd);
        }

        static {
            FROM_STRING = new HashMap<>();
            for (Annovarser.AnnotationSource t : Annovarser.AnnotationSource.values()) {
                addToMap(t);
            }
            addToMap(AccumuloVcfRecord.VcfDataType.INSTANCE);
            addToMap(AccumuloVcfInfoRecord.VcfInfoDataType.INSTANCE);
            addToMap(AccumuloReferenceGenomeRecord.ReferenceDataType.INSTANCE);
        }

        public static DataType fromString(String s) {
            return FROM_STRING.get(Utils.cleanUP(s));
        }

    }

}
