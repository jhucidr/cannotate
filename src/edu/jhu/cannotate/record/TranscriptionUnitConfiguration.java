/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class TranscriptionUnitConfiguration {
    private final int upstreamAndDownstreamWidth;
    private final int splicingThreshold;
    
    public static final TranscriptionUnitConfiguration DEFAULT = new TranscriptionUnitConfiguration(100, 3);

    public TranscriptionUnitConfiguration(int upstreamAndDownstreamWidth, int splicingThreshold) {
        this.upstreamAndDownstreamWidth = upstreamAndDownstreamWidth;
        this.splicingThreshold = splicingThreshold;
    }

    public int getSplicingThreshold() {
        return splicingThreshold;
    }

    public int getUpstreamAndDownstreamWidth() {
        return upstreamAndDownstreamWidth;
    }
    
}