/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.avro.AvroGenomeSequence;
import edu.jhu.cannotate.avro.AvroTranslator;
import edu.jhu.cannotate.avro.AvroUtils;
import edu.jhu.cannotate.ingest.CannotateLineData;
import edu.jhu.cannotate.reference.DnaWithMetaData;
import edu.jhu.cannotate.util.dna.Dna;
import java.io.File;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AccumuloReferenceGenomeRecord extends AccumuloRecord {

    public AccumuloReferenceGenomeRecord(AccumuloRecord base) {
        super(base);
    }

    public DnaWithMetaData getDnaWithMetadata() {
        AvroGenomeSequence ags = AvroUtils.fromByteRa(value.get(), AvroGenomeSequence.getClassSchema());
        return AvroTranslator.fromAvroDna(ags);
    }

    public Dna getDna() {
        return getDnaWithMetadata().getDna();
    }

    public static AccumuloReferenceGenomeRecord buildReferenceGenomeRecord(File referenceFile, DnaWithMetaData dna) {
        GenomicPosition pos = dna.getGenomicPosition();
        ReferenceDataType dataType = new ReferenceDataType(pos, dna);
        CannotateLineData lineData = dataType.buildLineData(referenceFile.getName());
        return AccumuloRecordType.REFERENCE.buildRecord(pos, lineData);
    }

    @Override
    public String toString() {
        return key.toString() + "\n" + getDnaWithMetadata().toString();
    }

    public static class ReferenceDataType implements DataType {

        private final GenomicPosition position;
        private final DnaWithMetaData dna;
        private static final String NO_LINE = "NO_LINE";
        public static final ReferenceDataType INSTANCE = new ReferenceDataType();

        private ReferenceDataType() {
            this.position = null;
            this.dna = null;
        }

        public ReferenceDataType(GenomicPosition position, DnaWithMetaData dna) {
            this.position = position;
            this.dna = dna;
        }

        @Override
        public AccumuloRecordType getRecordType() {
            return AccumuloRecordType.REFERENCE;
        }

        private void populateLineDate(CannotateLineData result) {
            result.setExtraInfo(dna);
            //allowing value and info to remain "nothing" for now.
            //TODO: When/if we need to populate the line data, do it here.
        }

        public CannotateLineData buildLineData(String fileName) {
            CannotateLineData result = new CannotateLineData(fileName, this, NO_LINE);
            populateLineDate(result);
            return result;
        }

        @Override
        public String name() {
            return "REFERENCE";
        }

        @Override
        public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
            populateLineDate(info);
            return position;
        }

    }

}
