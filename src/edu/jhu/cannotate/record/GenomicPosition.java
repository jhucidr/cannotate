/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.reference.ReferenceIngester;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;
import org.apache.accumulo.core.data.Range;

public class GenomicPosition implements Comparable<GenomicPosition> {

    private final String chromosome;
    private final Optional<String> ref;
    private final Optional<String> alt;
    private final long startPosition;
    private final long stopPosition;    

    public static void testSorting() {
        List<GenomicPosition> pos = new ArrayList<>();
        IntStream.range(0, 2500)
                .forEach(i -> pos.add(buildRandomForTesting()));
        Collections.sort(pos);
        pos.forEach(System.out::println);
    }

    private static final Random r = new Random();

    private static GenomicPosition buildRandomForTesting() {
        int chr = r.nextInt(23) + 1;
        long startPos = r.nextInt(10_000);
        long stopPos = startPos + r.nextInt(1000);
        return new GenomicPosition("" + chr, startPos, stopPos);
    }

    public GenomicPosition(String chromosome, String startPosition, String stopPosition) {
        this(chromosome, Long.parseLong(startPosition), Long.parseLong(stopPosition));
    }

    public GenomicPosition(String chromosome, long startPosition, long stopPosition) {
        this.chromosome = Utils.chromosomeConformist(chromosome);
        this.ref = Optional.empty();
        this.alt = Optional.empty();
        this.startPosition = startPosition;
        this.stopPosition = stopPosition;
    }

    public GenomicPosition(String chromosome, String ref, long startPosition, long stopPosition) {
        this.chromosome = Utils.chromosomeConformist(chromosome);
        this.ref = Optional.of(ref);
        this.alt = Optional.empty();
        this.startPosition = startPosition;
        this.stopPosition = stopPosition;
    }

    public GenomicPosition(String chromosome, String ref, String alt, String startPosition) {
        this(chromosome, ref, alt, Long.parseLong(startPosition));
    }

    public GenomicPosition(String chromosome, String ref, String alt, long startPosition) {
        this(chromosome, ref, alt, startPosition, calcStop(ref, alt, startPosition));
    }

    public GenomicPosition(String chromosome, String ref, String alt, String startPosition, String stopPosition) {
        this(chromosome, ref, alt, Long.parseLong(startPosition), Long.parseLong(stopPosition));
    }

    public GenomicPosition(String chromosome, String ref, String alt, long startPosition, long stopPosition) {
        this.chromosome = Utils.chromosomeConformist(chromosome);
        this.ref = Optional.of(ref);
        this.alt = Optional.of(alt);
        this.startPosition = startPosition;
        this.stopPosition = stopPosition;
    }

    @Override
    public String toString() {
        return "GenomicPosition{" + "chromosome=" + chromosome + ", ref=" + ref + ", alt=" + alt + ", startPosition=" + startPosition + ", stopPosition=" + stopPosition + '}';
    }

    public long calculateWidth() {
        return stopPosition - startPosition + 1;
    }

    public String getChromosome() {
        return chromosome;
    }

    public String getPaddedStartPosition() {
        return AccumuloUtils.padInteger(startPosition);
    }

    public String getPaddedStopPosition() {
        return AccumuloUtils.padInteger(stopPosition);
    }

    public String buildRowId() {
        return new StringBuilder()
                .append(getPaddedStartPosition()).append("_")
                .append(getPaddedStopPosition()).append("_")
                .append(getChromosome()).toString();
    }

    private static final String OR_ELSE = "NA";

    public String getRef() {
        return ref.orElse(OR_ELSE);
    }

    /**
     * Adjusts the range passed in to the bounds of this Genomic Position The
     * returned string is stop inclusive.
     */
    public String getOverlappingRange(long start, long stop) {
        return getStringRange(Math.max(start, getStartPosition()), Math.min(stop, getStopPosition()));
    }

    /**
     * Returns the String reference that matches the specified genomic range.
     * returns a "stop inclusive" range.
     */
    public String getStringRange(long start, long stop) {
        start -= getStartPosition();
        stop -= getStartPosition();
//        System.out.println("start = " + start);
//        System.out.println("stop = " + stop);
//        System.out.println("ref.length() = " + ref.get().length());
//        System.out.println("ref = " + ref);
//        System.out.println("calculateWidth() = " + calculateWidth());
        return ref.get().substring((int) start, (int) stop + 1);
    }

    public String getAlt() {
        return alt.orElse(OR_ELSE);
    }

    public long getStartPosition() {
        return startPosition;
    }

    public long getStopPosition() {
        return stopPosition;
    }

    public boolean hasAltAndRef() {
        return ref.isPresent() && alt.isPresent();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.chromosome);
        hash = 29 * hash + Objects.hashCode(this.ref);
        hash = 29 * hash + Objects.hashCode(this.alt);
        hash = 29 * hash + (int) (this.startPosition ^ (this.startPosition >>> 32));
        hash = 29 * hash + (int) (this.stopPosition ^ (this.stopPosition >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenomicPosition other = (GenomicPosition) obj;
        if (!Objects.equals(this.chromosome, other.chromosome)) {
            return false;
        }
        if (this.startPosition != other.startPosition) {
            return false;
        }
        if (this.stopPosition != other.stopPosition) {
            return false;
        }
        if (!Objects.equals(this.ref, other.ref)) {
            return false;
        }
        if (!Objects.equals(this.alt, other.alt)) {
            return false;
        }
        return true;
    }

    private static long calcStop(String ref, String alt, long position) {
        if (alt.length() >= ref.length()) {
            return position + alt.length() - 1;
        } else {
            //deletion or SNV
//           return position + 1 + "";
            //Use closed intervals instead of open.
            return position;
        }
    }

    public boolean isASpan() {
        return false == (startPosition == stopPosition);
    }

    private boolean contains(long location) {
        return startPosition <= location && stopPosition >= location;
    }

    public boolean overlaps(long start, long stop) {
        return contains(start) || contains(stop);
    }

    public boolean overlaps(GenomicPosition that) {
        if (this.chromosome.equalsIgnoreCase(that.chromosome)) {
            boolean result = this.contains(that.startPosition)
                    || this.contains(that.stopPosition)
                    || that.contains(this.stopPosition)
                    || that.contains(this.startPosition);
            return result;
        }
        return false;
    }

    public boolean isLessThan(GenomicPosition other) {
        return this.stopPosition < other.startPosition;
    }
    
    public long getDnaLength(){
        return this.stopPosition - this.startPosition;
    }

    public Range getReferenceRange() {
        int dnaLenth = ReferenceIngester.getDnaLength();
        long startRowNum = startPosition - (startPosition % dnaLenth);
        String startRow = AccumuloUtils.padInteger(startRowNum) + "_" + AccumuloUtils.padInteger(startRowNum + dnaLenth) + "_" + chromosome;
        long stopRowNum = stopPosition + dnaLenth - (stopPosition % dnaLenth);
        String stopRow = AccumuloUtils.padInteger(stopRowNum) + "_" + AccumuloUtils.padInteger(stopRowNum + dnaLenth) + "_" + chromosome;
        return new Range(startRow, stopRow);
    }

    @Override
    public int compareTo(GenomicPosition o) {
        int result = this.getChromosome().compareToIgnoreCase(o.getChromosome());
        if (result != 0) {
            return result;
        }
        result = Utils.signum(this.getStartPosition(), o.getStartPosition());
        if (result != 0) {
            return result;
        }
        return Utils.signum(this.getStopPosition(), o.getStopPosition());
    }
}
