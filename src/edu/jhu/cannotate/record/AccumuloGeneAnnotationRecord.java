/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.record;

import edu.jhu.cannotate.avro.AvroTranscriptionUnitInfo;
import edu.jhu.cannotate.avro.AvroTranslator;
import edu.jhu.cannotate.avro.AvroUtils;
import edu.jhu.cannotate.ingest.CannotateLineData;
import java.util.Objects;

public class AccumuloGeneAnnotationRecord extends AccumuloRecord {

    public static final AccumuloRecordType RECORD_TYPE = AccumuloRecordType.GENE;
    private TranscriptonUnitInformation transcriptonUnitInformation;

    public AccumuloGeneAnnotationRecord(AccumuloRecord base) {
        super(base);
    }

    public CannotateLineData buildLineData(GenomicPosition region) {
        CannotateLineData result = new CannotateLineData(getSourceFileName(), getDatabaseType(), "");
//        result.setDataId(type.toString());
        result.setValue(getValue().toString());
        return result;
    }

    @Override
    public String getAlternative() {
        return "";
    }

    @Override
    public String getReference() {
        return "";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.key);
        hash = 71 * hash + Objects.hashCode(this.value);
//        hash = 71 * hash + Objects.hashCode(this.exon);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AccumuloGeneAnnotationRecord other = (AccumuloGeneAnnotationRecord) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    public AvroTranscriptionUnitInfo getTranscriptionRecord() {
        return AvroUtils.fromByteRa(getValue().get(), AvroTranscriptionUnitInfo.getClassSchema());
    }

    public void configureTranscriptionUnitInformation(TranscriptionUnitConfiguration conf) {
        if (transcriptonUnitInformation == null) {
            transcriptonUnitInformation = AvroTranslator.fromAvro(getTranscriptionRecord())
                    .withSplicing(conf.getSplicingThreshold())
                    .withUpstreamAndDownstream(conf.getUpstreamAndDownstreamWidth());
        } else {
            return;
        }
    }

    public TranscriptonUnitInformation getTranscriptonUnitInformation() {
        if (transcriptonUnitInformation != null) {
            return transcriptonUnitInformation;
        } else {
            throw new IllegalStateException("Transcription Unit Information has not been configured.");
        }

    }

    public GenomicPosition getWidenedGenomicPosition() {
        if (transcriptonUnitInformation != null) {
            return transcriptonUnitInformation.getRepresentativeGenomicPosition();
        } else {
            throw new IllegalStateException("Transcription Unit Information has not been configured.");
        }
    }

}
