/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.example;

import edu.jhu.cannotate.record.CannotateRecord;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

public class AvroExample {

    public static void main(String... args) throws Exception {
//        fileSystemExample();
        byteRaExample();
    }

    public static void byteRaExample() throws Exception {
        CannotateRecord record = generateRecord();
        System.out.println("record = " + record);
        byte[] recordBytes = toByteRa(record);
        CannotateRecord record2 = fromByteRa(recordBytes);
        System.out.println("record2 = " + record2);
    }

    public static void fileSystemExample() throws Exception {
        CannotateRecord record = generateRecord();
        System.out.println("record = " + record);
        File written = toFile(record);
        readRecord(written);
    }

    public static byte[] toByteRa(CannotateRecord user) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
        DatumWriter<CannotateRecord> writer = new SpecificDatumWriter<>(CannotateRecord.getClassSchema());

        writer.write(user, encoder);
        encoder.flush();
        out.close();
        return out.toByteArray();
    }

    public static CannotateRecord fromByteRa(byte[] recordRa) throws Exception {
        SpecificDatumReader<CannotateRecord> reader = new SpecificDatumReader<>(CannotateRecord.getClassSchema());
        Decoder decoder = DecoderFactory.get().binaryDecoder(recordRa, null);
        return reader.read(null, decoder);
    }

    public static CannotateRecord generateRecord() {
        return CannotateRecord.newBuilder()
                .setAlt("TC")
                .setChr("1")
                .setIdentifier("A Lovely Record")
                .setRef("AC")
                .setStart(0)
                .setStop(0)
                .build();
    }

    public static File toFile(CannotateRecord toStore) throws Exception {
        return toFile(Arrays.asList(toStore));
    }

    public static File toFile(Collection<CannotateRecord> toStore) throws Exception {
        File outputFile = new File("records.ser");
        DatumWriter<CannotateRecord> userDatumWriter = new SpecificDatumWriter<>(CannotateRecord.class);
        try (DataFileWriter<CannotateRecord> dataFileWriter = new DataFileWriter<>(userDatumWriter)) {
            dataFileWriter.create(toStore.iterator().next().getSchema(), outputFile);
            toStore.stream().forEach(r -> {
                try {
                    dataFileWriter.append(r);
                } catch (Exception ex) {
                    throw new IllegalStateException(ex);
                }
            });
        }
        return outputFile;
    }

    public static List<CannotateRecord> readRecord(File file) throws Exception {
        // Deserialize Users from disk
        List<CannotateRecord> result = new ArrayList<>();
        DatumReader<CannotateRecord> userDatumReader = new SpecificDatumReader<>(CannotateRecord.class);
        try (DataFileReader<CannotateRecord> dataFileReader = new DataFileReader<>(file, userDatumReader)) {
            while (dataFileReader.hasNext()) {
// Reuse user object by passing it to next(). This saves us from
// allocating and garbage collecting many objects for files with
// many items.
                result.add(dataFileReader.next());
            }
        }
        return result;
    }
}
