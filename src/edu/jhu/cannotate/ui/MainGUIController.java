/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.ui;

import edu.jhu.cannotate.ui.config.ConfigGUIController;
import edu.jhu.cannotate.ui.dispatch.DispatcherGUIController;
import java.io.File;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author greenmk1
 */
public class MainGUIController {

    @FXML
    ConfigGUIController configGUIController;

    private TextField configFileTextField;

    @FXML
    private AnchorPane childPane;

    public TextField getConfigFileTextField() {
        return this.configFileTextField;
    }

    public void setConfigFileTextField(TextField newFilePath) {
        this.configFileTextField = newFilePath;
    }

    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        configFileTextField = null;
        childPane.getChildren().clear();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("config/ConfigGUI.fxml"));

        try {
            Parent configGUI = loader.load();
            ConfigGUIController configController = loader.getController();
            configController.setParentController(this);
            childPane.getChildren().add(configGUI);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void loadDispatchGUI(ActionEvent event) throws Exception {
        childPane.getChildren().clear();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("dispatch/DispatcherGUI.fxml"));
        Parent gui = loader.load();
        
        DispatcherGUIController controller = loader.getController();
        controller.setParentController(this);
        childPane.getChildren().add(gui);
    }

}
