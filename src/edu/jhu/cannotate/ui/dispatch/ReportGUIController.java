/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.ui.dispatch;

import edu.jhu.cannotate.dispatch.Dispatcher;
import edu.jhu.cannotate.util.FxUtils;
import edu.jhu.cannotate.ui.MainGUIController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author greenmk1
 */
public class ReportGUIController implements Initializable {

    private MainGUIController parentController;

    @FXML
    private TextField reportPathTextField;

    @FXML
    private Text statusTextBox;

    @FXML
    void reportBrowseButton(ActionEvent event) {
        FxUtils.insertFilePathIfSelected(reportPathTextField);
    }

    @FXML
    void reportSubmitButton(ActionEvent event) throws Exception {
        System.out.println("Creating Report...");

        if (reportPathTextField.getText().equals("")) {
            //this is different. dont check if file is good, check if file doesnt exist
            statusTextBox.setText("Problem with submission - nothing inserted.");
            throw new IllegalStateException(reportPathTextField.getText() + " is not a file.");
        }
        Dispatcher.dispatch(new String[]{"-generate_report",
            this.parentController.getConfigFileTextField().getText(),
            reportPathTextField.getText()});

        statusTextBox.setText("Success!");
    }

    public void setParentController(MainGUIController parentController) {
        this.parentController = parentController;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        statusTextBox.setText("Warning: this will overwrite any existing file with the same name.");

    }

}
