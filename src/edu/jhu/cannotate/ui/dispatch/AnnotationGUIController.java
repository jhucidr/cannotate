/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.ui.dispatch;

import edu.jhu.cannotate.dispatch.Dispatcher;
import edu.jhu.cannotate.ingest.Annovarser;
import edu.jhu.cannotate.ingest.Annovarser.AnnotationSource;
import edu.jhu.cannotate.util.FxUtils;
import edu.jhu.cannotate.ui.MainGUIController;
import edu.jhu.cannotate.util.Utils;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author greenmk1
 */
public class AnnotationGUIController implements Initializable {

    @FXML
    private TextField annotationPathTextField;

    @FXML
    private Text statusTextBox;

    @FXML
    private MenuButton annotationTypeMenuButton;

    private MainGUIController parentController;

    private AnnotationSource annotationType;

    @FXML
    void annotationBrowseButton(ActionEvent event) {
        FxUtils.insertFilePathIfSelected(annotationPathTextField);
    }

    @FXML
    void annotationSubmitButton(ActionEvent event) throws Exception {
        System.out.println("Submitting Annotation path...");

        String fileName = "";

        if (annotationType.equals("")) {
            statusTextBox.setText("Problem with submission - no Annotation Type selected.");
            throw new IllegalStateException("No annotation type selected.");
        }

        if (annotationPathTextField.getText().equals("")) {
            fileName = Utils.translateEnumNameToCamelCase(annotationType.name());
            //this is incorrect
//            Process p = Runtime.getRuntime().exec("./annotate_variation.pl -downdb -webfrom annovar " + fileName + " ./down_db");
            annotationPathTextField.setText("down_db\\hg18_" + fileName.toLowerCase() + ".txt");
        } else {
            if (!FxUtils.checkValidFile(annotationPathTextField)) {
                statusTextBox.setText("Problem with submission - invalid path.");
                throw new IllegalStateException(annotationPathTextField.getText() + " is not a valid file.");
            }
        }

        System.out.println(annotationType.name());
        System.out.println(annotationPathTextField.getText());
//        Dispatcher.dispatch(new String[]{"-ingest_annotation",
//            this.parentController.getConfigFileTextField().getText(),
//            annotationType.name(),
//            annotationPathTextField.getText()});

        statusTextBox.setText("Success!");
    }

    public void setParentController(MainGUIController parentController) {
        this.parentController = parentController;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        statusTextBox.setText("If path is left blank, the program will automatically download and ingest the selected Annotation Type - only works for Annotate files. Still incomplete.");
        annotationTypeMenuButton.getItems().clear();

        Annovarser.AnnotationSource annoTypes[] = Annovarser.AnnotationSource.values();
        for (Annovarser.AnnotationSource type : annoTypes) {
            MenuItem currType = new MenuItem(type.toString());
            //toString is to human readable, .name() is enum name -> annotationName
            currType.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    annotationTypeMenuButton.setText(type.toString());
                    annotationType = type;
                }
            });
            try {
                if (FxUtils.checkMetadataTable(type.name())) {
                    currType.setDisable(true);
                }
            } catch (Exception e) {
                System.err.println("Error with obtaining enum name from " + type.toString());
                e.printStackTrace();

            }
            annotationTypeMenuButton.getItems().add(currType);
//            System.out.println(Utils.translateEnumNameToCamelCase(type.name()));
//            System.out.println(Annovarser.AnnotationSource.valueOf(type.name()));
        }

    }

}
