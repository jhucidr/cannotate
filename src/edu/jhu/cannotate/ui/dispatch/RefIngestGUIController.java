/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.ui.dispatch;

import edu.jhu.cannotate.dispatch.Dispatcher;
import edu.jhu.cannotate.util.FxUtils;
import edu.jhu.cannotate.ui.MainGUIController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author greenmk1
 */
public class RefIngestGUIController implements Initializable {

    @FXML
    private TextField refPathTextField;

    @FXML
    private Text statusTextBox;

    private MainGUIController parentController;

    @FXML
    void RefIngestBrowseButton(ActionEvent event) {
        FxUtils.insertFilePathIfSelected(refPathTextField);
    }

    @FXML
    void refIngestSubmitButton(ActionEvent event) throws Exception {
        System.out.println("Submitting Reference path...");

        if (!FxUtils.checkValidFile(refPathTextField)) {
            statusTextBox.setText("Problem with submission - invalid path");
            throw new IllegalStateException(refPathTextField.getText() + " is not a valid file.");
        }
        Dispatcher.dispatch(new String[]{"-ingest_reference",
            this.parentController.getConfigFileTextField().getText(),
            refPathTextField.getText()});

        statusTextBox.setText("Success!");
    }

    public void setParentController(MainGUIController parentController) {
        this.parentController = parentController;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        statusTextBox.setText("");
    }

}
