/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.ui.dispatch;

import edu.jhu.cannotate.dispatch.AnnotationReaderDispatchee;
import edu.jhu.cannotate.dispatch.MultiAnnotationReaderDispatchee;
import edu.jhu.cannotate.dispatch.ReferenceIngestDispatchee;
import edu.jhu.cannotate.dispatch.ReportGenerationDispatchee;
import edu.jhu.cannotate.dispatch.VcfReaderDispatchee;
import edu.jhu.cannotate.ui.MainGUIController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;

/**
 * FXML Controller class
 *
 * @author greenmk1
 */
public class DispatcherGUIController implements Initializable {

    @FXML
    private Tooltip VCFTooltip;

    @FXML
    private Tooltip AnnotationTooltip;

    @FXML
    private Tooltip RepGenTooltip;

    @FXML
    private Tooltip MultiAnnotationTooltip;

    @FXML
    private Tooltip RefIngestTooltip;

    @FXML
    private AnnotationGUIController AnnotationGUIController;

    @FXML
    private MultiAnnotationGUIController MultiAnnotationGUIController;

    @FXML
    private RefIngestGUIController RefIngestGUIController;

    @FXML
    private ReportGUIController ReportGUIController;

    @FXML
    private VcfGUIController VcfGUIController;

    @FXML
    private TabPane tabPane;

    public TabPane getTabPane() {
        return tabPane;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        VcfReaderDispatchee vcf = new VcfReaderDispatchee();
        VCFTooltip.setText(vcf.getDescription());

        AnnotationReaderDispatchee annoReader = new AnnotationReaderDispatchee();
        AnnotationTooltip.setText(annoReader.getDescription());

        ReportGenerationDispatchee repGen = new ReportGenerationDispatchee();
        RepGenTooltip.setText(repGen.getDescription());

        MultiAnnotationReaderDispatchee multi = new MultiAnnotationReaderDispatchee();
        MultiAnnotationTooltip.setText(multi.getDescription());

        ReferenceIngestDispatchee refIng = new ReferenceIngestDispatchee();
        RefIngestTooltip.setText(refIng.getDescription());
    }

    public void setParentController(MainGUIController parent) {
        VcfGUIController.setParentController(parent);
        AnnotationGUIController.setParentController(parent);
        ReportGUIController.setParentController(parent);
        MultiAnnotationGUIController.setParentController(parent);
        RefIngestGUIController.setParentController(parent);
    }

}
