/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.ui.config;

import edu.jhu.cannotate.dispatch.Dispatcher;
import edu.jhu.cannotate.util.FxUtils;
import edu.jhu.cannotate.ui.MainGUIController;
import java.io.File;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author greenmk1
 */
public class ConfigGUIController {

    private MainGUIController parentController;

    @FXML
    private TextField configFileTextField;

    @FXML
    private Text statusTextBox;

    @FXML
    void configBrowseButton(ActionEvent event) {
        FxUtils.insertFilePathIfSelected(configFileTextField);
    }

    public void setParentController(MainGUIController parentController) {
        this.parentController = parentController;
    }

    @FXML
    private void configSubmitButton(ActionEvent event) throws Exception {
        System.out.println("Submitting config file...");

        if (!FxUtils.checkValidFile(configFileTextField)) {
            statusTextBox.setText("Invalid configuration file or path");
            throw new IllegalStateException(configFileTextField.getText() + " is not a file.");
        }
        Dispatcher.setConfig(configFileTextField.getText());
        statusTextBox.setText("Success! Loading Dispatcher...");
        parentController.loadDispatchGUI(event);
        parentController.setConfigFileTextField(configFileTextField);

    }

    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        statusTextBox.setText("");
    }

}
