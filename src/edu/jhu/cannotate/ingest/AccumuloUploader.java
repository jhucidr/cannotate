/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import edu.jhu.cannotate.util.accumulo.MutationCreator;
import edu.jhu.cannotate.util.accumulo.Persistor;
import org.apache.accumulo.core.client.Connector;

//TODO: consider removing this class, as it don't do much.
public class AccumuloUploader {

    private final Connector connector;

    public AccumuloUploader(Connector connector) {
        this.connector = connector;
    }

    public void upload(String tableName, MutationCreator mutationCreator) throws Exception {
        try (Persistor persistor = new Persistor(connector.createBatchWriter(tableName, AccumuloUtils.getDefaultConfig()))) {
            mutationCreator.mutate(persistor);
        }
    }

}
