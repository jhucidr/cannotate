/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.Sample;
import edu.jhu.cannotate.util.file.MultilineFileParser;
import edu.jhu.cannotate.util.functional.ExceptionalConsumer;
import edu.jhu.cannotate.util.functional.Operation;
import edu.jhu.cannotate.util.vcf.VariantType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Represents a VCF file formatted using either the 4.0 or 4.1 specifications.
 * Note that this implementation is NOT comprehensive and is primarily intended
 * to be used with VCF files generated via samtools. Intended to provide
 * necessary functionality with robustness. Uses the parsable callback pattern
 * (via Operation) to allow the client programmer to apply any arbitrary action
 * to the data.
 *
 *
 */
public class ParsableMultiSampleVcfFile extends File {

    private final Set<Sample> samples;
    private final Operation<Record> op;
    private final Operation<String> headerOperation;
    public static String FILE_EXTENSION = ".vcf";
    public static final String DATA_HEADER_START = "#";
    public static final String HEADER_INFO = "##";
    public static final String DELIMITER = "\t";
    public static final Sample NONE = new Sample("NOBODY_KNOWS_THE_TROUBLE_I_SEEN");
    private final Set<MultiSampleVcfFileRecordField> cols;
    private final boolean gtOptional;

    public static ParsableMultiSampleVcfFile create(String path, Operation<Record> op) throws IOException {
        Set<Sample> samples = new VcfSampleIdGrabbinator(new File(path)).getDemSamples();
        return new ParsableMultiSampleVcfFile(samples, path, op);
    }
    
    public ParsableMultiSampleVcfFile(Set<Sample> samples, String path, Operation<Record> op) {
        this(samples, path, op, EnumSet.allOf(MultiSampleVcfFileRecordField.class));
    }

    public ParsableMultiSampleVcfFile(Set<Sample> samples, String path, Operation<Record> op, Set<MultiSampleVcfFileRecordField> fields) {
        this(samples, path, new Operation.NoOp<>(), op, fields, false);
    }

    public ParsableMultiSampleVcfFile(Set<Sample> samples, String path, Operation<String> headerOp, Operation<Record> op, Set<MultiSampleVcfFileRecordField> fields, boolean gtOptional) {
        super(path);
        if (samples == null) {
            throw new NullPointerException("sample must not be null");
        }
        if (op == null) {
            throw new NullPointerException("op must not be null");
        }
        if (fields == null) {
            throw new NullPointerException("fields must not be null");
        }
        this.samples = samples;
        this.op = op;
        this.headerOperation = headerOp;
        this.cols = EnumSet.copyOf(fields);
        this.gtOptional = gtOptional;
    }

    private HeaderData headerData;

    private long parseHeader() throws Exception {
        long lineCount = 0;
        try (BufferedReader in = new BufferedReader(new FileReader(this))) {
            String line = null;
            while ((line = in.readLine()) != null) {
                lineCount++;
                if (line.startsWith(HEADER_INFO)) {
                    headerOperation.apply(line);
                } else if (line.startsWith(DATA_HEADER_START)) {
                    if (headerData == null) {
                        headerData = buildFileColumnMap(line);
                    } else {
                        throw new VcfFileParseException("Multiple data headers found: " + line);
                    }
                } else {
                    break;

                }
            }
        }
        return lineCount - 1;
    }

    //TODO: modify the code so this returns a stream, instead of accepting a Consumer.
    public void parseMultiLine() throws Exception {
        long headerLines = parseHeader();
//        MultilineFileParser mfp = new MultilineFileParser(this, Dispatcher.getConfig().getNumNodesInt(), headerLines);
        //Hard-coded for now. Should probably use some reasonably large constant, or make it variable depending on the number of cluster nodes.
        MultilineFileParser mfp = new MultilineFileParser(this, headerLines);
        ExceptionalConsumer<Record> recEater = getOp()::apply;
        mfp
                .parse()
                .map(line -> createRecord(headerData, line))
//                .parallel() // THIS IS THE PARALLEL WE'RE TESTING TODO
                .forEach(recEater);
    }

    public void parse() throws VcfFileParseException, IOException {
        HeaderData _headerData = null;
        String line = null;
        try (BufferedReader in = new BufferedReader(new FileReader(this))) {
            while ((line = in.readLine()) != null) {
                if (line.startsWith(HEADER_INFO)) {
                    headerOperation.apply(line);
                } else if (line.startsWith(DATA_HEADER_START)) {
                    if (_headerData == null) {
                        _headerData = buildFileColumnMap(line);
                    } else {
                        throw new VcfFileParseException("Multiple data headers found: " + line);
                    }
                } else {
                    getOp().apply(createRecord(_headerData, line));
                }
            }
        } catch (VcfFileParseException ex) {
            throw ex;
        } catch (Exception ex) {
            System.out.println(line);
            throw new VcfFileParseException(ex);
        }

    }

    private class RecordCreator {

        private final HeaderData headerData;

        public RecordCreator(HeaderData headerData) {
            this.headerData = headerData;
        }

        public Record create(String line) {
            try {
                return ParsableMultiSampleVcfFile.this.createRecord(headerData, line);
            } catch (VcfFileParseException ex) {
                throw new IllegalStateException(ex);
            }
        }

    }

    public static class Wrapperation<T> implements Operation<T> {

        private final Operation<T> op;

        public Wrapperation(Operation<T> op) {
            this.op = op;
        }

        @Override
        public void apply(T t) {
            try {
                op.apply(t);
            } catch (Exception e) {
                throw new IllegalStateException("Unable to apply operation to record: " + t, e);
            }
        }

    }

    private HeaderData buildFileColumnMap(String line) throws VcfFileParseException {
        Map<Integer, MultiSampleVcfFileRecordField> fileColumnMap = new HashMap<>();
        HashMap<Integer, Sample> sampleColumnMap = new HashMap<>();
        Set<MultiSampleVcfFileRecordField> missingColumns = new HashSet<>(cols);
        //drop the '#'
        String[] headerValues = line.substring(1).split(DELIMITER);
        for (int i = 0; i < headerValues.length; i++) {
            String headerValue = headerValues[i];
            Sample sample = getSample(headerValue);
            if (sample.equals(NONE)) {
                MultiSampleVcfFileRecordField field = MultiSampleVcfFileRecordField.fromString(headerValue);
                if (field != null) {
                    fileColumnMap.put(i, field);
                    missingColumns.remove(field);
                }
            } else {
                fileColumnMap.put(i, MultiSampleVcfFileRecordField.GENOTYPE);
                sampleColumnMap.put(i, sample);
                missingColumns.remove(MultiSampleVcfFileRecordField.GENOTYPE);
            }
        }
        if (false == missingColumns.isEmpty()) {
            throw new VcfFileParseException("The following columns weren't found: " + missingColumns);
        }
        return new HeaderData(fileColumnMap, sampleColumnMap);
    }

    public static class HeaderData {

        private final Map<Integer, MultiSampleVcfFileRecordField> fileColumnMap;
        private final Map<Integer, Sample> sampleColumnMap;

        public HeaderData(Map<Integer, MultiSampleVcfFileRecordField> fileColumnMap, Map<Integer, Sample> sampleColumnMap) {
            this.fileColumnMap = fileColumnMap;
            this.sampleColumnMap = sampleColumnMap;
        }

        public Map<Integer, MultiSampleVcfFileRecordField> getFileColumnMap() {
            return fileColumnMap;
        }

        public Map<Integer, Sample> getSampleColumnMap() {
            return sampleColumnMap;
        }
    }

    private Sample getSample(String value) {
        for (Sample sample : samples) {
            if (sample.getName().equalsIgnoreCase(value)) {
                return sample;
            }
        }
        return NONE;
    }

    private Record createRecord(HeaderData headerData, String line) throws VcfFileParseException {
        if (headerData == null) {
            throw new VcfFileParseException("No date header was found in file " + getAbsolutePath());
        }
        Record record = new Record(gtOptional);
        String[] lineStuff = line.split(DELIMITER);
        for (Entry<Integer, MultiSampleVcfFileRecordField> entry : headerData.getFileColumnMap().entrySet()) {
            MultiSampleVcfFileRecordField field = entry.getValue();
            Integer index = entry.getKey();
            if (false == headerData.getSampleColumnMap().containsKey(index)) {
                MultiSampleVcfFileRecordField fieldType = entry.getValue();
                fieldType.setDataIntoRecord(record, lineStuff[index]);
            }
        }
        for (Entry<Integer, Sample> entry : headerData.getSampleColumnMap().entrySet()) {
            Integer index = entry.getKey();
            if (headerData.getSampleColumnMap().containsKey(index)) {
                MultiSampleVcfFileRecordField.GENOTYPE.setDataIntoRecord(record, lineStuff[index], headerData.getSampleColumnMap().get(index));
            }
        }
        return record;
    }

    public Operation<Record> getOp() {
        return op;
    }

    public static class Record {

        private String chromosome;
        private long position;
        private String id;
        private String ref;
        private String alt;
        private Map<Integer, GenomicPosition> alts;
        private String qualityString;
        private String filter;
        private String info;
        private String format;
        private final Map<Sample, SampleLevelInformation> sampleMap;
        private final boolean gtOptional;
        private static final String ALT_DELIMITER = ",";//CROMMA

        public Record(boolean gtOptional) {
            this.gtOptional = gtOptional;
            this.sampleMap = new HashMap<>();
            this.alts = new HashMap<>();
        }

        public boolean isGtOptional() {
            return gtOptional;
        }

        public Set<GenomicPosition> retrieveSampleLevelGenomicPositions(Sample sample) {
            makeAlts();
            SampleLevelInformation sli = sampleMap.get(sample);
            return IntStream.of(sli.getGenotype1(), sli.getGenotype2())
                    .distinct() // More performant to limit the results earlier.
                    .filter(inty -> inty > 0)
                    .mapToObj(alts::get)
                    .collect(Collectors.toSet());
        }

        private void makeAlts() {
            if (alts.isEmpty()) {
                AtomicInteger atom = new AtomicInteger(0);
                alts.putAll(Arrays.stream(alt.split(ALT_DELIMITER))
                        .map(this::buildSampleLevelGenomicPosition)
                        .collect(Collectors.toMap(a -> atom.incrementAndGet(), Function.identity())));
            }
        }

        public GenomicPosition buildGenomicPosition() {
            return new GenomicPosition(chromosome, ref, alt, position);
        }
        
        private GenomicPosition buildSampleLevelGenomicPosition(String sampleLevelAlt) {
            return new GenomicPosition(chromosome, ref, sampleLevelAlt, position);
        }

        public String getChromosome() {
            return chromosome;
        }

        public void setChromosome(String chromosome) {
            this.chromosome = chromosome;
        }

        public long getPosition() {
            return position;
        }

        public void setPosition(long position) {
            this.position = position;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRef() {
            return ref;
        }

        public void setRef(String ref) {
            this.ref = ref;
        }

        public String getAlt() {
            return alt;
        }
        
        public List<String> getAlts() {
            return Arrays.asList(getAlt().split(","));
        }

        // Who wrote a setter that does two things? 
        public void setAlt(String alt) {
            this.alt = alt;
        }

        public String getQualityString() {
            return qualityString;
        }

        public void setQualityString(String qualityString) {
            this.qualityString = qualityString;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public boolean isIndel(Sample sample) {
            return getVariantType(sample).isIndel();
        }

        public VariantType getVariantType(Sample sample) {
            return VariantType.getType(getRef(), getAlt(), sampleMap.get(sample).getGenotype());
        }

        public void addSampleLevelInformation(Sample sample, SampleLevelInformation sampleLevelInfo) {
            sampleMap.put(sample, sampleLevelInfo);
        }

        public Map<Sample, SampleLevelInformation> getSampleMap() {
            return Collections.unmodifiableMap(sampleMap);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    public static class SampleLevelInformation {

        private String genotype;
        private String genotypeAndTheKitchenSink;

        public String getGenotype() {
            return genotype;
        }

        /**
         * These methods exist because String.split(DELIMITER) was not very
         * performant. It should be noted that this will break for genotypes
         * with more than 9 values.
         */
        public int getGenotype1() {
            return Integer.parseInt(genotype.substring(0, 1));
        }

        public int getGenotype2() {
            return Integer.parseInt(genotype.substring(2, 3));
        }

        public void setGenotype(String genotype) {
            if(genotype.length() != 3){
                throw new IllegalStateException("ERROR: getGenotype1() and getGenotype2() are only designed to handle 3-character genotypes. Provided genotype: " + genotype + " is " + genotype.length() + " characters long.");
            }
            this.genotype = genotype;
        }

        public String getGenotypeAndTheKitchenSink() {
            return genotypeAndTheKitchenSink;
        }

        public void setGenotypeAndTheKitchenSink(String genotypeAndTheKitchenSink) {
            this.genotypeAndTheKitchenSink = genotypeAndTheKitchenSink;
        }

        public boolean hasGenotype() {
            return genotype != null;
        }
    }

    public static class VcfFileParseException extends IllegalArgumentException {

        public VcfFileParseException(String msg) {
            super(msg);
        }

        public VcfFileParseException(Exception ex) {
            super(ex);
        }
    }

    public static class MultiOperation implements Operation<Record> {

        private final Map<String, Long> variantStrings = new HashMap<>();

        @Override
        public void apply(Record arg) throws Exception {

            for (SampleLevelInformation s : arg.getSampleMap().values()) {
                if (false == variantStrings.containsKey(s.getGenotype())) {
                    variantStrings.put(s.getGenotype(), 0L);
                }
                variantStrings.put(s.getGenotype(), variantStrings.get(s.getGenotype()) + 1);
            }
        }

        public Map<String, Long> getVariantStrings() {
            return variantStrings;
        }

    }

}
