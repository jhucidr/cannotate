/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.AccumuloRecord;
import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.DataType;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.file.MultilineFileParser;
import edu.jhu.cannotate.util.functional.Operation;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class Annovarser {

    private final File annotationFile;
    private final Operation<AccumuloRecord> op;
    private final AnnotationSource type;
    public static final String DELIMITER = "\t";

    public Annovarser(File annotationFile, Operation<AccumuloRecord> op, AnnotationSource type) {
        this.annotationFile = annotationFile;
        this.op = op;
        this.type = type;
    }

    public void parse() throws Exception {
        Path path = Paths.get(annotationFile.getAbsolutePath());
        try (Stream<String> lines = MultilineFileParser.lines(path)) {
            Wrapperation<AccumuloRecord> wrap = new Wrapperation<>(op);
            lines
                    //                    .parallel() //Is this causing an OOM?
                    .filter(type::isValidRecord)
                    .map(line -> new CannotateLineData(annotationFile.getName(), type, line))
                    .map(type::createAccumuloAnnotationRecord)
                    .forEach(wrap::apply);
            System.out.println("type.skippedRecords = " + type.skippedRecords);
        }
    }

    //Makes the biz foreachable.
    public static class Wrapperation<T> implements Operation<T> {

        private final Operation<T> op;

        public Wrapperation(Operation<T> op) {
            this.op = op;
        }

        @Override
        public void apply(T t) {
            try {
                op.apply(t);
            } catch (Exception e) {
                throw new IllegalStateException("Unable to apply operation to record: " + t, e);
            }
        }

        public static <T> Wrapperation<T> of(Operation<T> operation) {
            return new Wrapperation<>(operation);
        }

    }

    public static enum AnnotationSource implements DataType {

        CEU_2010(AccumuloRecordType.FILTER, 5, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[0];
                String start = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                return new GenomicPosition(chr, ref, alt, start);
            }

        },        
        CEU_2009(AccumuloRecordType.FILTER, 5, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[0];
                String start = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                return new GenomicPosition(chr, ref, alt, start);
            }

        },
        YRI_2010(AccumuloRecordType.FILTER, 5, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                String chr = lineData[0];
                String position = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                info.setValue(lineData[4]);
                return new GenomicPosition(chr, ref, alt, position);
            }

        },
        YRI_2009(AccumuloRecordType.FILTER, 5, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                String chr = lineData[0];
                String start = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                info.setValue(lineData[4]);
                return new GenomicPosition(chr, ref, alt, start);
            }

        },
        WG_RNA(AccumuloRecordType.REGION, 10, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                String chr = lineData[1];
                long start = Long.parseLong(lineData[2]);
                long stop = Long.parseLong(lineData[3]) - 1;
                info.setValue(lineData[4]);
                return new GenomicPosition(chr, start, stop);
            }
        },
        GENOMIC_SUPER_DUPS(AccumuloRecordType.REGION, 30, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[1];
                long start = Long.parseLong(lineData[2]);
                long stop = Long.parseLong(lineData[3]) - 1;
                return new GenomicPosition(chr, start, stop);
            }
        },
        // THIS ONE HAS TONS OF SWEET INFO.
        GWAS_CATALOG(AccumuloRecordType.REGION, 23, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[1];
                long start = Long.parseLong(lineData[2]);
                long stop = Long.parseLong(lineData[3]) - 1; // this is not a closed interval in the source file.
                return new GenomicPosition(chr, start, stop);
            }

        },
        JPT_CHB_2009(AccumuloRecordType.FILTER, 5, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[0];
                String start = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                return new GenomicPosition(chr, ref, alt, start);
            }
        },
        JPT_CHB_2010(AccumuloRecordType.FILTER, 5, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[0];
                String start = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                return new GenomicPosition(chr, ref, alt, start);
            }
        },
        //eventually need to use the known gene cross reference.
        //currently doesn't work (line num 11?)
        KNOWN_GENE(AccumuloRecordType.GENE, 12, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[0]);
                String chr = lineData[1];
                long start = Long.parseLong(lineData[3]);
                long stop = Long.parseLong(lineData[4]) - 1;
                String geneName = lineData[1];
                String transriptName = lineData[12];
                info.setExtraInfo(Tuple2.of(geneName, transriptName));
                return new GenomicPosition(chr, start, stop);
            }

        },
        REF_GENE(AccumuloRecordType.GENE, 16, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[1]);
                String chr = lineData[2];
                long start = Long.parseLong(lineData[4]);
                long stop = Long.parseLong(lineData[5]) - 1;
                String geneName = lineData[1];
                String transriptName = lineData[12];
                info.setExtraInfo(Tuple2.of(geneName, transriptName));
                return new GenomicPosition(chr, start, stop);
            }

        },
        TARGET_SCAN_S(AccumuloRecordType.REGION, 7, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[1];
                long start = Long.parseLong(lineData[2]);
                long stop = Long.parseLong(lineData[3]) - 1;
                return new GenomicPosition(chr, start, stop);
            }

        },
        TFBS_CONS_SITES(AccumuloRecordType.REGION, 8, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[1];
                long start = Long.parseLong(lineData[2]);
                long stop = Long.parseLong(lineData[3]) - 1;
                return new GenomicPosition(chr, start, stop);
            }
        },
        SNP_138(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                return handleSNP(info);
            }
        },
        ENS_GENE(AccumuloRecordType.GENE, 16, false) {

            // implement this if we find invalid records
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[1]);
                String chr = lineData[2];
                long start = Long.parseLong(lineData[4]);
                long stop = Long.parseLong(lineData[5]) - 1;
                String geneName = lineData[1];
                String transriptName = lineData[12];
                info.setExtraInfo(Tuple2.of(geneName, transriptName));
                return new GenomicPosition(chr, start, stop);
            }

        },
        CYTO_BAND(AccumuloRecordType.REGION, 16, false) {

            // implement this if we find invalid records
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[4]);
                String chr = lineData[0];
                long start = Long.parseLong(lineData[1]);
                long stop = Long.parseLong(lineData[2]) - 1;
                return new GenomicPosition(chr, start, stop);
            }

        },
        //New, added by Melissa
        CG_46(AccumuloRecordType.FILTER, 6, false) {

            // implement this?
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[5]);
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }
        },
        CG_69(AccumuloRecordType.FILTER, 6, false) {
            //THIS ONE EXACTLY SAME FORMAT AS CG_46
            //MAYBE REMOVE THIS AND JUST HAVE ONE CALLED "CG"
            //looking at data, file is exactly the same, just longer i.e. with more
            //46 may be redundant
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[5]);
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }
        },
        COSMIC_70(AccumuloRecordType.FILTER, 6, false) {

            // implement this?
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue((String)lineData[5]);
                
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }
        },
        NCI_60(AccumuloRecordType.FILTER, 6, false) {

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                info.setValue(lineData[5]);
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }
        },
        ESP_6500(AccumuloRecordType.FILTER, 7, false) {
            // THERE ARE MULTIPLE
            // only injested one, since it encompassed the rest
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                
                //rs info
                info.setExtraInfo(lineData[6]);
                
                info.setValue(lineData[5]);
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }
        },
        EXAC_03(AccumuloRecordType.FILTER, 13, false) {
            // THERE ARE MULTIPLE
            // Has a header that prevents this from being ingested
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                //races contains exac data per race

                ArrayList<String> races = new ArrayList<String>();
                for (int i = 6; i < 13; i++) {
                    races.add(lineData[i]);
                }
                info.setExtraInfo(races);

                info.setValue(lineData[5]);
                String chr = lineData[0];
                Long start = Long.parseLong(lineData[1]);
                Long stop = Long.parseLong(lineData[2]);
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }
        },
        ALL_1000G_2012(AccumuloRecordType.FILTER, 6, false) {
            //all: yri, ceu, jpt chb (they were separated for 2012)

            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);
                String chr = lineData[0];
                String start = lineData[1];
                String ref = lineData[2];
                String alt = lineData[3];
                info.setValue(lineData[4]);
                //extra info is same as esp 6500: rs info
                //set value only takes string
                info.setExtraInfo(lineData[5]);
                return new GenomicPosition(chr, ref, alt, start);
            }

        },
        //LOTS OF INFO
        LJB_26(AccumuloRecordType.FILTER, 29, false) {
            //TODO: parse ingest rest of columns
            //Also HAS HEADER, prevents it from being ingested
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);

                ArrayList<String> otherInfo = new ArrayList<String>();
                for (int i = 6; i < 29; i++) {
                    otherInfo.add(lineData[i]);
                }
                info.setExtraInfo(otherInfo);

                info.setValue(lineData[5]);
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }

        },
        DBNSFP_30A(AccumuloRecordType.FILTER, 39, false) {
            //TODO: parse ingest rest of columns
            @Override
            public boolean isValidRecord(String line) {
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                String[] lineData = info.getLine().split(DELIMITER);

                ArrayList<String> otherInfo = new ArrayList<String>();
                for (int i = 6; i < 39; i++) {
                    otherInfo.add(lineData[i]);
                }
                info.setExtraInfo(otherInfo);

                info.setValue(lineData[5]);
                String chr = lineData[0];
                String start = lineData[1];
                String stop = lineData[2];
                String ref = lineData[3];
                String alt = lineData[4];
                return new GenomicPosition(chr, ref, alt, start, stop);
            }

        },
        SNP_130(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                return handleSNP(info);
            }
        },
        SNP_131(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                return handleSNP(info);
            }
        },
        SNP_132(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                return handleSNP(info);
            }
        },
        SNP_135(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                return handleSNP(info);
            }
        },
        SNP_128(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                return handleSNP(info);
            }
        },
        SNP_129(AccumuloRecordType.FILTER, 0, true) {

            @Override
            public boolean isValidRecord(String line) {
                if (false == line.split(DELIMITER)[9].contains("/")) {
                    skippedRecords++;
                    return false;
                }
                return true;
            }

            @Override
            public GenomicPosition buildGenomicPositionAndPopulateLineData(CannotateLineData info) {
                //allows for saving other data if need be later.
                return handleSNP(info);
            }
        },;

        private static GenomicPosition handleSNP(CannotateLineData info) {
            String[] lineData = info.getLine().split(DELIMITER);
//                        info.setDataId(lineData[4]);
            info.setValue(lineData[4]); //rsid
            String chr = lineData[1];
            String start = lineData[2];
//                        String stop = lineData[3];
            String altRef = lineData[9];
            String[] altRefPieces = altRef.split("/");
            String ref = altRefPieces[0];
            String alt = altRefPieces[1];
            return new GenomicPosition(chr, ref, alt, start);
        }

        public AccumuloRecord createAccumuloAnnotationRecord(CannotateLineData info) {
            GenomicPosition pos = buildGenomicPositionAndPopulateLineData(info);
            return recordType.buildRecord(pos, info);
        }

        final int numColumns;
        final boolean variable;
        protected final AccumuloRecordType recordType;
        long skippedRecords = 0;

        private AnnotationSource(AccumuloRecordType recordType, int numColumns, boolean variable) {
            this.recordType = recordType;
            this.numColumns = numColumns;
            this.variable = variable;
        }

        @Override
        public AccumuloRecordType getRecordType() {
            return recordType;
        }

        @Override
        public String toString() {
            return Utils.translateEnumNameToHumanReadable(name());
        }

        public static AnnotationSource fromString(String s) {
            return fromStringMap.get(s.toLowerCase());
        }

        private static final Map<String, AnnotationSource> fromStringMap;

        static {
            fromStringMap = new HashMap<>();
            for (AnnotationSource as : AnnotationSource.values()) {
                fromStringMap.put(as.toString().toLowerCase(), as);
            }

        }

    }

    public static void main(String[] args) {
        Arrays.stream(AnnotationSource.values()).forEach(System.out::println);
    }

}
