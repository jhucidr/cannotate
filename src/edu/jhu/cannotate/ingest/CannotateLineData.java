/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.DataType;
import java.util.Objects;
import java.util.Optional;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class CannotateLineData {
    private final String filename;
    private final DataType annotationType;
    private final String line;
    //Sample for VCF, or annotation name in annotation files.
    private Optional<String> dataId;
    private Optional<String> value;
    private Optional<Object> extraInfo;

    public CannotateLineData(String filename, DataType annotationType, String line) {
        this.filename = filename;
        this.annotationType = annotationType;
        this.line = line;
        this.dataId = Optional.empty();
        this.value = Optional.empty();
    }

    public String getLine() {
        return line;
    }

    public DataType getAnnotationType() {
        return annotationType;
    }

    public AccumuloRecordType getRecordType() {
        return annotationType.getRecordType();
    }

    public String getFilename() {
        return filename;
    }

    public void setDataId(String dataIdName) {
        this.dataId = Optional.of(dataIdName);
    }

    public Optional<Object> getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(Object extraInfo) {
        this.extraInfo = Optional.of(extraInfo);
    }

    /**
     * Sample name or Annotation Name.
     * @return 
     */
    //VCF Record: SampleName
    //Filter Based:
    //Region Based:
    //Gene Based: RegionType
    public String getDataId() {
        return dataId.orElse("");
    }

    public String getValue() {
        return value.orElse("");
    }

    public void setValue(String value) {
        this.value = Optional.of(value);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.filename);
        hash = 53 * hash + Objects.hashCode(this.annotationType);
        hash = 53 * hash + Objects.hashCode(this.line);
        hash = 53 * hash + Objects.hashCode(this.dataId);
        hash = 53 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CannotateLineData other = (CannotateLineData) obj;
        if (!Objects.equals(this.filename, other.filename)) {
            return false;
        }
        if (this.annotationType != other.annotationType) {
            return false;
        }
        if (!Objects.equals(this.line, other.line)) {
            return false;
        }
        if (!Objects.equals(this.dataId, other.dataId)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LineMetadata{" + "filename=" + filename + ", annotationType=" + annotationType + '}';
    }
    
}
