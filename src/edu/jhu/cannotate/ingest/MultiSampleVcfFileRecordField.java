/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.Sample;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 */
public enum MultiSampleVcfFileRecordField {

    CHROMOSOME("CHROM") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setChromosome(value);
                }
            }, POSITION("POS") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setPosition(Long.parseLong(value));
                }
            }, ID("ID") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setId(value);
                }
            }, REF("REF") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setRef(value);
                }
            }, ALT("ALT") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setAlt(value);
                }
            }, QUALITY_STRING("QUAL") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setQualityString(value);
                }
            }, FILTER("FILTER") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setFilter(value);
                }
            }, INFO("INFO") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setInfo(value);
                }
            }, FORMAT("FORMAT") {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) {
                    record.setFormat(value);
                }
            }, GENOTYPE {
                @Override
                public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) throws ParsableMultiSampleVcfFile.VcfFileParseException {
                    ParsableMultiSampleVcfFile.SampleLevelInformation sampleLevelInformation = new ParsableMultiSampleVcfFile.SampleLevelInformation();
                    sampleLevelInformation.setGenotypeAndTheKitchenSink(value);
                    String format = record.getFormat();
                    if (format == null) {
                        throw new ParsableMultiSampleVcfFile.VcfFileParseException("The format string must be parsed before the genotype string.");
                    }
                    Map<String, Integer> formatMap = new HashMap<>();
                    String[] formatValues = format.split(GENOTYPE_STRING_DELIMITER);
                    for (int i = 0; i < formatValues.length; i++) {
                        formatMap.put(formatValues[i].trim().toUpperCase(), i);
                    }
                    if (formatMap.containsKey(GT_STRING)) {
                        String[] genotypeInfo = value.split(GENOTYPE_STRING_DELIMITER);
                        sampleLevelInformation.setGenotype(genotypeInfo[formatMap.get(GT_STRING)]);
                    } else {
                        if (false == record.isGtOptional()) {
                            throw new ParsableMultiSampleVcfFile.VcfFileParseException("The format string does not contain genotype information.");
                        }
                    }
                    record.addSampleLevelInformation(sample, sampleLevelInformation);
                }
            };
    private final String columnIdentifier;
    private static final String NO_DEFAULT_IDENTIFIER = "AINT_GOT_NONE";
    private static final String GT_STRING = "GT";
    private static final String GENOTYPE_STRING_DELIMITER = ":";

    private MultiSampleVcfFileRecordField(String columnIdentifier) {
        this.columnIdentifier = columnIdentifier;
    }

    private MultiSampleVcfFileRecordField() {
        this.columnIdentifier = NO_DEFAULT_IDENTIFIER;
    }

    public abstract void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value, Sample sample) throws ParsableMultiSampleVcfFile.VcfFileParseException;

    public void setDataIntoRecord(ParsableMultiSampleVcfFile.Record record, String value) throws ParsableMultiSampleVcfFile.VcfFileParseException {
        setDataIntoRecord(record, value, ParsableMultiSampleVcfFile.NONE);
    }

    public String getColumnIdentifier() {
        return columnIdentifier;
    }
    private static final Map<String, MultiSampleVcfFileRecordField> map = new HashMap<String, MultiSampleVcfFileRecordField>();

    static {
        for (MultiSampleVcfFileRecordField val : MultiSampleVcfFileRecordField.values()) {
            map.put(val.getColumnIdentifier().toUpperCase(), val);
        }
    }

    public static MultiSampleVcfFileRecordField fromString(String s) {
        return s == null ? null : map.get(s.toUpperCase());
    }
}
