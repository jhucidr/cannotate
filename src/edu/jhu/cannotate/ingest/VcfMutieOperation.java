/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.AccumuloVcfInfoRecord;
import edu.jhu.cannotate.record.AccumuloVcfRecord;
import edu.jhu.cannotate.util.accumulo.Persistor;
import edu.jhu.cannotate.util.functional.Operation;
import java.io.File;

public class VcfMutieOperation implements Operation<ParsableMultiSampleVcfFile.Record> {

    private final Persistor persistor;
    private final String vcfFileName;

    public VcfMutieOperation(Persistor persistor, String vcfFileName) {
        this.persistor = persistor;
        this.vcfFileName = new File(vcfFileName).getName();
    }

    @Override
    public void apply(ParsableMultiSampleVcfFile.Record record) throws Exception {
        AccumuloVcfRecord
                .fromVcfRecord(vcfFileName, record)
                .stream()
                //                .parallel()
                .forEach(sample -> persistor.addMutation(sample.buildMutation()));
        AccumuloVcfInfoRecord
                .fromVcfRecord(vcfFileName, record)
                .stream()
                .forEach(info -> persistor.addMutation(info.buildMutation()));
        
    }

}
