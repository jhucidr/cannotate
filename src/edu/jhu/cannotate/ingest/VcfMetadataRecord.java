/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.avro.AvroVcfMetadata;
import edu.jhu.cannotate.record.metadata.MetadataRecord;
import static edu.jhu.cannotate.record.metadata.MetadataRecord.TABLE;
import edu.jhu.cannotate.record.DataType;
import edu.jhu.cannotate.record.Sample;
import edu.jhu.cannotate.record.metadata.MetadataUtils;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class VcfMetadataRecord extends MetadataRecord {
    
    public VcfMetadataRecord(Key key, Value value) {
        super(key, value);
    }
    
    public static List<String> retrieveAllSamples() {
        try {
            
            return MetadataUtils.getMetadataByType(AvroRecordType.VCF_METADATA)
                    .stream()
                    .map(r -> (AvroVcfMetadata) r)
                    .flatMap(r -> r.getSamples().stream())
                    .map(CharSequence::toString)
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
            
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }

//        try (Stream<Map.Entry<Key, Value>> metadataStream = TABLE.streamTable()) {
//            return metadataStream
//                    .map(e -> new MetadataRecord(e.getKey(), e.getValue()))
//                    .filter(record -> record.getType() == AvroRecordType.VCF_METADATA)
//                    .map(record -> new VcfMetadataRecord(record.getKey(), record.getValue()))
//                    .map(VcfMetadataRecord::getSamples)
//                    .flatMap(List::stream)
//                    .collect(Collectors.toList());
//        } catch (Exception ex) {
//            throw new IllegalStateException(ex);
//        }
    }
    
    public static List<String> retrieveSamples(File file) throws Exception {
        return retrieveSamples(file.getName());
    }
    
    public static List<String> retrieveSamples(String filename) throws Exception {
        Scanner scan = null;
        try {
            scan = TABLE.buildScanner();
            scan.setRange(Range.exact(Utils.clean(filename)));
            List<String> samples = new ArrayList<>();
            for (Map.Entry<Key, Value> entry : scan) {
                VcfMetadataRecord record = new VcfMetadataRecord(entry.getKey(), entry.getValue());
                samples.addAll(record.getSamples());
            }
            scan.close();
            return samples;
        } finally {
            try {
                scan.close();
            } catch (Exception omnomnomnomnom) {
            }
        }
    }
    
    public static Value buildValue(Tuple2<File, DataType> fileInfo) {
        try {
            Set<Sample> samples = new VcfSampleIdGrabbinator(fileInfo.get1()).getDemSamples();
            String value = samples.stream()
                    .map(Sample::getName)
                    .map(String::trim)
                    .sorted()
                    .collect(Collectors.joining(","));
            return new Value(value.getBytes());
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }
    
    public List<String> getSamples() {
        return Arrays.asList(value.toString().split(","));
    }
    
}
