/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest.haplotype;

import edu.jhu.cannotate.ingest.MultiSampleVcfFileRecordField;
import edu.jhu.cannotate.ingest.ParsableMultiSampleVcfFile;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.functional.Operation;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class RsNumberTranslationOperation implements Operation<ParsableMultiSampleVcfFile.Record>{

    private final List<String> rsNumbers;
    Map<String, ParsableMultiSampleVcfFile.Record> rsNumberToRecordMap;

    public RsNumberTranslationOperation(List<String> rsNumbers) {
        rsNumberToRecordMap = new HashMap<>();
        this.rsNumbers = rsNumbers;
    }
    
    @Override
    public void apply(ParsableMultiSampleVcfFile.Record arg) throws Exception {
        if(rsNumbers.contains(Utils.clean(arg.getId()))) {
            rsNumberToRecordMap.put(arg.getId(), arg);
        }
    }
    
    public static Map<String, GenomicPosition> retrieveRsNumberTranslationData(List<String> rsNumbers, String pathToVcf) throws Exception {
        RsNumberTranslationOperation rsOp = new RsNumberTranslationOperation(rsNumbers);
        EnumSet<MultiSampleVcfFileRecordField> kaviarColumns = EnumSet.of(MultiSampleVcfFileRecordField.ALT,
                MultiSampleVcfFileRecordField.CHROMOSOME,
                MultiSampleVcfFileRecordField.POSITION,
                MultiSampleVcfFileRecordField.REF,
                MultiSampleVcfFileRecordField.QUALITY_STRING,
                MultiSampleVcfFileRecordField.FILTER,
                MultiSampleVcfFileRecordField.INFO,
                MultiSampleVcfFileRecordField.ID);
        ParsableMultiSampleVcfFile vcfFile = new ParsableMultiSampleVcfFile(new HashSet<>(), pathToVcf, rsOp, kaviarColumns);
        vcfFile.parse();
        Map<String, GenomicPosition> genomicPositionMap = rsOp.rsNumberToRecordMap
                .entrySet()
                .stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().buildGenomicPosition()));
        return genomicPositionMap;
    }
    
    
    public static void main(String[] args) throws IOException, Exception {
        List<String> rsFellers = Arrays.asList(new String[]{"rs4244285","rs4986893","rs28399504","rs56337013", "rs72552267", "rs72558186", "rs41291556"});
        String kaviar = "/Users/sean-la/Desktop/Kaviar-160204-Public/vcfs/Kaviar-160204-Public-hg38-trim.vcf";
        Map<String, GenomicPosition> rsNumberTranslationData = RsNumberTranslationOperation.retrieveRsNumberTranslationData(rsFellers, kaviar);
        rsNumberTranslationData
                .entrySet()
                .stream()
                .map(e -> e.getKey() + " -> " + e.getValue())
                .forEach(System.out::println);
    }
    
}
