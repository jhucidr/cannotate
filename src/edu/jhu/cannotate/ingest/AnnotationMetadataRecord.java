/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.avro.AvroAnnotationMetadata;
import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.record.metadata.MetadataRecord;
import edu.jhu.cannotate.record.DataType;
import edu.jhu.cannotate.record.metadata.MetadataUtils;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AnnotationMetadataRecord extends MetadataRecord {

    public AnnotationMetadataRecord(Key key, Value value) {
        super(key, value);
    }

    public static List<DataType> getCurrentAnnotationFields() throws Exception {
        List<AvroAnnotationMetadata> annotations = MetadataUtils.getMetadataByType(AvroRecordType.ANNOTATION_METADATA);
        return annotations
                .stream()
                .map(rec -> rec.getAnnotationName().toString())
                .map(DataType::fromString)
                .collect(Collectors.toList());
    }

    // should eventually have more information. Perhaps a schema?
    public static Value buildValue(Tuple2<File, DataType> fileInfo) {
        String value = fileInfo.get2().name();
        return new Value(value.getBytes());
    }
    
}
