/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.Sample;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public class VcfSampleIdGrabbinator {

    private final File vcfPath;

    public VcfSampleIdGrabbinator(File vcfPath) {
        this.vcfPath = vcfPath;
    }

    public Set<Sample> getDemSamples() throws IOException {
        Path vcf = Paths.get(vcfPath.getAbsolutePath());
        try (Stream<String> stream = Files.lines(vcf)) {
            Optional<String> headerLineMaybe = stream
                    .filter(VcfSampleIdGrabbinator::reglarHeader)
                    .findFirst();
            if (headerLineMaybe.isPresent()) {
                String headerLine = headerLineMaybe.get();
                headerLine = headerLine.substring(1); // Lop off the leading '#' sign.
                Set<Sample> samples = new HashSet<>();
                for (String col : headerLine.split(ParsableMultiSampleVcfFile.DELIMITER)) {
                    if (MultiSampleVcfFileRecordField.fromString(col) == null) {
                        samples.add(new Sample(col));
                    }
                }
                return samples;
            } else {
                //NO HEADER? N00000000000
                return new HashSet<>();
            }
        }
    }

    private static boolean reglarHeader(String s) {
        return false == s.startsWith(ParsableMultiSampleVcfFile.HEADER_INFO)
                && s.startsWith(ParsableMultiSampleVcfFile.DATA_HEADER_START);
    }

}
