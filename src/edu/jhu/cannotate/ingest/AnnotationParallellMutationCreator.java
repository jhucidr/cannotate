/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.ingest;

import edu.jhu.cannotate.record.AccumuloRecord;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.util.accumulo.MutationCreator;
import edu.jhu.cannotate.util.accumulo.Persistor;
import edu.jhu.cannotate.util.functional.Operation;
import java.io.File;

public class AnnotationParallellMutationCreator implements MutationCreator {

    private final File pathToAnnovarFile;
    private final Annovarser.AnnotationSource type;
    private GenomicPosition widestPosition = new GenomicPosition("1", 0, 0);
    
    public AnnotationParallellMutationCreator(File pathToAnnovarFile, Annovarser.AnnotationSource type) {
        this.pathToAnnovarFile = pathToAnnovarFile;
        this.type = type;
    }

    public GenomicPosition getWidestPosition() {
        return widestPosition;
    }

    @Override
    public void mutate(Persistor persistor) throws Exception {
        Operation.MultiOperation<AccumuloRecord> op = new Operation.MultiOperation<>();
        op.addOperation((AccumuloRecord rec) -> persistor.addMutation(rec.buildMutation()));
        op.addOperation((AccumuloRecord rec) -> persistor.addMutations(rec.buildChildishMutations()));
        op.addOperation((AccumuloRecord rec) -> theWidestPosition(rec));
        new Annovarser(pathToAnnovarFile, op, type).parse();
    }
    
    private void theWidestPosition(AccumuloRecord rec) {
        GenomicPosition position = rec.getGenomicPosition();
        if(position.calculateWidth() > widestPosition.calculateWidth()) {
            widestPosition = position;
        }
    }

}
