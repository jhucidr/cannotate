/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.table.AccumuloTable;

public class DatabaseCreationDispatchee implements Dispatchee {

    @Override
    public String getArg() {
        return "-create";
    }

    @Override
    public String getDescription() {
        return "Creates the CANNOTATE database using the provided configuration.";
    }

    @Override
    public String dispatch(String[] args) throws Exception {
        //TODO: Make this smart enough to accept a list of tables to create/re-create.
        AccumuloTable.createTables();
        return "Success!";
    }

    public static void main(String... args) throws Exception {
        String configFile = "src/configuration_example.txt";
        Dispatcher.dispatch(new String[]{"-create", configFile});
    }

}
