/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.reference.ReferenceIngester;
import edu.jhu.cannotate.table.AccumuloTable;
import java.io.File;

public class ReferenceIngestDispatchee implements Dispatchee {

    private static final int NUM_ARGS = 1;
    private static final String ARG = "-ingest_reference";

    @Override
    public String dispatch(String[] args) throws Exception {
        if (args.length != NUM_ARGS) {
            return NUM_ARGS + " arguments are expected, but " + args.length + " were found.";
        }
        File inputFile = new File(args[0]);
        ReferenceIngester ingester = new ReferenceIngester(inputFile);
        ingester.ingestData();
        return "Reference Ingest complete for: " + inputFile.getAbsolutePath();
    }

    @Override
    public String getArg() {
        return ARG;
    }

    @Override
    public String getDescription() {
        return "Ingests a FASTA formatted reference file.";
    }

    public static void main(String[] args) throws Exception {
        String configFile = "src/configuration_example.txt";
        String thatAmazingFile = "/disk2/run/GRCh38_full_analysis_set_plus_decoy_hla.fa";
        Dispatcher.dispatch(new String[]{ARG, configFile, thatAmazingFile});
    }

    public static void main1(String... args) throws Exception {
        AccumuloTable.TESTY_MCTEST_FACE.streamTable()
                .map(AccumuloRecordType::buildAccumuloRecord)
                .forEach(System.out::println);
    }

}
