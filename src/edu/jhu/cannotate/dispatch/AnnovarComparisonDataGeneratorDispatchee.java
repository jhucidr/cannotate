package edu.jhu.cannotate.dispatch;


import edu.jhu.cannotate.util.validation.AnnovarTablesComparisonCommandGenerator;
import java.io.File;

/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AnnovarComparisonDataGeneratorDispatchee implements Dispatchee {
    
    private static final int NUM_ARGS = 4;
    
     @Override
    public String dispatch(String[] args) throws Exception {
        if (args.length != NUM_ARGS) {
            return NUM_ARGS + " arguments are expected, but " + args.length + " were found.";
        }
        String vcfFile = args[0];
        String annovarDatabases = args[1];
        String annovarExecutable = args[2];
        String pathToDatabases = args[3];
        AnnovarTablesComparisonCommandGenerator commandGenerator = 
                 new AnnovarTablesComparisonCommandGenerator(new File(vcfFile), new File(annovarDatabases), new File(annovarExecutable), new File(pathToDatabases));
        String command = commandGenerator.build();
        System.out.println("Generating Annovar Output With the following Cmd:");
        System.out.println(command);
        Runtime.getRuntime().exec(command).waitFor();        
        return "Successfully executed Annovar tables command.";
    }
    
    @Override
    public String getArg() {
        return "-generate_annovar_data";
    }

    @Override
    public String getDescription() {
        return "Calls out to Annovar to generate comparison data..";
    }
 
    public static void main(String[] args) throws Exception {
        Dispatcher.dispatch(new String[]{
        "-generate_annovar_data",
        "-stand_alone",
        "/Users/sean-la/Documents/sequencing/annovar/500.vcf",
        "src/tables_config.txt",
        "/Users/sean-la/Documents/sequencing/annovar/table_annovar.pl",
        "/Users/sean-la/Documents/sequencing/annovar/down_db"});
    }
    
}
