/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.retrieve.RegionAnnotator;
import java.io.File;

//TODO: add transcription configuration.
public class ReportGenerationDispatchee implements Dispatchee {

    private static final int NUM_ARGS = 1;
    private static final String ARG = "-generate_report";

    @Override
    public String dispatch(String[] args) throws Exception {
        if (args.length != NUM_ARGS) {
            return NUM_ARGS + " arguments are expected, but " + args.length + " were found.";
        }
        //TODO: filtering the contents of the database (output file).
        File outputDir = new File(args[0]);
        new RegionAnnotator(outputDir).cannotate();
        return "Report generated in " + outputDir.getAbsolutePath();
    }

    @Override
    public String getArg() {
        return ARG;
    }

    @Override
    public String getDescription() {
        return "Generates an annotation report using ingested VCF and Annotation data.";
    }

    public static void main(String[] args) throws Exception {
        String configFile = "src/configuration_example.txt";
        Dispatcher.dispatch(new String[]{ARG, configFile, "pretty_output.txt"});
    }

}
