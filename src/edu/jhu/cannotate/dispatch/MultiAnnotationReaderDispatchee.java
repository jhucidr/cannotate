/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

public class MultiAnnotationReaderDispatchee implements Dispatchee {

    private static final int NUM_ARGS = 1;
    private static final String ARG = "-ingest_multiple_annotations";

    @Override
    public String dispatch(String[] args) throws Exception {
        if (args.length != NUM_ARGS) {
            return NUM_ARGS + " arguments are expected, but " + args.length + " were found.";
        }
        String pathToAnnotationFile = args[0];
        new AnnotationList(pathToAnnotationFile).getAnnotations().forEach(this::ingest);
        return "Annotations have been uploaded";
    }

    private void ingest(AnnotationList.Annotation annotation) {
        try {
            new AnnotationReaderDispatchee().dispatch(new String[]{annotation.getType().name(), annotation.getFilePath().getAbsolutePath()});
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    @Override
    public String getArg() {
        return ARG;
    }

    @Override
    public String getDescription() {
        return "Parse a set of annotation files as described in the provided file.";
    }

    public static void main(String[] args) throws Exception {
        String configFile = "../HadoopProject/src/configuration_example.txt";
        String pathToAnnotationFile = "../HadoopProject/run/multi_db_file.txt";
//        Dispatcher.dispatch(new String[]{"-create", configFile});
        Dispatcher.dispatch(new String[]{ARG, configFile, pathToAnnotationFile});
    }

}
