/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.avro.AvroAnnotationMetadata;
import edu.jhu.cannotate.avro.AvroFileMetadata;
import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.ingest.AccumuloUploader;
import edu.jhu.cannotate.ingest.AnnotationParallellMutationCreator;
import edu.jhu.cannotate.ingest.Annovarser;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.record.metadata.MetadataRecord;
import edu.jhu.cannotate.record.metadata.MetadataUtils;
import edu.jhu.cannotate.table.AccumuloTable;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import java.io.File;

public class AnnotationReaderDispatchee implements Dispatchee {

    private static final int NUM_ARGS = 2;

    @Override
    public String dispatch(String[] args) throws Exception {
        if (args.length != NUM_ARGS) {
            return NUM_ARGS + " arguments are expected, but " + args.length + " were found.";
        }
        String annotationType = args[0];
        String pathToAnnotationFile = args[1];
        return dispatch(annotationType, pathToAnnotationFile);
    }

    public String dispatch(String annotationType, String pathToAnnotationFile) throws Exception {
        AccumuloUploader accumuloUploader = new AccumuloUploader(AccumuloUtils.getConnection());
        Annovarser.AnnotationSource source = Annovarser.AnnotationSource.valueOf(annotationType.toUpperCase());
        
        File annotationFile = new File(pathToAnnotationFile);
        AnnotationParallellMutationCreator annovarDatabaseParallellMutationCreator
                = new AnnotationParallellMutationCreator(annotationFile, source);
        accumuloUploader.upload(AccumuloTable.ANNOTATION_DATA.getTableName(), annovarDatabaseParallellMutationCreator);

        AvroAnnotationMetadata annotationMetadata = AvroAnnotationMetadata
                .newBuilder()
                .setAnnotationName(source.name())
                .setFilename(pathToAnnotationFile)
                .setLargestRegion(annovarDatabaseParallellMutationCreator.getWidestPosition().calculateWidth())
                .build();   
        MetadataUtils.insertMetadata(annotationFile, annotationMetadata);
        
        AvroFileMetadata fileMetadata = MetadataUtils.buildAvroFileMetadata(annotationFile);
        MetadataUtils.insertMetadata(annotationFile, fileMetadata);
        
        return pathToAnnotationFile + "has been uploaded.";
    }

    @Override
    public String getArg() {
        return "-ingest_annotation";
    }

    @Override
    public String getDescription() {
        return "Parse an annotation file, and ingest into the accumulo database.";
    }

    
    
    
}
