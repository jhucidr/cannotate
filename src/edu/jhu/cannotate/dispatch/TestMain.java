/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.ingest.Annovarser;
import edu.jhu.cannotate.record.AccumuloGeneAnnotationRecord;
import edu.jhu.cannotate.record.AccumuloRecordType;
import edu.jhu.cannotate.record.DataType;
import edu.jhu.cannotate.table.AccumuloTable;



/**
 *
 * @author apluser
 */
public class TestMain {
    public static void main1(String... args) throws Exception {
        DatabaseCreationDispatchee.main(args);
        VcfReaderDispatchee.main(args);
        MultiAnnotationReaderDispatchee.main(args);
        ReportGenerationDispatchee.main(args);
    }
    
    public static void main2(String... args) throws Exception {
        AccumuloTable.ANNOTATION_DATA.streamTable()
                .map(AccumuloRecordType::buildAccumuloRecord)
                .filter(r -> r.getDatabaseType() == DataType.fromString(Annovarser.AnnotationSource.ENS_GENE.name()))
                .filter(r -> r.getGenomicPosition().getStartPosition() >= 16050000)
                .peek(System.out::println)
                .map(r -> ((AccumuloGeneAnnotationRecord) r).getTranscriptionRecord())
                .peek(System.out::println)
                .flatMap(tr -> tr.getRegions().stream().sorted())
                .forEach(System.out::println);
    }
    
    public static void main(String... args) throws Exception {
        main1(args);
//        main2(args);
    }
}
