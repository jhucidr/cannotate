/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

/**
 *
 *
 * @created Jun 12, 2015
 */
public interface Dispatchee {

    public default String getHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append(getArg());
        sb.append(System.getProperty("line.separator"));
        sb.append("\t");
        sb.append(getDescription());
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }

    public String getArg();

    public default String dispatch(String[] args) throws Exception {
        return "This dispatchee is not implemented.";
    }

    public default String getDescription() {
        return "No description available";
    }

}
