/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.avro.AvroFileMetadata;
import edu.jhu.cannotate.avro.AvroVcfMetadata;
import edu.jhu.cannotate.ingest.AccumuloUploader;
import edu.jhu.cannotate.ingest.ParsableMultiSampleVcfFile;
import edu.jhu.cannotate.ingest.VcfMutieOperation;
import edu.jhu.cannotate.ingest.VcfSampleIdGrabbinator;
import edu.jhu.cannotate.record.Sample;
import edu.jhu.cannotate.record.metadata.MetadataUtils;
import edu.jhu.cannotate.table.AccumuloTable;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import edu.jhu.cannotate.util.accumulo.MutationCreator;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class VcfReaderDispatchee implements Dispatchee {

    private static final int NUM_ARGS = 1;

    @Override
    public String dispatch(String[] args) throws Exception {
        if (args.length != NUM_ARGS) {
            return NUM_ARGS + " arguments are expected, but " + args.length + " were found.";
        }
        String pathToVcfFile = args[0];
        return dispatch(pathToVcfFile);
    }

    public String dispatch(String pathToVcfFile) throws Exception {
        System.out.println("Connecting To Accumulo Server...");
        AccumuloUploader accumuloUploader = new AccumuloUploader(AccumuloUtils.getConnection());

        System.out.println("Storing VCF Metadata...");
        File vcfFile = new File(pathToVcfFile);
        AvroFileMetadata fileMetadata = MetadataUtils.buildAvroFileMetadata(vcfFile);
        MetadataUtils.insertMetadata(vcfFile, fileMetadata);
        
        Set<Sample> samples = new VcfSampleIdGrabbinator(vcfFile).getDemSamples();
        List<CharSequence> sampleList = samples.stream()
                .map(sample -> sample.getName())
                .collect(Collectors.toList());
        AvroVcfMetadata vcfMetadata = AvroVcfMetadata.newBuilder()
                .setFilename(vcfFile.getName())
                .setSamples(sampleList)
                .build();
        MetadataUtils.insertMetadata(vcfFile, vcfMetadata);
        
        System.out.println("Ingesting VCF Data...");
        MutationCreator vcfMutationCreator = persistor -> ParsableMultiSampleVcfFile.create(pathToVcfFile, new VcfMutieOperation(persistor, pathToVcfFile)).parseMultiLine();
        accumuloUploader.upload(AccumuloTable.ANNOTATION_DATA.getTableName(), vcfMutationCreator);
        return pathToVcfFile + " has been uploaded.";

        
        
    }

    @Override
    public String getArg() {
        return "-ingest_vcf";
    }

    @Override
    public String getDescription() {
        return "Parses a VCF file, and ingests into the accumulo database.";
    }

    public static void main(String... args) throws Exception {
        String configFile = "src/configuration_example.txt";
//        Dispatcher.dispatch(new String[]{"-create", configFile});
//        Dispatcher.dispatch(new String[]{"-ingest_vcf", configFile, "/disk2/run/AllChr.vcf"});
//        Dispatcher.dispatch(new String[]{"-ingest_vcf", configFile, "/disk2/run/ALL.chr22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf"});
        Dispatcher.dispatch(new String[]{"-ingest_vcf", configFile, "/disk2/run/500_small.vcf"});

    }

}
