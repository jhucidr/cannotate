/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.ingest.Annovarser;
import edu.jhu.cannotate.util.Utils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnnotationList {

    private final String listFile;
    private static final String SEPARATOR = "\t";

    public AnnotationList(String listFile) {
        this.listFile = listFile;
    }

    public List<Annotation> getAnnotations() throws IOException {
        try (Stream<String> lines = Files.lines(Paths.get(listFile))) {
            return lines
                    .filter(Utils::filterComments)
                    .map(line -> convert(line))
                    .collect(Collectors.toList());
        }
    }

    private Annotation convert(String line) {
        String[] parts = line.split(SEPARATOR);
        return new Annotation(new File(parts[1]), Annovarser.AnnotationSource.valueOf(parts[0]));
    }

    public static class Annotation {

        private final File filePath;
        private final Annovarser.AnnotationSource type;

        public Annotation(File filePath, Annovarser.AnnotationSource type) {
            this.filePath = filePath;
            this.type = type;
        }

        public File getFilePath() {
            return filePath;
        }

        public Annovarser.AnnotationSource getType() {
            return type;
        }

    }

}
