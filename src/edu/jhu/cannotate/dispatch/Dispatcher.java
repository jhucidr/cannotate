/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.dispatch;

import edu.jhu.cannotate.config.AccumuloConfiguration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Dispatcher {

    private static final Map<String, Dispatchee> DISPATCH_MAP = new HashMap<>();
    private static AccumuloConfiguration CONFIG = null;
    private static final String NEW_LINE = System.getProperty("line.separator");
    private static final String NO_CONFIG = "-stand_alone";

    static {
        add(
                new Help(),
                new VcfReaderDispatchee(),
                new AnnotationReaderDispatchee(),
                new ReportGenerationDispatchee(),
                new DatabaseCreationDispatchee(),
                new MultiAnnotationReaderDispatchee(),
                new AnnovarComparisonDataGeneratorDispatchee(),
                new ReferenceIngestDispatchee()
        );
    }

    public static void main(String... args) throws Exception {
        dispatch(args);
    }

    public static void setConfig(String configPath) throws Exception {
        CONFIG = AccumuloConfiguration.read(configPath);
    }

    public static AccumuloConfiguration getConfig() {
        if (CONFIG == null) {
            System.out.println("\n\n\n");
            System.out.println(" ====== No config available... Setting default values =====");
            System.out.println(" ====== This will likely result in a non-functional instance of the application ====");
            System.out.println("\n\n\n");
            CONFIG = AccumuloConfiguration.read("./src/configuration_example.txt");
            return CONFIG;
        } else {
            return CONFIG;
        }
    }

    private static void add(Dispatchee... dispatchees) {
        DISPATCH_MAP.putAll(Arrays.stream(dispatchees)
                .collect(Collectors.toMap(d -> d.getArg(), d -> d)));
    }

    public static void dispatch(String... args) throws Exception {
        if (args.length < 2) {
            System.out.println(DISPATCH_MAP.get(Help.ARG).dispatch(args));
            return;
        }
        String configArg = args[0];
        if (false == configArg.equalsIgnoreCase(NO_CONFIG)) {
            CONFIG = AccumuloConfiguration.read(args[1]);
        }
        Dispatchee dispatchee = DISPATCH_MAP.getOrDefault(args[0], DISPATCH_MAP.get(Help.ARG));
        System.out.println(dispatchee.dispatch(Arrays.copyOfRange(args, 2, args.length)));
    }

    public static class Help implements Dispatchee {

        public static final String ARG = "-help";

        @Override
        public String getArg() {
            return ARG;
        }

        @Override
        public String dispatch(String[] args) throws Exception {
            StringBuilder sb = new StringBuilder();
            sb.append("The first two arguments for each option are always the option, followed by the location of the configuration file to use:");
            sb.append(NEW_LINE);
            sb.append("\t-option /path/to/config.txt");
            sb.append(NEW_LINE);

            String help = DISPATCH_MAP.values().stream()
                    .map(Dispatchee::getHelp)
                    .sorted()
                    .collect(Collectors.joining(System.getProperty("line.separator")));

            sb.append(help);
            return sb.toString();
        }

        @Override
        public String getDescription() {
            return "Tells you how to use each of the available options.";
        }

    }

}
