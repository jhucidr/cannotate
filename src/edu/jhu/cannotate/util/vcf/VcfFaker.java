/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.vcf;

import edu.jhu.cannotate.util.Utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.commons.lang.StringUtils;

public class VcfFaker {

    private final String vcfFile;
    private final String smallinatedFile;
    private final int numColumns;
    private final String delimiter;
    private final List<String> header = new ArrayList<>();
    private final List<String> contents = new ArrayList<>();
    private boolean inHeader = true;
    private static final String DELIMITER = "\t";

    public VcfFaker(String vcfFile, String smallinatedFile, int numColumns, String delimiter) {
        this.vcfFile = vcfFile;
        this.smallinatedFile = smallinatedFile;
        this.numColumns = numColumns;
        this.delimiter = delimiter;
    }

    private void parse() throws IOException {
        try (Stream<String> vcfLines = Files.lines(Paths.get(vcfFile))) {
            vcfLines.forEach(this::munge);
        }
    }

    public void process() throws IOException {
        parse();
        write();
    }

    private void write() throws IOException {
        try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(smallinatedFile))))) {
            header.forEach(writer::println);
            IntStream.range(1, 22)
                    .forEach(i -> writeVariants(writer, "" + i));
            writeVariants(writer, "X");
            writeVariants(writer, "Y");
        }
    }

    private String changeLine(String line, String newChr) {
        String noChr = Utils.after(line, DELIMITER);
        long pos = Long.parseLong(Utils.before(noChr, DELIMITER));
        long newPos = pos + (777 * count);
        String noPos = Utils.after(noChr, DELIMITER);
        return newChr + DELIMITER + newPos + DELIMITER + noPos;
    }
    private int count = 0;

    private void writeVariants(PrintWriter out, String chr) {
        count++;
        contents.stream()
                .map(s -> changeLine(s, chr))
                .forEach(out::println);
    }

    private void munge(String line) {
        if (inHeader) {
            header.add(line);
            if (StringUtils.countMatches(line, delimiter) >= numColumns - 1) {
                inHeader = false;
            }
        } else {
            contents.add(line);
        }
    }

}
