/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.vcf;

import java.util.EnumSet;

/**
 *
 *
 */
public enum VariantType {

    DELETION,
    INSERTION,
    MISSING,
    REF,
    SNV,
    HYBRID,;

    private static final EnumSet<VariantType> INDELZ = EnumSet.of(INSERTION, DELETION, HYBRID);

    public static VariantType getType(String ref, String alt, String gtString) {
        if (gtString.startsWith(".")) {
            return MISSING;
        } else {
            try {
                int gt1 = Integer.parseInt(gtString.substring(0, 1));
                int gt2 = Integer.parseInt(gtString.substring(gtString.length() - 1));
                if (gt1 == 0 && gt2 == 0) {
                    return REF;
                }
                String[] altRa = alt.split(",");
                String alleleOne = gt1 == 0 ? ref : altRa[gt1 - 1];
                String alleleTwo = gt2 == 0 ? ref : altRa[gt2 - 1];
                int refLength = ref.length();
                if (alleleOne.length() < refLength || alleleTwo.length() < refLength) {
                    if (alleleOne.length() > refLength || alleleTwo.length() > refLength) {
                        return HYBRID;
                    }
                    return DELETION;
                } else if (alleleOne.length() > refLength || alleleTwo.length() > refLength) {
                    return INSERTION;
                } else if (alleleOne.length() == refLength || alleleTwo.length() == refLength) {
                    return SNV;
                } else {
                    throw new IllegalStateException("Oh noes!");
                }
            } catch (NumberFormatException e) {
                System.out.println("gtString = " + gtString);
                throw e;
            }
        }
    }

    public boolean isIndel() {
        return INDELZ.contains(this);
    }

}
