/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.accumulo;

import edu.jhu.cannotate.util.Utils;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class ChromosomSizeSplitsGenerator {

    public List<String> parse(int numSplits) throws Exception {
        LongStream maxes = Utils.getResource("chromosome_sizes.txt")
                .stream()
                .map(line -> line.split("\\s+")[1])
                .mapToLong(Long::parseLong);

        List<Long> rangeMaxes = maxes
                .mapToObj(Long::new)
                .collect(Collectors.toList());

        long totes = rangeMaxes.stream().mapToLong(Long::longValue).sum();
        long splitSize = totes / numSplits;

        return IntStream.range(1, numSplits)
                .mapToObj(i -> AccumuloUtils.padInteger(getTransformedValue(rangeMaxes, i * splitSize)))
                .collect(Collectors.toList());

    }

    private long getTransformedValue(List<Long> rangeMaxes, long value) {
        if (rangeMaxes.size() == 1) {
            return value;
        }

        long currentTestValue = value / rangeMaxes.size();
        double difference = testValue(rangeMaxes, currentTestValue, value);

        long maxMax = rangeMaxes.stream().mapToLong(Long::longValue).max().getAsLong();

        while (Math.abs(difference) >= rangeMaxes.size()) {
            currentTestValue -= (double) difference / (double) rangeMaxes.size();
            difference = testValue(rangeMaxes, currentTestValue, value);
        }

        return currentTestValue;
    }

    private double testValue(List<Long> rangeMaxes, long testValue, long desiredValue) {
        long currentValue = getWeightedValue(rangeMaxes, testValue);
        return (double) currentValue - desiredValue;
    }

    private long getWeightedValue(List<Long> rangeMaxes, long value) {
        List<Long> filteredList = rangeMaxes.stream().filter(rangeMax -> value > rangeMax).collect(Collectors.toList());
        int numRangesIn = rangeMaxes.size() - filteredList.size();
        return (numRangesIn * value) + filteredList.stream().mapToLong(Long::longValue).sum();
    }

}
