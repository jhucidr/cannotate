/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.accumulo;

import edu.jhu.cannotate.config.AccumuloConfiguration;
import edu.jhu.cannotate.config.ConnectionData;
import edu.jhu.cannotate.dispatch.Dispatcher;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.functional.ExceptionalConsumer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.admin.TableOperations;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.hadoop.io.Text;

public class AccumuloUtils {

    private static ConnectionData CONNECTION_DATA;
    private static Connector CONNECTOR;
    public static final int KEYSPACE_DIMENSION = 2;
    private static final String[] CHARS = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"};
    private static List<String> KEY_SPACE = generateKeySpace(KEYSPACE_DIMENSION);

    private static synchronized void initConnection() throws Exception {
        if (CONNECTOR == null) {
            try {
                AccumuloConfiguration conf = Dispatcher.getConfig();
                CONNECTION_DATA = new ConnectionData(conf.getInstance(), conf.getZookeepers(), conf.getUser(), conf.getPass().getBytes());
                CONNECTOR = CONNECTION_DATA.getConnection();
            } catch (Exception ex) {
                throw new IllegalStateException(ex);
//            System.out.println("Warning: Accumulo Configuration was not read. No accumulo interactions are possible.");
            }
        }
    }

    public static Connector getConnection() throws Exception {
        if (CONNECTOR == null) {
            initConnection();
        }
        return CONNECTOR;
    }

    public static TableOperations getTableOperations() throws Exception {
        return getConnection().tableOperations();
    }

    public static Mutation initializeMutation(Text row) {
        return initializeMutation(row.toString());
    }

    public static Mutation initializeMutation(String row) {
        return new Mutation(getSplitKey(row));
    }

    public static boolean tableExists(String table) throws Exception {
        return getTableOperations().exists(table);
    }

    public static void createTable(String table) throws Exception {
        TableOperations to = getTableOperations();
        if (to.exists(table)) {
            System.out.println("createTable(" + table + ") Failed: Table  already exists.");
        } else {
            System.out.println("Creating Table " + table + "...");
            to.create(table);
        }
    }

    public static void deleteTable(String table) throws Exception {
        TableOperations to = getTableOperations();
        if (false == to.exists(table)) {
            System.out.println("deleteTable(" + table + ") failed: Table does not exist.");
        } else {
            to.delete(table);
            System.out.println("deleteTable(" + table + ") ok.");
        }
    }

    public static BatchWriterConfig getDefaultConfig() {
        BatchWriterConfig config = new BatchWriterConfig();
        config.setMaxMemory(Dispatcher.getConfig().getBatchWriterMaxRamInBytes()); // bytes available to batchwriter for buffering mutations
        config.setMaxWriteThreads(Dispatcher.getConfig().getBatchWriterNumThreadsInt());
        return config;
    }

    public static void updateTable(String table, Mutation masterOfMagnetism) throws Exception {
        try (Persistor persistor = new Persistor(getConnection().createBatchWriter(table, getDefaultConfig()))) {
            persistor.addMutation(masterOfMagnetism);
        }
    }

    public static void updateTable(String table, Collection<Mutation> teleportationTelepathyAndTelekinesis) throws Exception {
        try (Persistor persistor = new Persistor(getConnection().createBatchWriter(table, getDefaultConfig()))) {
            persistor.addMutations(teleportationTelepathyAndTelekinesis);
        }
    }

    /**
     * Remember to close the Scanner!
     */
    public static Scanner scanTable(String tableName) throws Exception {
        return getConnection().createScanner(tableName, Authorizations.EMPTY);
    }

    
    
    //remember to close this stream
    public static Stream<Entry<Key, Value>> streamTable(String tableName) throws Exception {
        Scanner scanner = scanTable(tableName);
        Stream<Entry<Key, Value>> result = StreamSupport.stream(scanner.spliterator(), false);
        result.onClose(scanner::close);
        return result;
    }

    public static void processTable(String tableName, ExceptionalConsumer<? super Entry<Key, Value>> consumer) throws Exception {
        processTable(tableName, consumer, new Range());
    }

    public static void processTable(String tableName, ExceptionalConsumer<? super Entry<Key, Value>> consumer, Range range) throws Exception {
        Scanner scanner = scanTable(tableName);
        scanner.setRange(range);
        try (Stream<Entry<Key, Value>> s = StreamSupport.stream(scanner.spliterator(), true)) {
            s.forEach(consumer);
        } finally {
            scanner.close();
        }
    }

    public static void printTable(String tableName) throws Exception {
        processTable(tableName, System.out::println);
    }

    //Why would we have tablets than are allowed by this range of keys?
    public static List<String> getHashSplits(int numTabletServers) {
        int numChars = KEYSPACE_DIMENSION;
        int numKeys = (int) Math.pow(CHARS.length, numChars);
        int rangeSize = numKeys / numTabletServers;

        System.out.println("numChars = " + numChars);

        List<String> keySpace = generateKeySpace(numChars);
        System.out.println(keySpace.size());
        List<String> splits = new ArrayList<>();
        for (int i = 1; i < numTabletServers; i++) {
            splits.add(keySpace.get(i * rangeSize));
        }
        return splits;
    }

    public static List<String> getSplits(int numTabletServers) {
        try {
            return new ChromosomSizeSplitsGenerator().parse(numTabletServers);
        } catch (Exception ex) {
            throw new IllegalStateException("Unable to construct splits.", ex);
        }
    }

    public static List<String> generateKeySpace(int numChars) {
        return Utils.composeN(AccumuloUtils::expandStream, numChars)
                .apply(Stream.of(""))
                .collect(Collectors.toList());
    }

    public static Stream<String> expandStream(Stream<String> ss) {
        return ss.flatMap(AccumuloUtils::appendChars);
    }

    public static Stream<String> appendChars(String start) {
        return Stream.of(CHARS).map(s -> start + s);
    }

    public static String getSplitKey(String string) {
//        String key = KEY_SPACE.get(Math.abs(string.toLowerCase().hashCode() % KEY_SPACE.size())) + string;
//        return key;
        return string;
    }

    public static String padInteger(long elLongo) {
        return String.format("%010d", elLongo);
    }

    public static String padInteger(String elLongo) {
        return String.format("%010d", Long.parseLong(elLongo.trim()));
    }

}
