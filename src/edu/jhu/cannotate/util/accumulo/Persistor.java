/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.accumulo;

import java.util.Collection;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.data.Mutation;

public class Persistor implements AutoCloseable {

    private final BatchWriter batchWriter;

    public Persistor(BatchWriter batchWriter) {
        this.batchWriter = batchWriter;
    }

    public void addMutation(Mutation mutation) {
        try {
            batchWriter.addMutation(mutation);
        } catch (MutationsRejectedException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    public void addMutations(Collection<Mutation> mutations) throws MutationsRejectedException {
        batchWriter.addMutations(mutations);
    }

    public void cleanUp() throws MutationsRejectedException {
        System.out.println("ADDING FINAL RECORDS");
        batchWriter.flush();
        batchWriter.close();
    }

    @Override
    public void close() throws Exception {
        cleanUp();
    }

}
