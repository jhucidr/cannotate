/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util;

import java.text.DecimalFormat;

/**
 * This class provides a time-based periodic status update based showing a
 * process's progress. By providing it with an expected number of updates, and
 * calling the update method as processing happens (for example, as each line is
 * parsed in a file), this class will print to System.out a percentage complete
 * on long-running processes.
 *
 */
public class StatusUpdater {

    private static final DecimalFormat df = new DecimalFormat("0.00");
    private final long updateRateInMillis;
    private final long expectedNumValues;
    private long counter = 0;
    private long lastUpdate = 0;

    public StatusUpdater(long expectedNumValues) {
        this(expectedNumValues, 2000);
    }

    public StatusUpdater(long expectedNumValues, double updateRateInSeconds) {
        this(expectedNumValues, (long) (updateRateInSeconds * 1000));
    }

    public StatusUpdater(long expectedNumValues, long updateRateInMillis) {
        this.expectedNumValues = expectedNumValues;
        this.updateRateInMillis = updateRateInMillis;
    }

    public void update() {
        update("Processing");
    }

    public void update(String processMsg) {
        counter++;
        if (lastUpdate == 0) {
            lastUpdate = System.currentTimeMillis();
        } else if (lastUpdate + updateRateInMillis < System.currentTimeMillis()) {
            lastUpdate = System.currentTimeMillis();
            double percentComplete = (double) 100 * (double) counter / (double) expectedNumValues;
            System.out.println(processMsg + " " + df.format(percentComplete) + "% complete.");
        }
    }

}
