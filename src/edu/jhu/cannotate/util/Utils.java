/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util;

import com.google.common.io.Resources;
import edu.jhu.cannotate.util.stream.StreamAlternator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class Utils {

    public static final String FS_PARAM_NAME = "fs.defaultFS";
    private static final Configuration config = new Configuration(true);
    public static final boolean RUN_LOCAL = true;

    public static void main(String... args) throws Exception {
        System.out.println(withinRange(Long.MAX_VALUE, Long.MIN_VALUE, 7));
    }

    static {
        String HADOOP_CONF_DIR = System.getenv("HADOOP_CONF_DIR");
        if (HADOOP_CONF_DIR == null || HADOOP_CONF_DIR.isEmpty()) {
            HADOOP_CONF_DIR = "/data/conf";
        }
        if (config.get(FS_PARAM_NAME).equalsIgnoreCase("file:///")) { //Attempt to add relevant XML files when missing.
//            System.out.println("===\t===\t===\tAdding stuff to config...");
            config.addResource(new Path(HADOOP_CONF_DIR, "core-site.xml"));
            config.addResource(new Path(HADOOP_CONF_DIR, "hdfs-site.xml"));
            config.addResource(new Path(new Path(HADOOP_CONF_DIR, "hbase"), "hbase-site.xml"));
            if (false == RUN_LOCAL) {
//                System.out.println("===\t===\t===\tAdding stuff to config for remote run...");
                config.addResource(new Path(HADOOP_CONF_DIR, "mapred-site.xml"));
                config.addResource(new Path(HADOOP_CONF_DIR, "capacity-scheduler.xml"));
                config.addResource(new Path(HADOOP_CONF_DIR, "hadoop-policy.xml"));
                config.addResource(new Path(HADOOP_CONF_DIR, "httpfs-site.xml"));
                config.addResource(new Path(HADOOP_CONF_DIR, "kms-acls.xml"));
                config.addResource(new Path(HADOOP_CONF_DIR, "kms-site.xml"));
                config.addResource(new Path(HADOOP_CONF_DIR, "yarn-site.xml"));
                config.setBoolean(MRJobConfig.MAPREDUCE_JOB_USER_CLASSPATH_FIRST, true);
            }
        }
    }

    private static String CHR = "CHR";

    //TODO: validate chromosomes from a list of know chromosomes: 1-23, X, Y, known contigs, Mitochondrial
    public static String chromosomeConformist(String chr) {
        chr = chr.trim().toUpperCase();
        if (chr.contains(CHR)) {
            chr = after(chr, CHR);
        }
        if (chr.contains("_")) {
            chr = chr.replace("_", "");
        }
        if (chr.contains("-")) {
            chr = chr.replace("-", "");
        }
        try {
            int chrAsInt = Integer.parseInt(chr);
            if (chrAsInt < 10) {
                //Zero-pad the chrosome.
                chr = "0" + chrAsInt;
            }
        } catch (Exception gulp) {
        }
        return chr.trim();
    }

    public static Configuration getHdfsConfiguration() {
        return config;
    }

    public static FileSystem getHdfsFileSystem() throws Exception {
        return FileSystem.get(getHdfsConfiguration());
    }

    public static PrintWriter getHdfsOutputWriter(String outputPath) throws Exception {
        return new PrintWriter(new BufferedOutputStream(Utils.getHdfsFileSystem().create(new Path(outputPath))));
    }

    public static boolean rename(Path src, Path dst) throws Exception {
        FileSystem fs = Utils.getHdfsFileSystem();
        if (false == fs.exists(src)) {
            throw new IllegalStateException("Error: Source path does not exist. " + src.toString());
        }
        System.out.println("Moving: " + src + " to " + dst);
        mkDirHdfs(dst.getParent());
        if (fs.exists(dst)) {
            if ((fs.isDirectory(src) && fs.isDirectory(dst)) || (fs.isFile(src) && fs.isFile(dst))) {
                Path tmpDst = new Path(dst.getParent(), dst.getName() + "_old");
                int counter = 1;
                while (fs.exists(tmpDst)) {
                    tmpDst = new Path(dst.getParent(), dst.getName() + "_old" + counter++);
                }
                if (false == fs.rename(dst, tmpDst)) {
                    throw new IllegalStateException("Unable to move dst " + dst.toString() + " to temporary location.");
                }
                if (false == fs.rename(src, dst)) {
                    try {
                        //Attempt a rollback.
                        if (false == fs.rename(tmpDst, dst)) {
                            System.out.println("Rollback Failed :(");
                        }
                    } catch (Exception omNomNomNom) {
                    }
                    throw new IllegalStateException("Unable to move src " + src + " to destination" + dst);
                }
                if (false == fs.delete(tmpDst, true)) {
                    System.out.println("Warning: Unable to delete temporary directory");
                }
            } else if (fs.isFile(src)) {
                throw new IllegalStateException("Can't move a file onto a directory.");
            } else if (fs.isDirectory(src)) {
                throw new IllegalStateException("Can't Move a directory onto a file.");
            }
        } else {
            if (false == fs.rename(src, dst)) {
                throw new IllegalStateException("Unable to move src " + src + " to destination" + dst);
            }
        }
        System.out.println("Move successful!!!");
        return true;
    }

    public static int signum(long i1, long i2) {
        return i1 == i2 ? 0 : (i1 < i2 ? -1 : 1);
    }

    public static int signum(String s1, String s2) {
        if (s1 == s2) {
            return 0;
        }
        if (s1 == null) {
            return -1;
        }
        if (s2 == null) {
            return 1;
        }
        return s1.compareToIgnoreCase(s2);
    }

    public static <T> Set<T> union(Set<T> one, Set<T> two) {
        HashSet<T> composed = new HashSet<>(one);
        composed.addAll(two);
        return composed;
    }

    private static void printEnv() {
        for (Map.Entry<String, String> entrySet : System.getenv().entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue();
            System.out.println(key + "=" + value);
        }
    }

    public static long countLines(String fileName) throws Exception {
        try (InputStream is = new BufferedInputStream(new FileInputStream(fileName))) {
            return countLines(is);
        }
    }

    public static long countLines(InputStream is) throws IOException {
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            is.close();
        }
    }

    public static boolean isTesting() {
        String projectDir = System.getProperty("user.dir").toLowerCase();
        return projectDir.contains("netbeans")
                || projectDir.contains("code");
    }

    public static void uploadToHdfs(String localFileToUpload, String hdfsDestination) throws Exception {
        Path source = new Path(localFileToUpload);
        Path dest = new Path(hdfsDestination);
        getHdfsFileSystem().copyFromLocalFile(source, dest);
    }

    public static void uploadToHdfs(URL website, String hdfsDestination) throws Exception {
        try (
                InputStream in = getRedirectedUrl(website.toString()).openStream();
                FSDataOutputStream out = getHdfsFileSystem().create(new Path(hdfsDestination));) {
            IOUtils.copy(in, out);
        }
    }

    public static void downloadFromHdfs(String hdfsFileToDownload, String localDestination) throws Exception {
        Path source = new Path(hdfsFileToDownload);
        Path dest = new Path(localDestination);
        getHdfsFileSystem().copyToLocalFile(source, dest);
    }

    public static List<Path> getHdfsFiles(Path startingDir, boolean recursive) throws Exception {
        return getHdfsFiles(startingDir, recursive, lfs -> true);
    }

    public static List<Path> getHdfsFiles(Path startingDir, boolean recursive, Predicate<? super LocatedFileStatus> filter) throws Exception {
        FileSystem fs = Utils.getHdfsFileSystem();
        RemoteIterator<LocatedFileStatus> files = fs.listFiles(startingDir, recursive);
        List<Path> result = new ArrayList<>();
        while (files.hasNext()) {
            LocatedFileStatus lfs = files.next();
            if (filter.test(lfs)) {
                result.add(lfs.getPath());
            }
        }
        return result;
    }

    public static void addFilesToJob(Job job, Path startingDir, Predicate<? super LocatedFileStatus> filter) throws Exception {
        List<Path> paths = Utils.getHdfsFiles(startingDir, false, filter);
        System.out.println("Adding the following HDFS files to be processed:");
        paths.stream().forEach(System.out::println);
        if (paths.isEmpty()) {
            throw new IllegalStateException("No CSV Files found in the specified HDFS directory: " + startingDir.toString());
        }
        for (Path path : paths) {
            FileInputFormat.addInputPath(job, path);
        }
    }

    public static URL getRedirectedUrl(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
        conn.setReadTimeout(5000);
        conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
        conn.addRequestProperty("User-Agent", "Mozilla");
        conn.addRequestProperty("Referer", "google.com");

        boolean redirect = false;

        // normally, 3xx is redirect
        int status = conn.getResponseCode();
        if (status != HttpURLConnection.HTTP_OK) {
            if (status == HttpURLConnection.HTTP_MOVED_TEMP
                    || status == HttpURLConnection.HTTP_MOVED_PERM
                    || status == HttpURLConnection.HTTP_SEE_OTHER) {
                redirect = true;
            }
        }
        if (redirect) {
            // get redirect url from "location" header field
            return getRedirectedUrl(conn.getHeaderField("Location"));
        }
        return obj;
    }

    public static void mkDirHdfs(Path dir) throws Exception {
        if (false == getHdfsFileSystem().isDirectory(dir)) {
            getHdfsFileSystem().mkdirs(dir);
        }
    }

    public static File getJarFilePath() throws Exception {
        return new File(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
    }

    public static String between(String source, String start, String end) {
        int i = source.indexOf(start);
        if (i >= 0) {
            int i2 = source.indexOf(end, i + start.length());
            if (i2 >= 0) {
                return source.substring(i + start.length(), i2);
            }
        }
        return "";
    }

    public static String before(String source, String before) {
        int i = source.indexOf(before);
        if (i >= 0) {
            return source.substring(0, i);
        }
        return "";
    }

    public static String after(String source, String after) {
        int i = source.indexOf(after);
        if (i >= 0) {
            return source.substring(i + after.length());
        }
        return "";
    }

    public static String substring(String source, String includeThisAndEverythingAfter) {
        int i = source.indexOf(includeThisAndEverythingAfter);
        if (i >= 0) {
            return source.substring(i);
        }
        return "";
    }

    public static boolean withinRange(long l1, long l2, long range) {
        if(range < 0)  {
            throw new IllegalArgumentException("The range must be a positive integer.");
        }
        long difference = Math.abs(Math.subtractExact(l1, l2));
        return range >= difference;
    }

    
    public static boolean withinRange(double d1, double d2, double range) {
        return d1 < d2
                ? range >= (d2 - d1)
                : range >= (d1 - d2);
    }
    
    /**
     * This method accepts any object, and returns a stream of that object
     * repeated n times.
     */
    public static <T> Stream<T> repeat(T toRepeat, int numRepeats) {
        return IntStream.range(0, numRepeats).mapToObj(i -> toRepeat);
    }

    /**
     * This method accepts a function<T,T>, and returns a new function composed
     * of the original function applied, or repeated, n times.
     */
    public static <T> Function<T, T> composeN(Function<T, T> func, int numRepeats) {
        return repeat(func, numRepeats).reduce(Function.identity(), (f1, f2) -> f1.andThen(f2));
    }

    public static <A, B, C> Function<A, C> compose(Function<A, B> f1, Function<B, C> f2) {
        return f1.andThen(f2);
    }

    /**
     * Determines the line seperator used in a file. This is important, because
     * we need to know the num chars for later.
     */
    public static String retrieveLineSeparator(File file) throws Exception {
        char current;
        String lineSeparator = "";
        FileInputStream in = new FileInputStream(file);
        try {
            while (in.available() > 0) {
                current = (char) in.read();
                if ((current == '\n') || (current == '\r')) {
                    lineSeparator += current;
                    if (in.available() > 0) {
                        char next = (char) in.read();
                        if ((next != current)
                                && ((next == '\r') || (next == '\n'))) {
                            lineSeparator += next;
                        }
                    }
                    return lineSeparator;
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return null;
    }

    public static enum ComposeStrategy {

        CONCATENATE, //Concat one stream onto the end of the other
        ALTERNATE;   // Alternate output between streams
    }

    public static <T> Stream<T> compose(ComposeStrategy strat, Stream<Stream<T>> streams) {
        if (strat == ComposeStrategy.CONCATENATE) {
            return streams.reduce(Stream.empty(), Stream::concat);
        } else if (strat == ComposeStrategy.ALTERNATE) {
            return StreamAlternator.of(streams);
        } else {
            throw new IllegalStateException("Unrecognized strategy: " + strat);
        }
    }

    public static boolean isRunningFromJar() {
        try {
            return false == Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath().endsWith("build/classes/");
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static File retrievePathToJar() {
        try {
            String jarPath = Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            if (jarPath.endsWith("build/classes/")) {
                jarPath = Utils.before(jarPath, "build/classes");
            }
            return new File(jarPath);
        } catch (URISyntaxException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static List<String> getResource(String resourcePath) throws Exception {
        if (isRunningFromJar()) {
            URL url = Resources.getResource(resourcePath);
            return Resources.readLines(url, Charsets.UTF_8);
        } else {
            return Files.readAllLines(Paths.get("src", resourcePath));
        }
    }

    public static String adjustStopPosition(String stopPosition) {
        long stop = Long.parseLong(stopPosition);
        return (stop - 1) + "";
    }

    public static boolean filterComments(String toTest) {
        toTest = toTest.trim();
        if (toTest.isEmpty()) {
            return false;
        }
        if (toTest.startsWith("//")) {
            return false;
        }
        if (toTest.startsWith("#")) {
            return false;
        }
        return true;
    }

    public static String translateEnumNameToCamelCase(String toTranslate) {
        StringBuilder sb = new StringBuilder();
        for (String word : toTranslate.split("_")) {
            for (int i = 0; i < word.toCharArray().length; i++) {
                char c = word.toCharArray()[i];
                sb.append(i == 0 ? Character.toUpperCase(c) : Character.toLowerCase(c));
            }
        }
        return sb.toString();
    }

    public static String translateEnumNameToHumanReadable(String toTranslate) {
        return Arrays.stream(toTranslate.split("_"))
                .map(s -> Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase())
                .collect(Collectors.joining(" "));
    }

    public static String clean(String s) {
        String scrubADubDub = s.replaceAll("[^A-Za-z0-9\\.]", "");
        return scrubADubDub;
    }

    public static String cleanUP(String s) {
        String scrubADubDub = s.replaceAll("[^A-Za-z0-9\\.]", "").toUpperCase();
        return scrubADubDub;
    }

    public static boolean isEmpty(String s) {
        return s == null ? true : s.trim().isEmpty();
    }



}
