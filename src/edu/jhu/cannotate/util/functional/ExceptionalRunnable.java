/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.functional;

/**
 * This class allows you to create and use a Consumer that throws Exceptions. It
 * is helpful when working with lambda expressions.
 *
 *
 * @param <T>
 */
@FunctionalInterface
public interface ExceptionalRunnable extends Runnable {

    public abstract void runSomethingExceptional() throws Exception;

    @Override
    public default void run() {
        try {
            runSomethingExceptional();
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static <V> ExceptionalRunnable of(RunnableThatThrowsException sup) {
        return sup::run;
    }

    @FunctionalInterface
    public static interface RunnableThatThrowsException {

        public abstract void run() throws Exception;
    }

}
