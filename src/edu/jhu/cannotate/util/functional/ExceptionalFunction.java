/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.functional;

import java.util.function.Function;

/**
 * This class allows you to create and use a Function that throws Exceptions. It
 * is helpful when working with lambda expressions.
 *
 *
 */
@FunctionalInterface
public interface ExceptionalFunction<T, R> extends Function<T, R> {

    public abstract R applySomethingExceptional(T t) throws Exception;

    @Override
    public default R apply(T t) {
        try {
            return applySomethingExceptional(t);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

}
