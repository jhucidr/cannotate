/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.functional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @param <T>
 */
@FunctionalInterface
public interface Operation<T> extends Serializable {

    public void apply(T arg) throws Exception;

    public static class NoOp<T> implements Operation<T> {

        @Override
        public void apply(T arg) {
            // The operation, it does nothing!
        }

        public static <T> Operation<T> build() {
            return new NoOp<T>();
        }
    }

    public static class MultiOperation<T> implements Operation<T> {

        private final List<Operation<T>> operations = new ArrayList<>();

        public MultiOperation() {
        }

        @Override
        public void apply(T arg) throws Exception {
            for (Operation<T> op : operations) {
                op.apply(arg);
            }
        }

        public void addOperation(Operation<T> op) {
            operations.add(op);
        }
    }

    public static class PrintingOperation<T> implements Operation<T> {

        @Override
        public void apply(T arg) throws Exception {
            System.out.println(arg.toString());
        }
    }
}
