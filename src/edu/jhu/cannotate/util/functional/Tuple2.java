/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.functional;

import java.util.AbstractMap;
import java.util.Map.Entry;
import java.util.Objects;

public class Tuple2<T, V> {

    private final T t;
    private final V v;

    public Tuple2(T t, V v) {
        this.t = t;
        this.v = v;
    }

    public T get1() {
        return t;
    }

    public V get2() {
        return v;
    }

    public static <T, V> Tuple2<T, V> of(T t, V v) {
        return new Tuple2<>(t, v);

    }

    public static <T, V> Tuple2<T, V> fromEntry(Entry<T, V> entry) {
        return new Tuple2<>(entry.getKey(), entry.getValue());
    }

    public Entry<T, V> toEntry() {
        return new AbstractMap.SimpleEntry<>(t, v);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.t);
        hash = 79 * hash + Objects.hashCode(this.v);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple2<?, ?> other = (Tuple2<?, ?>) obj;
        if (!Objects.equals(this.t, other.t)) {
            return false;
        }
        if (!Objects.equals(this.v, other.v)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Tuple2{" + t + ", " + v + '}';
    }

}
