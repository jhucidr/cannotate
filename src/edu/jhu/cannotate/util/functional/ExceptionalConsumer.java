/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.functional;

import java.util.function.Consumer;

/**
 * This class allows you to create and use a Consumer that throws Exceptions. It
 * is helpful when working with lambda expressions.
 *
 *
 * @param <T>
 */
@FunctionalInterface
public interface ExceptionalConsumer<T> extends Consumer<T> {

    public abstract void acceptSomethingExceptional(T t) throws Exception;

    @Override
    public default void accept(T t) {
        try {
            acceptSomethingExceptional(t);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

}
