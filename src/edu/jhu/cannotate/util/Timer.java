/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Timer {

    private static final boolean PRINT_TIMER_OUTPUT = true;
    private static final long APPLICATION_START = System.currentTimeMillis();
    private static final SimpleDateFormat time_only = new SimpleDateFormat("hh:mma");

    private long lastChecked = System.currentTimeMillis();
    public static final Timer timer = new Timer();

    static {
        System.out.println("Application Start Time: " + new Date().toString());
    }

    public void time(String message) {
        if (PRINT_TIMER_OUTPUT) {
            long sinceAppStart = (System.currentTimeMillis() - APPLICATION_START) / 1000;
            long sincelastCheck = (System.currentTimeMillis() - lastChecked) / 1000;
            System.out.println(message + " = " + sincelastCheck + " seconds. SinceAppStart = " + sinceAppStart + " seconds. Current Time: " + time_only.format(new Date()));
            lastChecked = System.currentTimeMillis();
        }
    }

}
