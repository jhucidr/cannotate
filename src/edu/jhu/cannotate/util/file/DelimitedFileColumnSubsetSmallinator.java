/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.file;

import edu.jhu.cannotate.ingest.ParsableMultiSampleVcfFile;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.apache.commons.lang.StringUtils;

public class DelimitedFileColumnSubsetSmallinator {

    private final String vcfFile;
    private final String smallinatedFile;
    private final int numColumns;
    private final String delimiter;

    public DelimitedFileColumnSubsetSmallinator(String vcfFile, String smallinatedFile, int numColumns, String delimiter) {
        this.vcfFile = vcfFile;
        this.smallinatedFile = smallinatedFile;
        this.numColumns = numColumns;
        this.delimiter = delimiter;
    }

    public void smallinate() throws IOException {
        try (Stream<String> vcfLines = Files.lines(Paths.get(vcfFile));
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(smallinatedFile))))) {
            vcfLines.map(this::munge).forEach(writer::println);

        }
    }

    private String munge(String line) {
        if (StringUtils.countMatches(line, delimiter) < numColumns) {
            return line;
        } else {
            return line.substring(0, StringUtils.ordinalIndexOf(line, delimiter, numColumns));
        }
    }

    public static void main(String[] args) throws IOException {
        String in = "/disk2/Cannotate/HadoopProject/run/500.vcf";
        String out = "/disk2/Cannotate/HadoopProject/run/500_small.vcf";
        System.out.println("Smallinating " + in + "...");
        new DelimitedFileColumnSubsetSmallinator(in, out, 15, ParsableMultiSampleVcfFile.DELIMITER).smallinate();
        System.out.println("Finished writing Smaller File:");
        System.out.println(out);
    }

}
