/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.file;

import edu.jhu.cannotate.util.StatusUpdater;
import edu.jhu.cannotate.util.Timer;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class will parse multiple parts of one file at the same time. It does
 * this by creating a unique BufferedFileReader for each section that the file
 * is divided into. This class outputs a stream which alternates, in order,
 * between the output from each Reader.
 *
 * @created Nov 6, 2015
 */
public class MultilineFileParser {

    private final File toParse;
    private final String lineSeperator;
    private final int numSplits;
    private final long linesToSkip;
    private long totalNumLines = 0;
    private long currentLine = 0;
    private double percentage = 0;
    private final DecimalFormat df = new DecimalFormat("0.00");

    public static void main(String[] args) throws Exception {
        try (BufferedReader in = new BufferedReader(new FileReader(new File("/disk2/Cannotate/HadoopProject/src/chromosome_sizes.txt")))) {
            String line = "";
            while ((line = in.readLine()) != null) {
                System.out.println("line = " + line);
            }
        }
    }

    private static final int MINIMUM_NUM_SPLITS = 8;

    private static int calcSplits() {
        int result = Runtime.getRuntime().availableProcessors() + 1;
        return result >= MINIMUM_NUM_SPLITS ? result : MINIMUM_NUM_SPLITS;
    }

    public MultilineFileParser(File toParse) throws Exception {
        this(toParse, 0);
    }

    /**
     * Use this constructor is there is a header that you want to parse first,
     * before moving onto the file content. Note: This class will simply ignore
     * the header. It will need to be handled in the calling context. Note:
     * First line is 0-indexed.
     */
    public MultilineFileParser(File toParse, long linesToSkip) throws Exception {
        this.toParse = toParse;
        this.numSplits = calcSplits();
        this.lineSeperator = Utils.retrieveLineSeparator(toParse);
        this.linesToSkip = linesToSkip;
    }

    public static Stream<String> lines(Path toParse) throws Exception {
        return new MultilineFileParser(toParse.toFile()).parse();
    }

    public static Stream<String> lines(File toParse) throws Exception {
        return new MultilineFileParser(toParse).parse();
    }

//    public static void main(String[] args) throws Exception {
//        File toParse = new File("/Users/newcojd1/Downloads/500_lines.txt");
//        MultilineFileParser parser = new MultilineFileParser(toParse, 5, 10);
//        parser.parse().forEach(System.out::println);
//    }
    private List<Tuple2<Long, Long>> mapLinesToByteLocations(Collection<Long> lineNumbers) throws Exception {
        List<Long> toFind = new ArrayList<>(lineNumbers);
        Collections.sort(toFind);
        List<Tuple2<Long, Long>> result = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(toParse))) {
            String line = null;
            long count = 0;
            long charCount = 0;
            long nextLine = -1;
            while ((line = in.readLine()) != null) {
                if (toFind.isEmpty()) {
                    return result;
                }
                if (nextLine == -1) {
                    nextLine = toFind.get(0);
                }
                if (nextLine == count) {
                    result.add(Tuple2.of(nextLine, charCount));
                    toFind.remove(0);
                    nextLine = -1;
                }
                charCount += line.length() + lineSeperator.length();
                count++;
            }
        }
        if (false == toFind.isEmpty()) {
            System.out.println("Failed to find the following lines in the file: " + toFind);
        }
        return result;
    }

    /**
     * This method divides the file up into parts based off the number of
     * desired splits. The Partial file parsers are divided equally by lines.
     * Remainder lines are included in the trailing stream.
     */
    private List<PartialFileParser> preParse() throws Exception {
        Timer timer = new Timer();
        System.out.println(" === === === Parsing " + toParse.getAbsolutePath() + "...");
        totalNumLines = Utils.countLines(toParse.getAbsolutePath()) + 1;
        timer.time("Counting " + totalNumLines + " file lines");
        long fileLineIncrement = (totalNumLines - linesToSkip) / numSplits;
        Set<Long> fileLineSplits = new HashSet<>();
        for (int i = 0; i < numSplits; i++) {
            fileLineSplits.add(linesToSkip + (fileLineIncrement * i));
        }
        List<Tuple2<Long, Long>> fileLineMap = mapLinesToByteLocations(fileLineSplits);
        timer.time("Seconds spent mapping " + numSplits + " file line locations");
        List<PartialFileParser> result = new ArrayList<>();
        for (int i = 0; i < fileLineSplits.size() - 1; i++) {
            Tuple2<Long, Long> tuple = fileLineMap.get(i);
            result.add(new PartialFileParser(tuple.get2(), fileLineIncrement));
        }
        //Make sure that the last parial file parser contains all of the lines.
        result.add(new PartialFileParser(fileLineMap.get(fileLineMap.size() - 1).get2(), totalNumLines));
        return result;
    }

    public Stream<String> parse() throws Exception {
        List<PartialFileParser> parsers = preParse();
        Stream<Stream<String>> stringStreams = parsers.stream()
                .map(PartialFileParser::parse);
        StatusUpdater su = new StatusUpdater(totalNumLines);
        return Utils.compose(Utils.ComposeStrategy.ALTERNATE, stringStreams)
                .peek(s -> su.update("Ingest"));
    }

    /**
     * This class will parse a part of a file, bounded by the specified start
     * character and numLinesToParse.
     */
    private class PartialFileParser implements AutoCloseable, Iterator<String> {

        private final long start; //Start will be a new line.
        private final long numLinesToParse;
        private long lineNumber;
        private BufferedReader in;
        private String cachedNextValue;
        private boolean continueParsing = true;

        public PartialFileParser(long start, long numLinesToParse) {
            this.start = start;
            this.numLinesToParse = numLinesToParse;
            this.lineNumber = 0;
        }

        public Stream<String> parse() {
            if (numLinesToParse == 0) { //Short circuit if we don't want to parse any characters.
                return Stream.empty();
            }
            try {
                in = new BufferedReader(new FileReader(toParse));
                in.skip(start);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Iterable<String> myIterable = () -> this;
            Stream<String> result = StreamSupport.stream(myIterable.spliterator(), false);
            result.onClose(this::close);
            return result;
        }

        @Override
        public boolean hasNext() {
            if (continueParsing == false) {
                return false;
            }
            if (lineNumber >= numLinesToParse) {
                continueParsing = false;
                return false;
            }
            if (cachedNextValue != null) {
                return true;
            } else {
                try {
                    cachedNextValue = in.readLine();
                    if (cachedNextValue == null) {
                        continueParsing = false;
                        return false;
                    }
                    return true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    continueParsing = false;
                    return false;
                }
            }
        }

        @Override
        public String next() {
            if (hasNext()) {
                String result = cachedNextValue;
                lineNumber++;
                cachedNextValue = null;
                return result;
            } else {
                throw new NoSuchElementException("No more elements. Forever.");
            }
        }

        public void close() {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ex) {
                    //Om Nom Nom Nom
                }
            }
        }

    }
}
