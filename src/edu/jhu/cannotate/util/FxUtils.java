/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhu.cannotate.util;

import edu.jhu.cannotate.avro.AvroAnnotationMetadata;
import edu.jhu.cannotate.avro.AvroFileMetadataUtils;
import edu.jhu.cannotate.avro.AvroRecordType;
import edu.jhu.cannotate.record.metadata.MetadataUtils;
import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 *
 * @author newcojd1
 */
public class FxUtils {

    public static void fadeIn(double durationInSeconds, Node... nodes) {
        Duration d = Duration.seconds(durationInSeconds);
        Timeline timeline = new Timeline();
        List<KeyFrame> keyFrames = new ArrayList<>();
        for (Node n : nodes) {
            keyFrames.add(new KeyFrame(d, new KeyValue(n.opacityProperty(), 1)));
        }
        timeline.getKeyFrames().addAll(keyFrames);
        timeline.play();
    }

    public static void fadeOut(double durationInSeconds, Node... nodes) {
        Duration d = Duration.seconds(durationInSeconds);
        Timeline timeline = new Timeline();
        List<KeyFrame> keyFrames = new ArrayList<>();
        for (Node n : nodes) {
            keyFrames.add(new KeyFrame(d, new KeyValue(n.opacityProperty(), 0)));
        }
        timeline.getKeyFrames().addAll(keyFrames);
        timeline.play();
    }

    private static File prevDir = null;

    public static File browseForFile(File startDir) {
        startDir = getDirectory(startDir);
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(startDir != null ? startDir : prevDir);
        fileChooser.setTitle("Open Resource File");
        File result = fileChooser.showOpenDialog(null);
        File parent = getDirectory(result);
        if (parent != null) {
            prevDir = parent;
        }
        return result;
    }

    public static File getDirectory(File file) {
        if (file == null) {
            return null;
        } else if (file.isFile()) {
            return file.getParentFile();
        } else if (file.isDirectory()) {
            return file;
        } else {
            return null;
        }
    }

    public static boolean checkValidFile(TextField textField) {
        if (!textField.getText().equals("")) {
            File temp = new File(textField.getText());
            if (temp.isFile()) {
                return true;
            }
        }
        return false;
    }
    
    public static String browseForFilePath(File startDir) {
        File selectedFile = browseForFile(startDir);
        return selectedFile == null ? "" : selectedFile.getAbsolutePath();
    }

    public static void insertFilePathIfSelected(TextInputControl n) {
        File toSet = browseForFile(n.getText().trim().isEmpty() ? prevDir : new File(n.getText()));
        if (toSet != null) {
            n.setText(toSet.getAbsolutePath());
        }
    }

    public static void openFile(File toOpen) throws Exception {
        Desktop.getDesktop().open(toOpen);
    }
    
    public static boolean checkMetadataTable(String s) throws Exception {
        AvroFileMetadataUtils util = new AvroFileMetadataUtils();
        List<AvroAnnotationMetadata> annotationMetadataList = MetadataUtils.getMetadataByType(AvroRecordType.ANNOTATION_METADATA);
        for (AvroAnnotationMetadata annotationMetadata : annotationMetadataList) {
            if (annotationMetadata.getAnnotationName().toString().equals(s)) {
                return true;
            }
        }
        return false;
    }
}
