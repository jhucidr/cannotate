/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.validation;

import edu.jhu.cannotate.util.Utils;
import java.io.File;
import java.nio.file.Files;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class AnnovarTablesComparisonCommandGenerator {
    
    private final File vcfFile;
    private final File annovarDatabases;
    private final File annovarExecutable;
    private final File pathToDatabases;
    private static final String DELIMITER = ",";

    public AnnovarTablesComparisonCommandGenerator(File vcfFile, File annovarDatabases, File annovarExecutable, File pathToDatabases) {
        this.vcfFile = vcfFile;
        this.annovarDatabases = annovarDatabases;
        this.annovarExecutable = annovarExecutable;
        this.pathToDatabases = pathToDatabases;
    }
    
    public String build() throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(annovarExecutable.getAbsolutePath());
        sb.append(" ");
        sb.append(vcfFile.getAbsolutePath());
        sb.append(" ");
        sb.append(pathToDatabases);
        sb.append(" -out ");
        sb.append(new File(vcfFile.getParent(), Utils.before(vcfFile.getName(), ".")).getAbsolutePath());
        sb.append(" -remove -protocol ");
        sb.append(buildDatabases());
        sb.append(" -operation ");
        sb.append(buildOperationString());
        sb.append(" -nastring ");
        sb.append("."); //this is what shows up when there isn't any data.
        sb.append(" -vcfinput ");
        return sb.toString();
    }

    private String buildDatabases() throws Exception {
        try(Stream<String> lines =  Files.lines(annovarDatabases.toPath()) ) {
            return lines.map(l -> l.split(DELIMITER)[0])
                .collect(Collectors.joining(DELIMITER));
        }
    }
    
    private String buildOperationString() throws Exception {
        try(Stream<String> lines =  Files.lines(annovarDatabases.toPath()) ) {
            return lines.map(l -> l.split(DELIMITER)[1])
                .collect(Collectors.joining(DELIMITER));
        }
    }
    
    public static void main(String[] args) throws Exception {
        AnnovarTablesComparisonCommandGenerator genny 
                = new AnnovarTablesComparisonCommandGenerator(
                        new File("/Users/sean-la/Documents/sequencing/annovar/500.vcf"), 
                        new File("src/tables_config.txt"),
                        new File("/Users/sean-la/Documents/sequencing/annovar/table_annovar.pl"),
                        new File("/Users/sean-la/Documents/sequencing/annovar/down_db"));
        System.out.println(genny.build());
    }
    
}
