/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.dna;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Information from https://en.wikipedia.org/wiki/Genetic_code#RNA_codon_table
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Sep 2, 2016
 */
public class AminoAcidTranslator {

static final SetMultimap<AminoAcid, Codon> CODON_MAP = buildCodonMap();

    private static SetMultimap<AminoAcid, Codon> buildCodonMap() {
        SetMultimap<AminoAcid, Codon> result = HashMultimap.create();
        for (Codon codon : Codon.values()) {
            result.put(codon.getAminoAcid(), codon);
        }
        return result;
    }


}
