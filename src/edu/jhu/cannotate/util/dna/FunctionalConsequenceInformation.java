/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.util.dna;

import edu.jhu.cannotate.record.CodonChangeInformation;
import edu.jhu.cannotate.record.TranscriptonUnitInformation;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author newcojd1
 */
public class FunctionalConsequenceInformation {
    
    private final Set<FunctionalConsequence> consequences;
    private final CodonChangeInformation codonInformation;
    private final TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion;

    public FunctionalConsequenceInformation(Set<FunctionalConsequence> consequences, CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
        this.consequences = Collections.unmodifiableSet(consequences);
        this.codonInformation = codonInformation;
        this.transcriptionRegion = transcriptionRegion;
    }



    public CodonChangeInformation getCodonInformation() {
        return codonInformation;
    }

    public Set<FunctionalConsequence> getConsequences() {
        return consequences;
    }

    public TranscriptonUnitInformation.TranscriptionRegion getTranscriptionRegion() {
        return transcriptionRegion;
    }

    @Override
    public String toString() {
        return "FunctionalConsequenceInformation{" + "consequences=" + consequences + ", codonInformation=" + codonInformation + ", transcriptionRegion=" + transcriptionRegion + '}';
    }
    
}
