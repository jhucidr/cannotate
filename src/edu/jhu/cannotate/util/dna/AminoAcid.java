package edu.jhu.cannotate.util.dna;

import edu.jhu.cannotate.util.Utils;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Sep 2, 2016
 */
public enum AminoAcid {

    PHENYLALANINE("Phenylalanine", "Phe", 'F'),
    LEUCINE("Leucine", "Leu", 'L'),
    SERINE("Serine", "Ser", 'S'),
    TYROSINE("Tyrosine", "Tyr", 'Y'),
    TERMINATION("Termination", "Ter", 'X'), //Ochre: TAA, Amber: TAG, Opal or Umber: TGA //Stop Amino Acid
    CYSTEINE("Cysteine", "Cys", 'C'),
    TRYPTOPHAN("Tryptophan", "Trp", 'W'),
    PROLINE("Proline", "Pro", 'P'),
    HISTIDINE("Histidine", "His", 'H'),
    GLUTAMINE("Glutamine", "Gln", 'Q'),
    ARGININE("Arginine", "Arg", 'R'),
    ISOLEUCINE("Isoleucine", "Ile", 'I'),
    METHIONINE("Methionine", "Met", 'M'), //Start Amino Acid: ATG
    THREONINE("Threonine", "Thr", 'T'),
    ASPARAGINE("Asparagine", "Asn", 'N'), //Aspartate or Asparagine
    LYSINE("Lysine", "Lys", 'K'),
    VALINE("Valine", "Val", 'V'),
    ALANINE("Alanine", "Ala", 'A'),
    ASPARTATE("Aspartate", "Asp", 'D'),
    GLUTAMATE("Glutamate", "Glu", 'E'), //Glutamate or Glutamine
    GLYCINE("Glycine", "Gly", 'G'),;

    private final String fullName;
    private final String abbreviation;
    private final char abbreviationChar;

    private AminoAcid(String s, String abbreviation, char abbreviationChar) {
        this.fullName = s;
        this.abbreviation = abbreviation;
        this.abbreviationChar = abbreviationChar;
    }

    public String getFullName() {
        return fullName;
    }

    public char getAbbreviationChar() {
        return abbreviationChar;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public Set<Codon> getCodons() {
        return AminoAcidTranslator.CODON_MAP.get(this);
    }


    //TAA, TAG, TGA
    private static final Pattern FIND_ARNOLD_SCHWARZENEGGER = Pattern.compile("TAA|TAG|TGA", Pattern.CASE_INSENSITIVE);

    //TODO: If we need to do a bunch of open-reading frame manipulations, we should encapsulate all of the behaviour into a single class.
    public static List<AminoAcid> findFirstOpenReadingFrame(String dna) {
        dna = dna.toUpperCase().trim();
        dna = Utils.substring(dna, Codon.ATG.toString());
        Matcher theTerminator = FIND_ARNOLD_SCHWARZENEGGER.matcher(dna);
        int stop = -1;
        while (theTerminator.find()) {
            if (stop % 3 == 0) {
                break;
            }
            stop = theTerminator.end();
            System.out.println("regionStart = " + stop);
        }
        System.out.println("regionStart = " + stop);
        String readingFrame = dna.substring(0, stop);
        System.out.println("readingFrame = " + readingFrame);
        return translateReadingFrame(readingFrame);
    }

    public static List<AminoAcid> translateReadingFrame(String dna) {
        //Expect to start with start, and end with end.
        return Codon.translateDnaToCodons(dna)
                .stream()                
                .map(Codon::getAminoAcid)
                .collect(Collectors.toList());
    }

    public static char invertChar(char c) {
        c = Character.toUpperCase(c);
        switch (c) {
            case 'A':
                return 'T';
            case 'T':
                return 'A';
            case 'C':
                return 'G';
            case 'G':
                return 'C';
            default:
                throw new IllegalStateException("arg");

        }
    }

    public static String invertDna(String dna) {      
        StringBuilder sb = new StringBuilder();
        for(char c : dna.toCharArray()){
            sb.append(invertChar(c));
        }
        return sb.toString();
    }

    public static void main(String... args) throws Exception {        
        for(AminoAcid acid : AminoAcid.values()){
            System.out.println("acid = " + acid);
        }
//        String dna = "TCAAACCCGTACAAAGGGAAATCCACACTTTGGTGAATCGAAGCGCGGCATCAGGATTTCCTTTTGGATACCTGAAACAAAGCCCATCGTGGTCCTTAGACTTGGCACACTTACACCTGCAGCGCGCGCATGTGGAATTGGAGGCCAAGTTCGATCCCTACACCGACGTACGATGCAACTGTGTGGATGTGACGAGCTTCATTTATACGCTTCGCGCGCCGGACCGGCCTCCGCAAGGCGCAGCAGTGCACAAGCAAATGACAATTAACCACCGTGTATTCGTTATAGCATCAGGCAGTTTAAGTCGGGACAATAGGGGCCGCA";
//        List<AminoAcid> openFrame = findFirstOpenReadingFrame(dna);
//        openFrame.forEach(System.out::println);
//        long length = translateDnaToCodons(dna)
//                .stream()
//                .peek(System.out::println)
//                .map(Codon::getAminoAcid)
//                .peek(System.out::println)
//                .count();
//        System.out.println("length = " + length);
    }

}
