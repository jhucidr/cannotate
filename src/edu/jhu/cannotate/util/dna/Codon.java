/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Sep 2, 2016
 */
public enum Codon {

    TTT(AminoAcid.PHENYLALANINE),
    TTC(AminoAcid.PHENYLALANINE),
    TTA(AminoAcid.LEUCINE),
    TTG(AminoAcid.LEUCINE),
    TCT(AminoAcid.SERINE),
    TCC(AminoAcid.SERINE),
    TCA(AminoAcid.SERINE),
    TCG(AminoAcid.SERINE),
    TAT(AminoAcid.TYROSINE),
    TAC(AminoAcid.TYROSINE),
    TAA(AminoAcid.TERMINATION), //Stop Codon Ochre
    TAG(AminoAcid.TERMINATION), //Stop Codon Amber
    TGA(AminoAcid.TERMINATION), //Stop Codon Opal or Umber
    TGT(AminoAcid.CYSTEINE),
    TGC(AminoAcid.CYSTEINE),
    TGG(AminoAcid.TRYPTOPHAN),
    CTT(AminoAcid.LEUCINE),
    CTC(AminoAcid.LEUCINE),
    CTA(AminoAcid.LEUCINE),
    CTG(AminoAcid.LEUCINE),
    CCT(AminoAcid.PROLINE),
    CCC(AminoAcid.PROLINE),
    CCA(AminoAcid.PROLINE),
    CCG(AminoAcid.PROLINE),
    CAT(AminoAcid.HISTIDINE),
    CAC(AminoAcid.HISTIDINE),
    CAA(AminoAcid.GLUTAMINE),
    CAG(AminoAcid.GLUTAMINE),
    CGT(AminoAcid.ARGININE),
    CGC(AminoAcid.ARGININE),
    CGA(AminoAcid.ARGININE),
    CGG(AminoAcid.ARGININE),
    ATT(AminoAcid.ISOLEUCINE),
    ATC(AminoAcid.ISOLEUCINE),
    ATA(AminoAcid.ISOLEUCINE),
    ATG(AminoAcid.METHIONINE), //Start Codon
    ACT(AminoAcid.THREONINE),
    ACC(AminoAcid.THREONINE),
    ACA(AminoAcid.THREONINE),
    ACG(AminoAcid.THREONINE),
    AAT(AminoAcid.ASPARAGINE),
    AAC(AminoAcid.ASPARAGINE),
    AAA(AminoAcid.LYSINE),
    AAG(AminoAcid.LYSINE),
    AGT(AminoAcid.SERINE),
    AGC(AminoAcid.SERINE),
    AGA(AminoAcid.ARGININE),
    AGG(AminoAcid.ARGININE),
    GTT(AminoAcid.VALINE),
    GTC(AminoAcid.VALINE),
    GTA(AminoAcid.VALINE),
    GTG(AminoAcid.VALINE),
    GCT(AminoAcid.ALANINE),
    GCC(AminoAcid.ALANINE),
    GCA(AminoAcid.ALANINE),
    GCG(AminoAcid.ALANINE),
    GAT(AminoAcid.ASPARTATE),
    GAC(AminoAcid.ASPARTATE),
    GAA(AminoAcid.GLUTAMATE),
    GAG(AminoAcid.GLUTAMATE),
    GGT(AminoAcid.GLYCINE),
    GGC(AminoAcid.GLYCINE),
    GGA(AminoAcid.GLYCINE),
    GGG(AminoAcid.GLYCINE), //
    //  n/a(AminoAcid.ASPARTATE),
    //  n/a(AminoAcid.GLUTAMATE),
    ;
    private final AminoAcid aminoAcid;

    private Codon(AminoAcid acid) {
        this.aminoAcid = acid;
    }

    public AminoAcid getAminoAcid() {
        return aminoAcid;
    }

    public static Codon fromString(String s) {
        return Codon.valueOf(s.toUpperCase());
    }

    public boolean isStartCodon() {
        return this.aminoAcid == AminoAcid.METHIONINE;
    }

    public boolean isStopCodon() {
        return this.aminoAcid == AminoAcid.TERMINATION;
    }

    private static final Pattern p = Pattern.compile("[actgACTG]{3}");

    public static List<Codon> translateDnaToCodons(String dna) {
        if (dna.length() % 3 != 0) {
            throw new IllegalStateException("Invalid Reading Frame: must be a mutliple of three. Actual length=" + dna.length() + ". nucleotides=" + dna);
        }
        Matcher matcher = p.matcher(dna);
        List<Codon> result = new ArrayList<>();
        while (matcher.find() == true) {
            String group = matcher.group();
            result.add(Codon.fromString(group));
        }
        return result;
    }

    public static Codon translateToCodon(String dna) {
        if(dna.length() != 3) {
            throw new IllegalStateException("Invalid codon: must be length three. Actual length=" + dna.length() + ". nucleotides=" + dna);
        }
        return Codon.fromString(dna);
    }
    
}
