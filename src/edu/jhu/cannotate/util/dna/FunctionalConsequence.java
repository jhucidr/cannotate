/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.util.dna;

import edu.jhu.cannotate.avro.RegionType;
import edu.jhu.cannotate.record.CodonChangeInformation;
import edu.jhu.cannotate.record.TranscriptonUnitInformation;

/**
 *
 * @author newcojd1
 */
public enum FunctionalConsequence implements GloveFitter {
//
//    INTERGENIC("The variant is in an intergenic region", false),
//    UPSTREAM("Upstream of a gene (default length: 5K bases)", false) {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return transcriptionRegion.getRegionType() == RegionType.UPSTREAM;
//        }
//
//    },
//    UTR_5_PRIME("Variant hits 5′UTR region", false) {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return transcriptionRegion.getRegionType() == RegionType.UTR_5_PRIME;
//        }
//    },
//    START_GAINED("A variant in 5′UTR region produces a three base sequence that can be a START codon.", true) {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            if (transcriptionRegion.getRegionType() == RegionType.UTR_5_PRIME) {
//                return (codonInformation.getReferenceCodon().isStartCodon() == false) && codonInformation.getVariantCodon().isStartCodon();
//            }
//            return false;
//        }
//
//    },
//    SPLICE_SITE_ACCEPTOR("The variant hits a splice acceptor site (defined as two bases before exon start, except for the first exon).", false),
//    SPLICE_SITE_DONOR("The variant hits a Splice donor site (defined as two bases after coding exon end, except for the last exon)."),
//    START_LOST("Variant causes start codon to be mutated into a non-start codon.") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return codonInformation.getReferenceCodon().isStartCodon() && (codonInformation.getVariantCodon().isStartCodon() == false);
//        }
//
//    },
//    SYNONYMOUS_START("Variant causes start codon to be mutated into another start codon.") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            //there's only one start codon; this should not happen in earth animals.
//            return codonInformation.getReferenceCodon().isStartCodon() && codonInformation.getVariantCodon().isStartCodon();
//        }
//
//    },
//    CDS("The variant hits a CDS."),
//    EXON("The variant hits an exon.") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return transcriptionRegion.getRegionType() == RegionType.EXON;
//        }
//
//    },
//    NON_SYNONYMOUS_CODING("Variant causes a codon that produces a different amino acid") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return codonInformation.getReferenceCodon().getAminoAcid() != codonInformation.getVariantCodon().getAminoAcid();
//        }
//
//    },
//    SYNONYMOUS_CODING("Variant causes a codon that produces the same amino acid") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return codonInformation.getReferenceCodon().getAminoAcid() == codonInformation.getVariantCodon().getAminoAcid();
//        }
//
//    },
// 
//    STOP_GAINED("Variant causes a STOP codon") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return (codonInformation.getReferenceCodon().isStopCodon() == false) && codonInformation.getVariantCodon().isStopCodon();
//        }
//
//    },
//    SYNONYMOUS_STOP("Variant causes stop codon to be mutated into another stop codon.") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return codonInformation.getReferenceCodon().isStopCodon() && codonInformation.getVariantCodon().isStopCodon();
//        }
//
//    },
//    STOP_LOST("Variant causes stop codon to be mutated into a non-stop codon") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return codonInformation.getReferenceCodon().isStopCodon() && (codonInformation.getVariantCodon().isStopCodon() == false);
//        }
//
//    },
//    INTRON("Variant hits an intron. Technically, hits no exon in the transcript."),
//    UTR_3_PRIME("Variant hits 3′UTR region") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return transcriptionRegion.getRegionType() == RegionType.UTR_3_PRIME;
//        }
//
//    },
//
//    DOWNSTREAM("Downstream of a gene (default length: 5K bases)") {
//        @Override
//        public boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
//            return transcriptionRegion.getRegionType() == RegionType.DOWNSTREAM;
//        }
//
//    },
//    INTRON_CONSERVED("The variant is in a highly conserved intronic region"),
//    INTERGENIC_CONSERVED("The variant is in a highly conserved intergenic region"),;
//
//    private final String description;
//    private final boolean requiresCodonChangeInformation;
//
//    private FunctionalConsequence(String description, boolean requiresCodonChangeInformation) {
//        this.description = description;
//        this.requiresCodonChangeInformation = requiresCodonChangeInformation;
//    }

// Indel-based annotations:
//    UTR_5_DELETED("The variant deletes an exon which is in the 5′UTR of the transcript") {
//        //this is an indel
//    },
//    EXON_DELETED("A deletion removes the whole exon.") {
//        //this is an indel
//    },
//   FRAME_SHIFT("Insertion or deletion causes a frame shift") {
//        //this is an indel.
//    },
//    CODON_INSERTION("One or many codons are inserted") {
//        //indel
//    },
//    CODON_CHANGE_PLUS_CODON_INSERTION("One codon is changed and one or many codons are inserted") {
//        //indel
//    },
//    CODON_DELETION("One or many codons are deleted") {
//        //indel
//    },
//    CODON_CHANGE_PLUS_CODON_DELETION("One codon is changed and one or more codons are deleted") {
//        //indel
//    },
//    UTR_3_DELETED("The variant deletes an exon which is in the 3′UTR of the transcript") {
//        //this is an indel
//    },

}
