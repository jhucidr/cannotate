/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.dna;

import java.nio.ByteBuffer;
import java.util.List;

/**
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Sep 9, 2016
 */
public class Dna {

    private final int length;
    private final long startPosition;
    private final byte[] bytes;

    public Dna(long startPosition, String nucleotides) {
        this.startPosition = startPosition;
        this.length = nucleotides.length();
        this.bytes = DnaUtils.compressString(nucleotides);
    }

    public Dna(int length, long startPosition, ByteBuffer bytesBuff) {
        this.length = length;
        this.startPosition = startPosition;
        this.bytes = bytesBuff.array();
    }

    public Dna slice(int position) {
        long newStart = startPosition + position;
        return new Dna(newStart, substring(newStart));
    }

    public String substring(long startPosition) {
        return substring(startPosition, this.startPosition + length);
    }

    public String substring(long startPosition, long stopPosition) {
        if (startPosition < this.startPosition || stopPosition < this.startPosition) {
            throw new IllegalArgumentException("Start and stop must be >= this.startPosition.. start = " + startPosition + ". stop = " + stopPosition + ". this.startPosition = " + this.startPosition);
        }
        int start = (int) (startPosition - this.startPosition);
        int stop = (int) (stopPosition - this.startPosition);
        return _substring(start, stop);
    }

    public String _substring(int startInclusive, int stopExclusive) {
        if (startInclusive > length || stopExclusive > length) {
            throw new IllegalArgumentException("Start and stop must be <= length. start = " + startInclusive + ". stop = " + stopExclusive + ". length = " + length);
        }
        return DnaUtils.decompressData(bytes, startInclusive, stopExclusive);
    }

    public String getDna() {
        return DnaUtils.decompressData(bytes, length);
    }

    @Override
    public String toString() {
        return "Dna{" + "length=" + length + ", startPosition=" + startPosition + ", dna=" + getDna() + '}';
    }

    public long getStartPosition() {
        return startPosition;
    }

    public int getLength() {
        return length;
    }

    public long getStopPosition() {
        return getStartPosition() + getLength();
    }

    public String getRange() {
        return "[" + getStartPosition() + ", " + getStopPosition() + "]";
    }

    public boolean overlapMatches(Dna strand) {
        long start = Math.max(this.getStartPosition(), strand.getStartPosition());
        long stop = Math.min(this.getStopPosition(), strand.getStopPosition());
        if (start < stop) {
            return substring(start, stop).equals(strand.substring(start, stop));
        } else {
            System.out.println("There is no overlap between strands: " + this.getRange() + " and " + strand.getRange());
            //Throw an exception?
            return false;
        }
    }

    public ByteBuffer getBytes() {
        return ByteBuffer.wrap(bytes);
    }

    //Note: This can't easily be done because each individual DNA may have excess bits at the end of the last byte that needs to be cropped.
    //Much safer to decompress the strand into a String, and recompress into a DNA after merging via StringBuilder.
    //That said, This becomes trivial if we know that the encoded string is divisible by 8 
    //     (because every 8 characters encode 3 complete bytes, with no partial bits at the end.) 
    //Thus, with our Reference DNA objects of length 1000, we should be able to simply append together the byte buffers.
    public static Dna mergeDna(List<Dna> strands) {
        int numBytes = 0;
        int numNucleotides = 0;
        for (Dna strand : strands) {
            int strandLength = strand.getLength();
            if (strandLength % 8 != 0) {
                throw new IllegalStateException("MergeDNA is only designed to work for strands that are a multiple of 8.");
            }
            numBytes += strand.bytes.length;
            numNucleotides += strandLength;
        }
        ByteBuffer buffer = ByteBuffer.allocate(numBytes);
//        int bytePosition = 0; //Bute position
        for (Dna strand : strands) {
            buffer.put(strand.bytes);
//            bytePosition += strand.bytes.length;
        }
        return new Dna(numNucleotides, strands.get(0).getStartPosition(), buffer);
    }

    public static void main(String... args) throws Exception {
        Dna strand1 = new Dna(100, "AACCTTGGACTG");
        System.out.println("strand1 = " + strand1);
        Dna strand2 = new Dna(98, "CCAACCTTGGACTGGA");
        System.out.println("strand1.overlapMatches(strand2) = " + strand1.overlapMatches(strand2));
    }

}
