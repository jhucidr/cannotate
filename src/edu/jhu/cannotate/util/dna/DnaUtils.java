/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.dna;

import java.util.Random;
import java.util.stream.Collectors;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;

/**
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Sep 9, 2016
 */
public class DnaUtils {

    private static final boolean DEBUG_COMPRESSION = false;
    private static final boolean DEBUG_DECOMPRESSION = false;
    
    //outside the method in case there's tight looping.
    private static final Random RANDOM = new Random(67903);

    public static void main(String... args) throws Exception {
        //Right now, needs to be a multiple of 8.
//        String dna = "ACTCCAGG";
//        String dna = "CAAAAAAA";
        String dna = "ACATGTGACCCTTTAAAGAGAGAGATAACCTAGATACCATGATAAAATTATATATACCCCCCCAGGGAGGATTTTATT";
        System.out.println("dna = " + dna);
        byte[] compressed = compressString(dna);
        for (int start = 0; start <= dna.length(); start++) {
            for (int stop = start; stop <= dna.length(); stop++) {
                String decompressed = decompressData(compressed, start, stop);
                String expected = dna.substring(start, stop);
//                System.out.println("decompressed = " + decompressed);
//                System.out.println("expected     = " + );
                if (false == decompressed.equals(expected)) {
                    throw new IllegalStateException("Whattt!!!!!");
                } else {
                    System.out.println("GOOD:" + decompressed);
                }

            }
        }
    }

    //    public static Map <Integer, String> translationMap(){
//        return null;
//        //TODO: consider building a lookup map that will translate any 3-byte integer (24-bit) into the corresponding 8 letters.
    //Measure performance against existing method.
//    }
//    
    public static byte[] compressString(String nucleotides) {
        int stringIndex = 0;
        byte[] result = initArray(nucleotides);
//        System.out.println("result.length = " + result.length);
        String current = "";
        for (int raIndex = 0; raIndex < result.length; raIndex += 3) {
//            System.out.println("raIndex = " + raIndex);
            //3 Bytes => 8 chars
//            String current = nucleotides.length() >= (stringIndex + 8)
            if (nucleotides.length() >= stringIndex + 8) {
                current = nucleotides.substring(stringIndex, stringIndex + 8);
            } else {
                current = nucleotides.substring(stringIndex, nucleotides.length());
                current = padCurrentIfNecessary(current);
            }
            if (current.length() == 8) {
                int threeBytes = eightCharsToThreeByteInteger(current);
                result[raIndex] = (byte) ((threeBytes >> 16) & 0xFF);
                result[raIndex + 1] = (byte) ((threeBytes >> 8) & 0xFF);
                result[raIndex + 2] = (byte) (threeBytes & 0xFF);
//            System.out.println("result[raIndex + 0] = " + result[raIndex + 0]);
//            System.out.println("result[raIndex + 1] = " + result[raIndex + 1]);
//            System.out.println("result[raIndex + 2] = " + result[raIndex + 2]);
            } else if (current.length() == 5) {
                int twoBytes = fiveCharsToTwoByteInteger(current);
                result[raIndex] = (byte) ((twoBytes >> 8) & 0xFF);
                result[raIndex + 1] = (byte) (twoBytes & 0xFF);
            } else {
                result[raIndex] = twoCharsToOneByte(current);
            }
            stringIndex += 8;
        }
        return result;
    }

    private static byte[] initArray(String nucleotides) {
        int size = (int) Math.ceil((double) nucleotides.length() * 3d / 8d);
        return new byte[size];
    }

    public static int eightCharsToThreeByteInteger(String eightCharString) {
        return baseToBits(eightCharString.charAt(7)) 
                | baseToBits(eightCharString.charAt(6)) << 3
                | baseToBits(eightCharString.charAt(5)) << 6
                | baseToBits(eightCharString.charAt(4)) << 9
                | baseToBits(eightCharString.charAt(3)) << 12
                | baseToBits(eightCharString.charAt(2)) << 15
                | baseToBits(eightCharString.charAt(1)) << 18
                | baseToBits(eightCharString.charAt(0)) << 21;
    }

    //Right adjusted
    public static int fiveCharsToTwoByteInteger(String fiveCharString) {
        return baseToBits(fiveCharString.charAt(4))
                | baseToBits(fiveCharString.charAt(3)) << 3
                | baseToBits(fiveCharString.charAt(2)) << 6
                | baseToBits(fiveCharString.charAt(1)) << 9
                | baseToBits(fiveCharString.charAt(0)) << 12;
    }

    //Right adjusted
    public static byte twoCharsToOneByte(String twoCharString) {
        return (byte) (baseToBits(twoCharString.charAt(1))
                | baseToBits(twoCharString.charAt(0)) << 3);
    }

    public static long roundUp(long num, long divisor) {
        return (long) Math.ceil((double) num / divisor);
    }

    public static int roundUp(int num, int divisor) {
        return (int) Math.ceil((double) num / divisor);
    }

    public static void printHex(byte toPrint) {
        System.out.println("0x" + Integer.toHexString(Byte.toUnsignedInt(toPrint)));
    }

    public static void printHex(int toPrint) {
        System.out.println("0x" + Integer.toHexString(toPrint));
    }

    public static String toHex(byte b) {
        return "0x" + Integer.toHexString(Byte.toUnsignedInt(b));
    }

    public static String toHex(int toPrint) {
        return "0x" + Integer.toHexString(toPrint);
    }

//    public static String decompressData(byte[] toDecompress) {
//        return decompressData(toDecompress, 0, toDecompress.length * 8 / 3);
//    }

    public static String decompressData(byte[] toDecompress, int stringLength) {
        return decompressData(toDecompress, 0, stringLength);
    }

    public static String decompressData(byte[] toDecompress, int startingChar, int stoppingChar) {
        
        if (startingChar < 0 || stoppingChar < 0) {
            throw new IllegalArgumentException("Start and stop must be >= 0. start = " + startingChar + ". stop = " + stoppingChar);
        }
        if (startingChar > stoppingChar) {
            throw new IllegalArgumentException("Start must be <= stop. start = " + startingChar + ". stop = " + stoppingChar);
        }
        
        if(startingChar == stoppingChar) {
            return "";
        }
        
        StringBuilder sb = new StringBuilder();
        int firstByte = Math.floorDiv(startingChar, 8) * 3;
        int leftOffset = startingChar % 8;
        int stop = stoppingChar - startingChar + leftOffset;
        int lastByte = firstByte + ((stop - 1) * 3 / 8);
        if (lastByte > toDecompress.length) {
            throw new IllegalStateException("The last byte exceeds the length of the byte array.");
        }
        
        for (int i = firstByte; i <= lastByte; i += 3) {
            if (i + 2 < toDecompress.length) {
                int compressed = Byte.toUnsignedInt(toDecompress[i]) << 16
                        | Byte.toUnsignedInt(toDecompress[i + 1]) << 8
                        | Byte.toUnsignedInt(toDecompress[i + 2]);
//                printHex(compressed);
                sb.append(threeBytesToString(compressed));
            } else if (i + 1 < toDecompress.length) {
                int compressed = Byte.toUnsignedInt(toDecompress[i]) << 8
                        | Byte.toUnsignedInt(toDecompress[i + 1]);
//                printHex(compressed);
                sb.append(twoBytesToString(compressed));
            } else {
//                printHex(toDecompress[i]);
                sb.append(oneByteToString(toDecompress[i]));
            }
        }
//        System.out.println("leftOffset = " + leftOffset);
//        System.out.println("stoppingChar = " + stoppingChar);
        return sb.substring(leftOffset, stop);
    }

    public static String threeBytesToString(int threeBytes) {
        StringBuilder sb = new StringBuilder();
        sb.append(bitsToBase((threeBytes >> 21) & 0x7));
        sb.append(bitsToBase((threeBytes >> 18) & 0x7));
        sb.append(bitsToBase((threeBytes >> 15) & 0x7));
        sb.append(bitsToBase((threeBytes >> 12) & 0x7));
        sb.append(bitsToBase((threeBytes >> 9) & 0x7));
        sb.append(bitsToBase((threeBytes >> 6) & 0x7));
        sb.append(bitsToBase((threeBytes >> 3) & 0x7));
        sb.append(bitsToBase(threeBytes & 0x7));
        return sb.toString();
    }

    public static String twoBytesToString(int threeBytes) {
        StringBuilder sb = new StringBuilder();
        sb.append(bitsToBase((threeBytes >> 12) & 0x7));
        sb.append(bitsToBase((threeBytes >> 9) & 0x7));
        sb.append(bitsToBase((threeBytes >> 6) & 0x7));
        sb.append(bitsToBase((threeBytes >> 3) & 0x7));
        sb.append(bitsToBase(threeBytes & 0x7));
        return sb.toString();
    }

    public static String oneByteToString(int threeBytes) {
        StringBuilder sb = new StringBuilder();
        sb.append(bitsToBase((threeBytes >> 3) & 0x7));
        sb.append(bitsToBase(threeBytes & 0x7));
        return sb.toString();
    }

    public static char bitsToBase(int threeBits) {
        switch (threeBits) {
            case 0:
                return 'A';
            case 1:
                return 'C';
            case 2:
                return 'G';
            case 3:
                return 'T';
            case 4:
                return 'N';
            case 5:
            case 6:
            case 7:
                return 'X';
            default:
                throw new IllegalStateException("Invalid bits: " + threeBits + ". Was expecting an int in the range [0,7].");
        }
    }
    /*
     /This turns a char into a 3-bit integer
     */

    public static int baseToBits(char c) {
        switch (c) {
            case 'a':
            case 'A':
                return 0;
            case 'c':
            case 'C':
                return 1;
            case 'g':
            case 'G':
                return 2;
            case 't':
            case 'T':
            case 'u':
            case 'U':
                return 3;
            case 'N':
            case 'n':
            case ' ':
            case '.':
                return 4;
            default:
                return 0b111; //This is the MAX VALUE that can be returned (using max of 3 bits). 0b111 = 7
        }
    }

    private static String padCurrentIfNecessary(String current) {
        switch (current.length()) {
            case 1:
                return current + "X";
            case 2:
                return current;
            case 3:
                return current + "XX";
            case 4:
                return current + "X";
            case 5:
                return current;
            case 6:
                return current + "XX";
            case 7:
                return current + "X";
            case 8:
                return current;
            default:
                throw new IllegalStateException("Unexpected string length: " + current.length() + ". value = " + current);
        }

    }


    
    public static String generateRandomDNA(long length) {
        return RANDOM.ints(length, 0, 3)
                .mapToObj(DnaUtils::mapDNA)
                .collect(Collectors.joining()); 
    }

    private static String mapDNA(int val) {
        switch(val % 4) {
            case 0: return "A";
            case 1: return "T";
            case 2: return "C";
            case 3: return "G";
            default: throw new IllegalStateException("DNA just isn't: " + (val % 4));
        }
    }
    
}
