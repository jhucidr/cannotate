/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.util.dna;

import edu.jhu.cannotate.record.CodonChangeInformation;
import edu.jhu.cannotate.record.TranscriptonUnitInformation;

/**
 *
 * @author newcojd1
 */
public interface GloveFitter {

    public default boolean doesTheGloveFit(CodonChangeInformation codonInformation, TranscriptonUnitInformation.TranscriptionRegion transcriptionRegion) {
        return false;
    }
}
