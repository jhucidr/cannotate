/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.stream;

import edu.jhu.cannotate.util.functional.ExceptionalRunnable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamMerge<T, V, R> {

    private final Stream<T> ts;
    private final Stream<V> vs;

    private OrderHandler<T, V> oh = (T t, V v) -> {
        return IncrementType.STREAM_ONE;
    };

    private ElementHandler<T, R> eh1 = (T t) -> new ArrayList<>();
    private ElementHandler<V, R> eh2 = (V v) -> new ArrayList<>();

    public StreamMerge(Iterator<T> it1, Iterator<V> it2) {
        this(() -> it1, () -> it2);
    }

    public StreamMerge(Iterable<T> ts, Iterable<V> vs) {
        this.ts = StreamSupport.stream(ts.spliterator(), false);
        this.vs = StreamSupport.stream(vs.spliterator(), false);
    }

    public StreamMerge(Stream<T> ts, Stream<V> vs) {
        this.ts = ts;
        this.vs = vs;
    }

    public void setElementHandlerOne(ElementHandler<T, R> eh1) {
        this.eh1 = eh1;
    }

    public void setElementHandlerTwo(ElementHandler<V, R> eh2) {
        this.eh2 = eh2;
    }

    public void setOrderHandler(OrderHandler<T, V> oh) {
        this.oh = oh;
    }

    public Stream<R> stream() {
        EntryAccumulator myIterator = new EntryAccumulator();
        Iterable<R> myIterable = () -> myIterator;
        Stream<R> result = StreamSupport.stream(myIterable.spliterator(), false);
        result.onClose(ExceptionalRunnable.of(myIterator::close));
        return result;
    }

    public static enum IncrementType {

        STREAM_ONE,
        STREAM_TWO;
    }

    @FunctionalInterface
    public static interface OrderHandler<T, V> {

        public IncrementType increment(T t, V v);
    }

    @FunctionalInterface
    public static interface ElementHandler<T, R> {

        public Collection<R> handleElement(T toHandle);
    }

    private class EntryAccumulator implements Iterator<R>, AutoCloseable {

        private final Iterator<T> it1;
        private final Iterator<V> it2;
        private T t;
        private V v;

        private Collection<R> cachedNextEntry = null;
        private boolean isFinished = false;

        public EntryAccumulator() {
            this.it1 = ts.iterator();
            this.it2 = vs.iterator();
            this.t = it1.hasNext() ? it1.next() : null;
            this.v = it2.hasNext() ? it2.next() : null;
        }

        @Override
        public R next() {
            if (hasNext()) {
                R retVal = cachedNextEntry.iterator().next();
                cachedNextEntry.remove(retVal);
                return retVal;
            } else {
                throw new NoSuchElementException("No more elements. Forever.");
            }
        }

        @Override
        public boolean hasNext() {
            if (cachedNextEntry != null && cachedNextEntry.size() > 0) {
                return true;
            }
            if (isFinished) {
                return false;
            }

            while (isFinished == false) {
                IncrementType type = oh.increment(t, v);
                switch (type) {
                    case STREAM_ONE:
                        if (it1.hasNext()) {
                            t = it1.next();
                            cachedNextEntry = new ArrayList<>(eh1.handleElement(t));
                        } else if (it2.hasNext()) {
                            v = it2.next();
                            cachedNextEntry = new ArrayList<>(eh2.handleElement(v));
                        } else {
                            isFinished = true;
                        }
                        break;
                    case STREAM_TWO:
                        if (it2.hasNext()) {
                            v = it2.next();
                            cachedNextEntry = new ArrayList<>(eh2.handleElement(v));
                        } else if (it1.hasNext()) {
                            t = it1.next();
                            cachedNextEntry = new ArrayList<>(eh1.handleElement(t));
                        } else {
                            isFinished = true;
                        }
                        break;
                    default:
                        throw new IllegalStateException("Unrecognized Increment Type: " + type);
                }
                if (cachedNextEntry != null && cachedNextEntry.size() > 0) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public void close() {
            ts.close();
            vs.close();
        }
    }
}
