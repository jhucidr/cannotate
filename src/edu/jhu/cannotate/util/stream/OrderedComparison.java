/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.stream;

import edu.jhu.cannotate.util.functional.ExceptionalRunnable;
import edu.jhu.cannotate.util.functional.Tuple2;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This is a utility class that will compare two ordered collections, iterables,
 * or streams, and perform an operation whenever a match is found.
 *
 *
 */
public class OrderedComparison<T, V> implements AutoCloseable {

    private final Stream<T> ts;
    private final Stream<V> vs;

    private OrderHandler<T, V> oh = (T t, V v) -> {
        return 0;
    };

    private Accumulator<T, V> accumulator = null;

    public OrderedComparison(Iterator<T> it1, Iterator<V> it2) {
        this(() -> it1, () -> it2);
    }

    public OrderedComparison(Iterable<T> ts, Iterable<V> vs) {
        this.ts = StreamSupport.stream(ts.spliterator(), false);
        this.vs = StreamSupport.stream(vs.spliterator(), false);
    }

    public OrderedComparison(Stream<T> ts, Stream<V> vs) {
        this.ts = ts;
        this.vs = vs;
    }

    public void setOrderHandler(OrderHandler<T, V> handler) {
        this.oh = handler;
    }

    public void setAccumulator(Accumulator<T, V> accu) {
        this.accumulator = accu;
    }

    public Stream<Entry<T, V>> stream() {
        MyIterator<T, V> myIterator = (accumulator == null) ? new EntryIterator() : new EntryAccumulator();
        Iterable<Entry<T, V>> myIterable = () -> myIterator;
        Stream<Entry<T, V>> result = StreamSupport.stream(myIterable.spliterator(), false);
        result.onClose(ExceptionalRunnable.of(myIterator::close));
        return result;
    }

    private interface MyIterator<T, V> extends Iterator<Entry<T, V>>, AutoCloseable {
    }

    private class EntryIterator implements MyIterator<T, V> {

        private final Iterator<T> it1 = ts.iterator();
        private final Iterator<V> it2 = vs.iterator();
        private Entry<T, V> cachedNextEntry;

        @Override
        public Entry<T, V> next() {
            if (hasNext()) {
                Entry<T, V> retVal = cachedNextEntry;
                cachedNextEntry = null;
                return retVal;
            } else {
                throw new NoSuchElementException("No more elements. Forever.");
            }
        }

        @Override
        public boolean hasNext() {
            if (cachedNextEntry != null) {
                return true;
            }
            if (it1.hasNext() && it2.hasNext()) {
                T t = it1.next();
                V v = it2.next();
                while (true) {
                    int signum = oh.signum(t, v);
                    if (signum == 0) {
                        cachedNextEntry = new AbstractMap.SimpleEntry<>(t, v);
                        return true;
                    } else if (signum > 0) {
                        // t is bigger; increment v
                        if (it2.hasNext()) {
                            v = it2.next();
                        } else {
                            return false;
                        }
                    } else if (signum < 0) {
                        // v is bigger; increment t
                        if (it1.hasNext()) {
                            t = it1.next();
                        } else {
                            return false;
                        }
                    }
                }
            }
            return false;
        }

        @Override
        public void close() {
            ts.close();
            vs.close();
        }

    }

    @Override
    public void close() throws Exception {
        try {
            ts.close();
        } catch (Exception ex) {
        }
        try {
            vs.close();
        } catch (Exception ex) {
        }
    }

    @FunctionalInterface
    public static interface OrderHandler<T, V> {

        /**
         * This method determines which stream to increment
         */
        public int signum(T t, V v);

    }

    @FunctionalInterface
    public static interface Accumulator<T, V> {

        /**
         * This method accumulates results for one-to-many relationships.
         */
        public Collection<Tuple2<T, V>> accumulate(T t, V v);
    }

    private class EntryAccumulator implements MyIterator<T, V> {

        private final Iterator<T> it1 = ts.iterator();
        private final Iterator<V> it2 = vs.iterator();
        private Collection<Tuple2<T, V>> cachedNextEntry = null;
        private boolean isFinished = false;

        private T t = null;// it1.next();
        private V v = null;// it2.next();

        @Override
        public Entry<T, V> next() {
            if (hasNext()) {
                Tuple2<T, V> retVal = cachedNextEntry.iterator().next();
                cachedNextEntry.remove(retVal);
                return retVal.toEntry();
            } else {
                throw new NoSuchElementException("No more elements. Forever.");
            }
        }

        @Override
        public boolean hasNext() {
            if (cachedNextEntry != null && cachedNextEntry.size() > 0) {
                return true;
            }
            if (isFinished) {
                return false;
            }
            if (t == null && it1.hasNext()) {
                t = it1.next();
            }
            if (v == null && it2.hasNext()) {
                v = it2.next();
            }
            while (isFinished == false) {
                int signum = oh.signum(t, v);
                cachedNextEntry = accumulator.accumulate(t, v);
                if (signum == 0) {
                    throw new IllegalStateException("Oops... You're doing it wrong. Please choose a stream to increment.");
                } else if (signum > 0) {
                    // t is bigger; increment v
                    if (it2.hasNext()) {
                        v = it2.next();
                    } else if (it1.hasNext()) {
                        t = it1.next();
                    } else {
                        isFinished = true;
                    }
                } else if (signum < 0) {
                    // v is bigger; increment t
                    if (it1.hasNext()) {
                        t = it1.next();
                    } else if (it2.hasNext()) {
                        v = it2.next();
                    } else {
                        isFinished = true;
                    }
                }
                if (cachedNextEntry != null && cachedNextEntry.size() > 0) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public void close() {
            ts.close();
            vs.close();
        }
    }
}
