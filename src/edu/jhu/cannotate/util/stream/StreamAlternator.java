/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.util.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class will accept many Stream<T>, and returns a single Stream<T> where
 * each element shuffles between the source streams, in order, while each source
 * stream contains data.
 *
 * As each source Stream becomes exhausted, it is removed from the shuffle, and
 * the output will continue to shuffle between the remaining streams.
 *
 *
 * @created Nov 7, 2015
 */
public class StreamAlternator<T> implements Iterator<T>, AutoCloseable {

    private final List<Stream<T>> streamList;
    private final List<Iterator<T>> iteratorList;
    private int counter = 0;

    public StreamAlternator(Collection<Stream<T>> streamList) {
        Objects.requireNonNull(streamList);
        if (streamList.isEmpty()) {
            throw new IllegalArgumentException("streamList must not be empty.");
        }
        this.streamList = new ArrayList<>(streamList);
        this.iteratorList = this.streamList.stream().map(Stream::iterator).collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public boolean hasNext() {
        if (iteratorList.isEmpty()) {
            return false;
        }
        while (false == iteratorList.get(counter).hasNext()) {
            iteratorList.remove(counter);
            Stream<T> finishedStream = streamList.remove(counter);
            finishedStream.close();
            if (iteratorList.isEmpty()) {
                return false;
            }
            if (counter >= iteratorList.size()) {
                counter = 0;
            }
        }
        return true;
    }

    @Override
    public T next() {
        if (hasNext()) {
            T result = iteratorList.get(counter).next();
            counter++;
            if (counter >= iteratorList.size()) {
                counter = 0;
            }
            return result;
        } else {
            throw new NoSuchElementException("No more elements. Forever.");
        }
    }

    @Override
    public void close() {
        for (Stream<T> stream : streamList) {
            try {
                stream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        streamList.clear();
        iteratorList.clear();
    }

    @SafeVarargs
    public static <T> Stream<T> of(Stream<T>... streams) {
        return of(Arrays.asList(streams));
    }

    public static <T> Stream<T> of(Stream<Stream<T>> streams) {
        return of(streams.collect(Collectors.toList()));
    }

    public static <T> Stream<T> of(Collection<Stream<T>> streams) {
        StreamAlternator<T> iterator = new StreamAlternator<>(streams);
        Iterable<T> myIterable = () -> iterator;
        Stream<T> result = StreamSupport.stream(myIterable.spliterator(), false);
        result.onClose(iterator::close);
        return result;
    }

}
