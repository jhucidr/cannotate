/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 */
package edu.jhu.cannotate.reference;

import edu.jhu.cannotate.record.AccumuloReferenceGenomeRecord;
import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.retrieve.ReferenceRetriever;
import edu.jhu.cannotate.util.dna.Dna;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author newcojd1
 */
public class ReferenceDictionary {

    private static final Map<SimplePosition, SoftReference<Dna>> REFERENCE_DICTIONARY = new HashMap<>();
    private static final long NUCLEOTIDE_BATCH_SIZE = 100_000L;

    private static void populateDictionary(GenomicPosition position) throws Exception {
        if (position.getDnaLength() < NUCLEOTIDE_BATCH_SIZE) {
            //Minimize the number of calls to populate dictionary.
            // NUCLEOTIDE_BATCH_SIZE of 100,000 nucleotides translates to 100 reference records from the table.
            position = new GenomicPosition(position.getChromosome(), position.getStartPosition(), position.getStartPosition() + NUCLEOTIDE_BATCH_SIZE);
        }
        List<AccumuloReferenceGenomeRecord> reference = ReferenceRetriever.getReference(position);
        reference.stream().forEach((accumuloRecord) -> {
            SimplePosition key = new SimplePosition(new GenomicPosition(accumuloRecord.getChromosome(), accumuloRecord.getStartAsLong(), accumuloRecord.getStopAsLong()));
            SoftReference<Dna> value = new SoftReference<>(accumuloRecord.getDna());
            REFERENCE_DICTIONARY.put(key, value);
        });
    }

    private static List<SimplePosition> buildSimplePositionList(GenomicPosition position) {
        int dnaLenth = ReferenceIngester.getDnaLength();
        long startPosition = position.getStartPosition();
        long stopPosition = position.getStopPosition();
        long startRowNum = startPosition - (startPosition % dnaLenth);
        long stopRowNum = stopPosition + dnaLenth - (stopPosition % dnaLenth);
        long currentStartRow = startRowNum;
        List<SimplePosition> result = new ArrayList<>();
        while (currentStartRow < stopRowNum) {
            SimplePosition sp = new SimplePosition(new GenomicPosition(position.getChromosome(), currentStartRow, currentStartRow + dnaLenth));
            result.add(sp);
//            Dna dna = fromSimplePosition(sp);
            currentStartRow += dnaLenth;
        }
        return result;
    }

    private static void tryToPopulateDictionary(GenomicPosition position, int populateLimit) {
        if (populateLimit < 0) {
            throw new IllegalStateException("Populate limit reached when retrieving reference for " + position);
        }
        try {
            populateDictionary(position);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static String fromDictionary(GenomicPosition position) {
        List<Dna> dnaList = new ArrayList<>();
        List<SimplePosition> simplePos = buildSimplePositionList(position);
        int populateLimit = 3; //Make sure we don't get stuck in a tight loop of populating
        for (SimplePosition simple : simplePos) {
            SoftReference<Dna> ref = REFERENCE_DICTIONARY.get(simple);
            if (ref == null) {
                tryToPopulateDictionary(position, --populateLimit);
                ref = REFERENCE_DICTIONARY.get(simple);
                if (ref == null) {
                    throw new IllegalStateException("Unable to retrieve " + position.getStartPosition() + " from the database for GenomicPosition " + position);
                }
            }
            Dna dna = ref.get();
            if (dna == null) {
                tryToPopulateDictionary(position, --populateLimit);
                dna = REFERENCE_DICTIONARY.get(simple).get();
            }
            dnaList.add(dna);
        }
        Dna mergeDna = Dna.mergeDna(dnaList);
        return mergeDna.substring(position.getStartPosition(), position.getStopPosition() + 1);
    }

    public static Dna fromDictionaryCompressed(GenomicPosition position) {
        String dna = fromDictionary(position);
        return new Dna(position.getStartPosition(), dna);
    }

    public static void main(String... args) throws Exception {
        List<GenomicPosition> posList = Arrays.asList(
                new GenomicPosition("1", 123456, 129876),
                new GenomicPosition("1", 123500, 123550),
                new GenomicPosition("1", 123505, 123510),
                new GenomicPosition("1", 435000, 436500),
                new GenomicPosition("1", 135250, 135500),
                new GenomicPosition("1", 135300, 135500),
                new GenomicPosition("1", 140250, 140500),
                new GenomicPosition("1", 140250, 140750)
        );
        long currentTime = System.currentTimeMillis();
        for (GenomicPosition genomicPosition : posList) {
            long newTime = System.currentTimeMillis();
            System.out.println(newTime - currentTime);
            currentTime = newTime;
            String ref = fromDictionary(genomicPosition);
            System.out.println("ref.length() = " + ref.length());
            System.out.println("genomicPosition.calculateWidth() = " + genomicPosition.calculateWidth());
        }
    }

    private static class SimplePosition {
        //Wraps Genomic Position, but is agnostic to alt and ref.

        private final GenomicPosition myPosition;

        public SimplePosition(GenomicPosition myPosition) {
            this.myPosition = myPosition;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final SimplePosition other = (SimplePosition) obj;
            if (!Objects.equals(this.myPosition.getChromosome(), other.myPosition.getChromosome())) {
                return false;
            }
            if (this.myPosition.getStartPosition() != other.myPosition.getStartPosition()) {
                return false;
            }
            if (this.myPosition.getStopPosition() != other.myPosition.getStopPosition()) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 29 * hash + Objects.hashCode(myPosition.getChromosome());
            hash = 29 * hash + (int) (myPosition.getStartPosition() ^ (myPosition.getStartPosition() >>> 32));
            hash = 29 * hash + (int) (myPosition.getStopPosition() ^ (myPosition.getStopPosition() >>> 32));
            return hash;
        }

    }
}
