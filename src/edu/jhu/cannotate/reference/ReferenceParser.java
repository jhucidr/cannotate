/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.reference;

import edu.jhu.cannotate.avro.AvroGenomeSequence;
import edu.jhu.cannotate.avro.AvroTranslator;
import edu.jhu.cannotate.avro.AvroUtils;
import edu.jhu.cannotate.util.dna.Dna;
import edu.jhu.cannotate.util.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */

public class ReferenceParser implements AutoCloseable, Iterator<DnaWithMetaData> {

    private final File referenceFile;
    private BufferedReader in;
    private String cachedNextValue;
    private boolean continueParsing = true;
    private RegionMetaData currentMetaData;
    private long currentPosition = 0;
    public static final String METADATA_CHAR = ">";
    

    public ReferenceParser(File referenceFile) {
        this.referenceFile = referenceFile;
    }

    public Stream<DnaWithMetaData> parse() throws Exception {
        in = new BufferedReader(new FileReader(referenceFile));
        Iterable<DnaWithMetaData> myIterable = () -> this;
        Stream<DnaWithMetaData> result = StreamSupport.stream(myIterable.spliterator(), false);
        result.onClose(this::close);
        return result;
    }

    @Override
    public void close() {
        try {
            in.close();
        } catch (IOException ex) {
            throw new IllegalStateException("Issue closing file reader for reference file:" + referenceFile, ex);
        }
    }

    @Override
    public boolean hasNext() {
        if (continueParsing == false) {
            return false;
        }
        if (cachedNextValue != null) {
            return true;
        }
        try {
            String nextLine = in.readLine();
            if (nextLine == null) {
                continueParsing = false;
                return false;

            }
            while (nextLine != null && nextLine.startsWith(METADATA_CHAR)) {
                currentMetaData = new RegionMetaData(nextLine);
                currentPosition = 0;
                nextLine = in.readLine();
            }
            cachedNextValue = nextLine;
            if (cachedNextValue == null) {
                continueParsing = false;
                return false;
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            continueParsing = false;
            return false;
        }

    }

    @Override
    public DnaWithMetaData next() {
        if (hasNext()) {
            DnaWithMetaData result = buildDnaWithMetaData(cachedNextValue);
            cachedNextValue = null;
            return result;
        } else {
            throw new NoSuchElementException("No more elements. Forever.");
        }
    }

    private DnaWithMetaData buildDnaWithMetaData(String readLine) {
        Dna dna = new Dna(currentPosition, readLine);
        currentPosition += readLine.length();
        return new DnaWithMetaData(dna, currentMetaData.getChromosome());
    }

    public static class RegionMetaData {

        private final String chromosome;
        private final String everythang;

        public RegionMetaData(String record) {
            this.chromosome = Utils.chromosomeConformist(Utils.between(record, ">", " "));
            this.everythang = record;
        }

        public String getChromosome() {
            return chromosome;
        }

    }

    public static void main(String[] args) throws Exception {
        Stream<DnaWithMetaData> dnaz = new ReferenceParser(new File("/Users/sean-la/Documents/sequencing/reference/GRCh38_full_analysis_set_plus_decoy_hla.fa")).parse();
        AvroUtils.toFile(
                dnaz.map(AvroTranslator::toAvroDna), 
                new File("/Users/sean-la/Documents/sequencing/reference/compressed_avro_reference"), 
                AvroGenomeSequence.getClassSchema());
    }

    public static long COUNTER = 0;

//    public static String printEveryOnceInAWhile(DnaWithMetaData dnaWithMetaData) {
//        COUNTER++;
//        try {
//            String dnaString = dnaWithMetaData.getDna().getDna();
//            if (COUNTER % 100000 == 0) {
//                System.out.println(dnaWithMetaData.getChromosome());
//            }
//            return dnaString;
//        } catch (Exception e) {
//            System.out.println("WEEWOOOO WEEEWOOO: " + dnaWithMetaData.getDnaString() + " " + dnaWithMetaData.getDnaString().length());
//            System.out.println("THE GUY " + new Dna(0, dnaWithMetaData.getDnaString()).getDna());
//            throw e;
//        }
//    }

}
