/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.reference;

import edu.jhu.cannotate.record.AccumuloReferenceGenomeRecord;
import edu.jhu.cannotate.table.AccumuloTable;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.dna.Dna;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.accumulo.core.data.Mutation;

/**
 *
 * @author David Newcomer<david.newcomer@jhuapl.edu>
 * @created Sep 20, 2016
 */
public class ReferenceIngester {

    private final File referenceFile;
    private final int dnaLength;
    private List<DnaWithMetaData> toStore = new ArrayList<>();
//    private DnaWithMetaData current;
    private String currentChr;
    private static final int DNA_LENGTH = 1000;
    private final Optional<String> chromosome;

    public ReferenceIngester(File inputFile) {
//        this(inputFile, "2");
        this.referenceFile = inputFile;
        this.dnaLength = DNA_LENGTH;
        this.chromosome = Optional.empty();
    }

    public ReferenceIngester(File inputFile, String chromosome) {
        this.referenceFile = inputFile;
        this.dnaLength = DNA_LENGTH;
        this.chromosome = Optional.of(Utils.chromosomeConformist(chromosome));
    }

    public static int getDnaLength() {
        return DNA_LENGTH;
    }

    public void ingestData() throws Exception {
        ReferenceParser referenceParser = new ReferenceParser(referenceFile);
        referenceParser.parse().forEachOrdered(this::processRecord);

    }

    private boolean currentIsReadyToStore() {
        Dna first = toStore.get(0).getDna();
        Dna last = toStore.get(toStore.size() - 1).getDna();
        return dnaLength <= (last.getStopPosition() - first.getStartPosition());
    }

    private DnaWithMetaData mergeCached() {
        StringBuilder sb = new StringBuilder();
        int count = dnaLength;
        List<DnaWithMetaData> toRemove = new ArrayList<>();
        for (DnaWithMetaData dna : toStore) {
            toRemove.add(dna);
            if (count > dna.getDna().getLength()) {
                sb.append(dna.getDna().getDna());
                count -= dna.getDna().getLength();
            } else {
                sb.append(dna.getDna()._substring(0, count));
                break;
            }
        }
        toStore.removeAll(toRemove);
        if (sb.length() == dnaLength && count > 0) {
            DnaWithMetaData lastMeta = toRemove.get(toRemove.size() - 1);
            toStore.add(0, lastMeta.slice(count));
        }
        Dna merged = new Dna(toRemove.get(0).getDna().getStartPosition(), sb.toString());
        return new DnaWithMetaData(merged, currentChr);
    }

    private final List<Mutation> batch = new ArrayList<>();
    private static final int BATCH_SIZE = 1000;

    private void storeMutation() {
        if (false == toStore.isEmpty()) {
            try {
                DnaWithMetaData merged = mergeCached();
                AccumuloReferenceGenomeRecord record = AccumuloReferenceGenomeRecord.buildReferenceGenomeRecord(referenceFile, merged);
                batch.add(record.buildMutation());
                if (batch.size() >= BATCH_SIZE) {
                    AccumuloTable.TESTY_MCTEST_FACE.update(batch);
                    batch.clear();
                }

            } catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        }
    }

    private void processRecord(DnaWithMetaData dna) {
        if (currentChr == null) {
            currentChr = dna.getChromosome();
        }
        if (chromosomeChanged(dna)) {
            storeMutation();
            currentChr = dna.getChromosome();
            System.out.println("currentChr = " + currentChr);
        }
        if (chromosome.isPresent() && (false == chromosome.get().equalsIgnoreCase(currentChr))) {
            return;
        }
        toStore.add(dna);
        if (currentIsReadyToStore()) {
            storeMutation();
        }
    }

    private boolean chromosomeChanged(DnaWithMetaData dna) {
        return false == currentChr.equals(dna.getChromosome());
    }

}
