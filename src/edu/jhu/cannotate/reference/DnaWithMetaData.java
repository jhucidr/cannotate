/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.reference;

import edu.jhu.cannotate.record.GenomicPosition;
import edu.jhu.cannotate.util.Utils;
import edu.jhu.cannotate.util.dna.Dna;


/**
 *
 * @author Sean Griffith <S.Griffith@jhu.edu>
 */
public class DnaWithMetaData {

    private final Dna dna;
    private final String chromosome;

    public DnaWithMetaData(Dna dna, String chromosome) {
        this.dna = dna;
        this.chromosome = Utils.chromosomeConformist(chromosome);
    }

    public String getChromosome() {
        return chromosome;
    }

    public Dna getDna() {
        return dna;
    }

    public GenomicPosition getGenomicPosition() {
        return new GenomicPosition(chromosome, dna.getStartPosition(), dna.getStopPosition());
    }

    @Override
    public String toString() {
        return "DnaWithMetaData{" + "dna=" + dna + ", chromosome=" + chromosome + '}';
    }

    public DnaWithMetaData slice(int position) {        
        return new DnaWithMetaData(this.dna.slice(position), getChromosome());
    }

}
