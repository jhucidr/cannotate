/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.table;

import edu.jhu.cannotate.dispatch.Dispatcher;
import edu.jhu.cannotate.util.accumulo.AccumuloUtils;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;

/**
 *
 *
 * @created Jun 12, 2015
 */
public enum AccumuloTable {

    ANNOTATION_DATA("annotation_data"),
    METADATA("cannotate_metadata"),
    TESTY_MCTEST_FACE("the_testiest_of_tables");
    private final String tableName;

    private AccumuloTable(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }

    private static final Map<String, AccumuloTable> FROM_STRING = Arrays.stream(AccumuloTable.values())
            .collect(Collectors.toMap(v -> v.getTableName().toLowerCase(), v -> v));

    public static AccumuloTable fromString(String tableName) {
        return FROM_STRING.get(tableName.trim().toLowerCase());
    }

    private static boolean isYes(String yes){
        yes = yes.toLowerCase();
        return yes.equals("y") || yes.equals("yes");
    }
    
    public static void createTables() throws Exception {
        for (AccumuloTable table : AccumuloTable.values()) {
            //TODO: these splits aren't neccesarily right.
            if (table.exists()) {
                System.out.println(table.tableName + " already exists. Delete it? y/n");
                java.util.Scanner s = new java.util.Scanner(System.in);
                String result = s.nextLine();
                if (isYes(result)) {
                    System.out.println("Deleting " + table.tableName + "...");
                    AccumuloUtils.deleteTable(table.getTableName());
                } else {
                    System.out.println("Keeping existing version of " + table.tableName);
                }
            }
            AccumuloUtils.createTable(table.getTableName());
            SortedSet<Text> reelSplits = new TreeSet<>();
            List<String> splits = AccumuloUtils.getSplits(Dispatcher.getConfig().getNumNodesInt());
            splits.stream().forEach((split) -> {
                reelSplits.add(new Text(split));
            });
            if (false == reelSplits.isEmpty()) {
                AccumuloUtils.getTableOperations().addSplits(table.getTableName(), reelSplits);
            }
        }
    }

    public Scanner buildScanner() throws Exception {
        return AccumuloUtils.scanTable(tableName);
    }

    //remember to close the stream
    public Stream<Entry<Key, Value>> streamTable() throws Exception {
        return AccumuloUtils.streamTable(tableName);
    }

    public boolean exists() throws Exception {
        return AccumuloUtils.tableExists(this.tableName);
    }

    public void createTable() throws Exception {
        AccumuloUtils.createTable(this.tableName);
    }

    public void update(Collection<Mutation> mutations) throws Exception {
        AccumuloUtils.updateTable(this.tableName, mutations);
    }

    public void update(Mutation mutation) throws Exception {
        AccumuloUtils.updateTable(this.tableName, mutation);
    }

}
