/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.config;

import edu.jhu.cannotate.util.Utils;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;

public class AccumuloConfiguration {

    private String instance;
    private String zookeepers;
    private String user;
    private String pass;
    private String numNodes;
    private String batchWriterNumThreads;
    private String batchWriterMaxRam;
    

    public AccumuloConfiguration() {
    }

    public static AccumuloConfiguration read(String filePath) {
        AccumuloConfiguration configuration = new AccumuloConfiguration();
        if (Paths.get(filePath).toFile().exists()) {
            try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
                lines
                        .filter(Utils::filterComments)
                        .forEach(s -> Config.set(s, configuration));
            } catch (IOException ex) {
                //TODO: we need a logging solution
                System.out.println("Unable to parse the configuration file: " + filePath + ". Setting default values.");
            }
        }
        Config.setMissing(configuration);
        return configuration;
    }

    public static void write(final AccumuloConfiguration config, String pathToFile) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(pathToFile))) {
            
            for (Config c : Config.values()) {
                writer.write(c.get(config));
                writer.newLine();
            }
        }
    }

    @FunctionalInterface
    public static interface ConfigSetter extends BiConsumer<AccumuloConfiguration, String> {
    }

    @FunctionalInterface
    public static interface ConfigGetter extends Function<AccumuloConfiguration, String> {
    }

    public static enum Config {

        INSTANCE(AccumuloConfiguration::setInstance, AccumuloConfiguration::getInstance, "Instance"),
        ZOOKEEPER(AccumuloConfiguration::setZookeepers, AccumuloConfiguration::getZookeepers, "Zookeepa"),
        USER(AccumuloConfiguration::setUser, AccumuloConfiguration::getUser, "Sean"),
        PASS(AccumuloConfiguration::setPass, AccumuloConfiguration::getPass, "password"),
        NUM_NODES(AccumuloConfiguration::setNumNodes, AccumuloConfiguration::getNumNodes, "4"),
        BATCH_WRITER_NUM_THREADS(AccumuloConfiguration::setBatchWriterNumThreads, AccumuloConfiguration::getBatchWriterNumThreads, "20"),
        BATCH_WRITER_MAX_RAM(AccumuloConfiguration::setBatchWriterMaxRam, AccumuloConfiguration::getBatchWriterMaxRam, "1"),;

        private final ConfigSetter configSetter;
        private final ConfigGetter configGetter;
        private final String defaultValue;
        private static final String SEPARATOR = "=";

        private Config(ConfigSetter configSetter, ConfigGetter configGetter, String defaultValue) {
            this.configSetter = configSetter;
            this.configGetter = configGetter;
            this.defaultValue = defaultValue;
        }

        private String get(AccumuloConfiguration conf) {
            return toString() + SEPARATOR + configGetter.apply(conf);
        }

        private static void set(String confLine, AccumuloConfiguration conf) {
            if (confLine.startsWith("#") || confLine.startsWith("//") || confLine.trim().isEmpty()) {
                //Ignore comments and empty lines.
                return;
            }
            String[] values = confLine.split(SEPARATOR);
            if (values.length != 2) {
                throw new IllegalArgumentException("Malformed configuration line.");
            }
            Config config = valueOf(values[0].trim().toUpperCase());
            System.out.println("Configuration: Setting config value " + config.name() + " = " + values[1]);
            config.configSetter.accept(conf, values[1]);
        }

        private static void setMissing(AccumuloConfiguration conf) {
            Stream.of(Config.values()).forEach(c -> c.setDefaultIfNull(conf));
        }

        private void setDefaultIfNull(AccumuloConfiguration conf) {
            if (null == configGetter.apply(conf)) {
//                System.out.println("Setting missing value: " + this.name() + " = " + defaultValue);
                configSetter.accept(conf, defaultValue);
            }
        }

    }

    public String getInstance() {
        return instance;
    }

    public String getZookeepers() {
        return zookeepers;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getNumNodes() {
        return numNodes;
    }

    public int getNumNodesInt() {
        return Integer.parseInt(getNumNodes());
    }

    public void setNumNodes(String numNodes) {
        this.numNodes = numNodes;
    }

    private void setInstance(String instance) {
        this.instance = instance;
    }

    private void setZookeepers(String zookeepers) {
        this.zookeepers = zookeepers;
    }

    private void setUser(String user) {
        this.user = user;
    }

    private void setPass(String pass) {
        this.pass = pass;
    }

    public void setBatchWriterNumThreads(String batchWriterNumThreads) {
        this.batchWriterNumThreads = batchWriterNumThreads;
    }

    public void setBatchWriterMaxRam(String batchWriterMaxRam) {
        this.batchWriterMaxRam = batchWriterMaxRam;
    }

    public String getBatchWriterNumThreads() {
        return batchWriterNumThreads;
    }

    public int getBatchWriterNumThreadsInt() {
        return Integer.parseInt(getBatchWriterNumThreads());
    }

    public String getBatchWriterMaxRam() {
        return batchWriterMaxRam;
    }

    public long getBatchWriterMaxRamInBytes() {
        return (long) (Double.parseDouble(getBatchWriterMaxRam()) * 1_000_000_000);
    }

}
