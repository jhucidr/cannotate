/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.SecretKeySpec;

public class Decryptor {

    private final File privateKeyFile;

    public Decryptor(File privateKeyFile) {
        this.privateKeyFile = privateKeyFile;
    }

    public List<Encryptable.ObjData> decrypt(File aesKeyIn, File toDecrypt) throws Exception {
        Cipher aesInstance = Cipher.getInstance("AES");
        SecretKeySpec aesKeySpec = loadKey(aesKeyIn, privateKeyFile);
        aesInstance.init(Cipher.DECRYPT_MODE, aesKeySpec);

        try (CipherInputStream is = new CipherInputStream(new FileInputStream(aesKeyIn), aesInstance)) {
            byte[] bytes = new byte[is.available()];
            is.read(bytes);
            return toChars(bytes);
        }
    }

    private List<Encryptable.ObjData> toChars(byte[] bytes) {
        List<Encryptable.ObjData> theData = new ArrayList<>();

        for (byte aByte : bytes) {
            if (aByte == Encryptable.DELIMITER) {

            }
        }
        return theData;
    }

    private SecretKeySpec loadKey(File aesKeyIn, File privateKeyFile) throws GeneralSecurityException, IOException {
        // read private key to be used to decrypt the AES key
        byte[] encodedKey = new byte[(int) privateKeyFile.length()];
        new FileInputStream(privateKeyFile).read(encodedKey);

        // create private key
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey pk = kf.generatePrivate(privateKeySpec);

        // read AES key
        Cipher rsaInstance = Cipher.getInstance("RSA");
        rsaInstance.init(Cipher.DECRYPT_MODE, pk);
        byte[] aesKey = new byte[32];
        CipherInputStream is = new CipherInputStream(new FileInputStream(aesKeyIn), rsaInstance);
        is.read(aesKey);
        SecretKeySpec aeskeySpec = new SecretKeySpec(aesKey, "AES");
        return aeskeySpec;
    }

}
