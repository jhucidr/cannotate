/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor {

    private final File publicKeyFile;

    public Encryptor(File publicKeyFile) {
        this.publicKeyFile = publicKeyFile;
    }

    public void encrypt(Encryptable e, File output, File outputKey) throws Exception {

        Cipher aesInstance = Cipher.getInstance("AES");

        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(256);
        SecretKey key = kgen.generateKey();
        byte[] aesKey = key.getEncoded();
        SecretKeySpec aeskeySpec = new SecretKeySpec(aesKey, "AES");

        aesInstance.init(Cipher.ENCRYPT_MODE, aeskeySpec);

        try (CipherOutputStream os = new CipherOutputStream(new FileOutputStream(output), aesInstance)) {
            for (Object content : e.getContents()) {
                os.write(content.toString().getBytes());
                os.write(Encryptable.DELIMITER);
            }
        }
        saveKey(outputKey, aesKey);

    }

    private void saveKey(File out, byte[] aesKey) throws IOException, GeneralSecurityException {
        Cipher rsaInstance = Cipher.getInstance("RSA");

        // read public key to be used to encrypt the AES key
        byte[] encodedKey = new byte[(int) publicKeyFile.length()];
        new FileInputStream(publicKeyFile).read(encodedKey);

        // create public key
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey pk = kf.generatePublic(publicKeySpec);

        // write AES key
        rsaInstance.init(Cipher.ENCRYPT_MODE, pk);
        CipherOutputStream os = new CipherOutputStream(new FileOutputStream(out), rsaInstance);
        os.write(aesKey);
        os.close();
    }

}
