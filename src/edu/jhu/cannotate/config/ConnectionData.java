/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.config;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Instance;
import org.apache.accumulo.core.client.ZooKeeperInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;

public class ConnectionData {

    private final String instance;
    private final String zoozeepers;
    private final String user;
    //TODO: encrypt
    private final byte[] pass;

    public ConnectionData(String instance, String zoozeepers, String user, byte[] pass) {
        this.instance = instance;
        this.zoozeepers = zoozeepers;
        this.user = user;
        this.pass = pass;
    }

    public Connector getConnection() throws Exception {
        Instance inst = new ZooKeeperInstance(instance, zoozeepers);
        Connector conn = inst.getConnector(user, new PasswordToken(pass));
        return conn;
    }

}
