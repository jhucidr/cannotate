/*
 * Copyright (c) 2016 Johns Hopkins University Applied Physics Lab and Johns Hopkins Genomics.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Sean M.L. Griffith <S.Griffith@jhu.edu> 
 *     J. David Newcomer <David.Newcomer@jhuapl.edu>
 */
package edu.jhu.cannotate.config;

import java.util.List;

public interface Encryptable<T> {

    public static final char DELIMITER = '~';

    public List<Object> getContents();

    public T getObject(List<ObjData> contents);

    public static class ObjData {

        private final char[] data;

        public ObjData(char[] data) {
            this.data = data;
        }

        public String asString() {
            return new String(data);
        }

    }

}
