#!/bin/csh

## from UCSC tables
foreach database (refGene knownGene ensGene )
/Users/sean-la/Documents/sequencing/annovar/annotate_variation.pl -downdb $database -buildver hg19 .
end


## from annovar website
foreach database (dbnsfp31a_interpro dbscsnv11 exac03nonpsych mitimpact24 clinvar_20151201 clinvar_20160302 snp142 )
/Users/sean-la/Documents/sequencing/annovar/annotate_variation.pl -downdb $database -webfrom annovar -buildver hg19 .
end