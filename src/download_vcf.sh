#!/bin/bash

ftp_root="ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/20130502/"
vcf_list_file="./vcf_list.txt"

Download() {
    name=$1
    gz_name=$name".gz"
    wget $ftp_root$gz_name
    gzip -d $gz_name
    echo $name >> $vcf_list_file
}

# this file gets created and appended to over the course of the script.
if [ -f $vcf_list_file ]; then
    rm $vcf_list_file 
fi

chromosomes=( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 ) 
for i in "${chromosomes[@]}"; do
    Download "ALL.chr"$i".phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf" &
done 

Download "ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf"
Download "ALL.chrY.phase3_integrated_v1b.20130502.genotypes.vcf.gz"